package xammt.binding.datamodel;

/**
 * Created with IntelliJ IDEA.
 * User: brano
 */

import data.models.program.element.Type;
import metadata.AnnotationType;

public class ConfigWrapper {
    //annotation path -> full qualified name of annotation element
    private String annotationPath;

    //annotation type ATTRIBUTE/ANNOTATION
    private AnnotationType annotationType;

    //xpath of xml element to be inspected
    private String parentElementXPath;

    //Used to generate binding from program model
    private String genericXpath;

    //xpath of xml value to compare with annotation value (within element)
    private String expectedValueXpath;

    //referenced program element type FIELD/METHOD/CLASS
    private Type programElementType;

    //xpath of xml value to identifying program element (within element)
    private String programElementIdentifierXpath;
    //comparator config, e.g. "Equality()"
    private String comparatorConfiguration;

    //=======Getters & Setters===============================

    public String getAnnotationPath() {
        return annotationPath;
    }

    public void setAnnotationPath(String annotationPath) {
        this.annotationPath = annotationPath;
    }

    public AnnotationType getAnnotationType() {
        return annotationType;
    }

    public void setAnnotationType(AnnotationType annotationType) {
        this.annotationType = annotationType;
    }

    public String getExpectedValueXpath() {
        return expectedValueXpath;
    }

    public void setExpectedValueXpath(String expectedValueXpath) {
        this.expectedValueXpath = expectedValueXpath;
    }

    public Type getProgramElementType() {
        return programElementType;
    }

    public void setProgramElementType(Type programElementType) {
        this.programElementType = programElementType;
    }

    public String getProgramElementIdentifierXpath() {
        return programElementIdentifierXpath;
    }

    public void setProgramElementIdentifierXpath(String programElementIdentifierXpath) {
        this.programElementIdentifierXpath = programElementIdentifierXpath;
    }

    public String getComparatorConfiguration() {
        return comparatorConfiguration;
    }

    public void setComparatorConfiguration(String comparatorConfiguration) {
        this.comparatorConfiguration = comparatorConfiguration;
    }

    public String getParentElementXPath() {
        return parentElementXPath;
    }

    public void setParentElementXPath(String parentElementXPath) {
        this.parentElementXPath = parentElementXPath;
    }

    public void setGenericXpath(String genericXpath) {
        this.genericXpath = genericXpath;
    }

    public String getGenericXpath() {
        return genericXpath;
    }

    //============Overridden methods=============================


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ConfigWrapper)) return false;

        ConfigWrapper that = (ConfigWrapper) o;

        if (!annotationPath.equals(that.annotationPath)) return false;
        if (annotationType != that.annotationType) return false;
        if (!comparatorConfiguration.equals(that.comparatorConfiguration)) return false;
        if (!parentElementXPath.equals(that.parentElementXPath)) return false;
        if (!expectedValueXpath.equals(that.expectedValueXpath)) return false;
        if (!programElementIdentifierXpath.equals(that.programElementIdentifierXpath)) return false;
        if (programElementType != that.programElementType) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = annotationPath.hashCode();
        result = 31 * result + annotationType.hashCode();
        result = 31 * result + parentElementXPath.hashCode();
        result = 31 * result + expectedValueXpath.hashCode();
        result = 31 * result + programElementType.hashCode();
        result = 31 * result + programElementIdentifierXpath.hashCode();
        result = 31 * result + comparatorConfiguration.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return annotationPath;
    }

}
