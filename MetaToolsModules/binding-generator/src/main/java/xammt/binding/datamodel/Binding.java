package xammt.binding.datamodel;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import xammt.binding.generator.helpers.Utilities;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: brano
 */

/*
<metadata id="table.name" type="ATTRIBUTE" target-type="CLASS" target="hibexam.Employee">
        <xpath>/hibernate-mapping/class/@table</xpath>
        <file>/Employee.hbm.xml</file>
</metadata>
 */
public class Binding implements XMLElement{
    //attributes
    private String id;
    private String type;
    private String targetType;
    private String target;

    //element values
    private String xpath;
    private String xmlFilePath;
    private Comparator comparator;

    private static final String TEMPLATE_PATH = "/xammt/binding/datamodel/templates/binding.vm";
//    private static final String TEMPLATE_PATH = "/xammt/binding/hibernate/datamodel/templates/binding.vm";

    public Binding(String id, String type, String targetType, String target, String xpath, String xmlFilePath, String comparator) {
        this.id = id;
        this.type = type;
        this.targetType = targetType;
        this.target = target;
        this.xpath = xpath;
        this.xmlFilePath = xmlFilePath;
        this.comparator = parseComparator(comparator);
    }

    public Comparator getComparator() {
        return comparator;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTargetType() {
        return targetType;
    }

    public void setTargetType(String targetType) {
        this.targetType = targetType;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getXpath() {
        return xpath;
    }

    public void setXpath(String xpath) {
        this.xpath = xpath;
    }

    public String getXmlFilePath() {
        return xmlFilePath;
    }

    public void setXmlFilePath(String xmlFilePath) {
        this.xmlFilePath = xmlFilePath;
    }

    private Comparator parseComparator(String comparator) {
        String name = comparator.substring(0, comparator.indexOf("("));
        List<String> parametersList = new ArrayList<String>();
        String[] parameters = comparator.substring(comparator.indexOf("(") + 1, comparator.indexOf(")")).split(",");

        return new Comparator(name, parameters);
    }

    @Override
    public String asXMLElement() {
        VelocityContext velocityContext = new VelocityContext();
        velocityContext.put("id", id);
        velocityContext.put("type", type);
        velocityContext.put("targetType", targetType);
        velocityContext.put("target", target);
        velocityContext.put("xpath", xpath);
        velocityContext.put("xmlFilePath", xmlFilePath);
        velocityContext.put("comparator", comparator);

        return Utilities.generateCode(velocityContext, TEMPLATE_PATH);
    }

    public static class Comparator implements XMLElement {
        public static final String TEMPLATE_PATH = "/xammt/binding/datamodel/templates/comparator.vm";
        private String name;
        private List<String> parameters = new ArrayList<String>(0);

        public Comparator(String name, List<String> parameters) {
            this.name = name;
            this.parameters = parameters;
        }

        public Comparator(String name, String... parameters) {
            this.name = name;
            for (String parameter : parameters) {
                this.parameters.add(parameter);
            }
        }

        public String getName() {
            return name;
        }

        public List<String> getParameters() {
            return parameters;
        }

        @Override
        public String asXMLElement() {
            VelocityContext velocityContext = new VelocityContext();
            velocityContext.put("comparator", this);

            return Utilities.generateCode(velocityContext, TEMPLATE_PATH);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Binding)) return false;

        Binding binding = (Binding) o;

        if (!id.equals(binding.id)) return false;
        if (!target.equals(binding.target)) return false;
        if (!targetType.equals(binding.targetType)) return false;
        if (!type.equals(binding.type)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + type.hashCode();
        result = 31 * result + targetType.hashCode();
        result = 31 * result + target.hashCode();
        result = 31 * result + xmlFilePath.hashCode();
        result = 31 * result + comparator.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Binding{" +
                "id='" + id + '\'' +
                ", type='" + type + '\'' +
                ", targetType='" + targetType + '\'' +
                ", target='" + target + '\'' +
                '}';
    }
}
