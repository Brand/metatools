package xammt.binding.datamodel;


import org.jdom2.Document;

import java.util.HashMap;
import java.util.List;

/**
 * User: brano
 */
public class XMLClassInfo {
    private String fileName;
    private String fileFullName;
    private List<ClassInfo> classes;
    private Document xmlDocument;

    public XMLClassInfo(String fileName, String fileFullName, Document mappingFile) {
        this.fileName = fileName;
        this.fileFullName = fileFullName;
        this.xmlDocument = mappingFile;
    }

    public String getFileName() {
        return fileName;
    }

    public String getFileFullName() {
        return fileFullName;
    }

    public List<ClassInfo> getClasses() {
        return classes;
    }

    public void setClasses(List<ClassInfo> classes) {
        this.classes = classes;
    }

    public Document getXmlDocument() {
        return xmlDocument;
    }

    public boolean addClassInfo(String simpleName, String fillyQualifiedName, String packageName) {
        ClassInfo classInfo = new ClassInfo(simpleName, fillyQualifiedName, packageName);
        return classes.add(classInfo);
    }

    public static class ClassInfo {
        private String simpleName;
        private String fullyQualifiedName;
        private String packageName;
        private HashMap<String, String> tokens;

        public ClassInfo(String simpleName, String fullyQualifiedName, String packageName) {
            this.simpleName = simpleName;
            this.fullyQualifiedName = fullyQualifiedName;
            this.packageName = packageName;

            tokens = new HashMap<String, String>();
            tokens.put("class_name", simpleName);
            tokens.put("package_name", packageName);
            tokens.put("fully_qualified_class_name", fullyQualifiedName);
        }

        public String getSimpleName() {
            return simpleName;
        }

        public String getFullyQualifiedName() {
            return fullyQualifiedName;
        }

        public String getPackageName() {
            return packageName;
        }

        public HashMap<String, String> getTokens() {
            return tokens;
        }
    }
}
