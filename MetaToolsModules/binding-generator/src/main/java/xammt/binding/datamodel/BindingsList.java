package xammt.binding.datamodel;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import xammt.binding.generator.helpers.Utilities;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: brano
 */
public class BindingsList implements XMLElement {
    private List<Binding> bindingsList;
    private String modelLoaderClasspath = "xammt.validator.modules.metadata.loader.xml.TreePatternModelLoader";

    private static final String TEMPLATE_PATH = "/xammt/binding/datamodel/templates/bindings-list.vm";

    public BindingsList(String modelLoaderClasspath) {
        if (modelLoaderClasspath != null && !"".equals(modelLoaderClasspath.trim())) {
            this.modelLoaderClasspath = modelLoaderClasspath;
        }
        this.bindingsList = new ArrayList<Binding>();
    }

    public String getModelLoaderClasspath() {
        return modelLoaderClasspath;
    }

    public void setModelLoaderClasspath(String modelLoaderClasspath) {
        this.modelLoaderClasspath = modelLoaderClasspath;
    }

    public List<Binding> getBindingsList() {
        return bindingsList;
    }

    public void setBindingsList(List<Binding> bindingsList) {
        this.bindingsList = bindingsList;
    }

    public void addBinding(Binding binding) {
        bindingsList.add(binding);
    }

    public List<Binding> getBindingsForElement(String target) {
        List<Binding> subList = new ArrayList<Binding>();
        if (target != null && !"".equals(target)) {
            for (Binding binding : bindingsList) {
                if (binding.getTarget().equals(target)) {
                    subList.add(binding);
                }
            }
        }

        return subList;
    }

    @Override
    public String asXMLElement() {
        VelocityContext velocityContext = new VelocityContext();
        velocityContext.put("modelLoaderClasspath", modelLoaderClasspath);
        velocityContext.put("bindingsList", bindingsList);

        return Utilities.generateCode(velocityContext, TEMPLATE_PATH);
    }

    public static BindingsList mergeBindingsList(BindingsList first, BindingsList second) {
        first.getBindingsList().removeAll(second.getBindingsList());
        first.getBindingsList().addAll(second.getBindingsList());
        return first;
    }
}
