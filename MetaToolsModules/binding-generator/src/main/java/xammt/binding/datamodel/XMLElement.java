package xammt.binding.datamodel;

/**
 * Created with IntelliJ IDEA.
 * User: brano
 */
public interface XMLElement {
    String asXMLElement();
}
