package xammt.binding.generator.helpers;

import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.jdom2.xpath.XPathExpression;
import org.jdom2.xpath.XPathFactory;

import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: brano
 */
public class Utilities {
    public static String replaceTokens(String template, HashMap<String, String> tokenValues) {
        StringWriter sw = new StringWriter();
        try {
            Velocity.init();
            VelocityContext context = new VelocityContext();
            for (String token : tokenValues.keySet()) {
                context.put(token, tokenValues.get(token));
            }

            Velocity.evaluate(context, sw, "tokenized_string", template);
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.

        }

        return sw.toString();
    }

    public static List<Object> evaluateXPath(Object context, String xpathString) {
        XPathFactory factory = XPathFactory.instance();
        XPathExpression<Object> xpath = factory.compile(xpathString);

        return xpath.evaluate(context);
    }

    public static String generateCode(VelocityContext context, String templateResource) {
        try {
            StringWriter writer = new StringWriter();
            Reader reader = new InputStreamReader(Utilities.class.getResourceAsStream(templateResource));
            Velocity.evaluate(context, writer, "error", reader);
            return writer.toString();
        } catch (Exception ex) {
            ex.printStackTrace();
            return "";
        }
    }
}
