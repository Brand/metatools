package xammt.binding.generator;

import api.ProgramElementInterface;
import api.ProgramMetadataModelInterface;
import data.models.program.element.IdentificatorParser;
import xammt.binding.datamodel.Binding;
import xammt.binding.datamodel.BindingsList;
import xammt.binding.datamodel.ConfigWrapper;
import xammt.binding.datamodel.XMLClassInfo;
import xammt.binding.generator.helpers.Helper;
import xammt.binding.generator.helpers.Utilities;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: brano
 */
public class ProgramMetadataModelBindingGenerator extends BindingGenerator {

    public ProgramMetadataModelBindingGenerator(List<File> xmlFiles, Helper helperObject, List<ConfigWrapper> configs, ProgramMetadataModelInterface programModel) {
        super(xmlFiles, helperObject, configs, programModel);
    }

    protected BindingsList generateBindings() {
        BindingsList bindings = new BindingsList("xammt.validator.modules.metadata.loader.xml.TreePatternModelLoader");
        for (ConfigWrapper config : configs) {
            String annotationMemberFQName = config.getAnnotationPath();
            Map<String,ProgramElementInterface> filteredProgramElementInterfaceMap = programModel.filterProgramElementsByAnnotationPath(annotationMemberFQName);
            for (String programElementIdentifier : filteredProgramElementInterfaceMap.keySet()) {
                ProgramElementInterface programElementInterface = filteredProgramElementInterfaceMap.get(programElementIdentifier);

                String enclosingElementQualifiedId = IdentificatorParser.getEnclosingElementQualifiedId(programElementIdentifier);

                List<XMLClassInfo> associatedClassesInXMLs = getAssociatedClassesInXMLs(enclosingElementQualifiedId);
                if (associatedClassesInXMLs.size() == 0) {
                    System.out.println("No associated XML configurations for class: " + enclosingElementQualifiedId);
                    continue;
                }

                Map<String, Object> metadata = programElementInterface.getMetadata();
                for (String annotationIdentifier : metadata.keySet()) {
                    if (!annotationMemberFQName.equals(annotationIdentifier)) {
                        continue;
                    }

//                    Object metadataValue = metadata.get(annotationIdentifier);

                    for (XMLClassInfo associatedClassesInXML : associatedClassesInXMLs) {
                        for (XMLClassInfo.ClassInfo classInfo : associatedClassesInXML.getClasses()) {
                            if (classInfo.getFullyQualifiedName().equals(enclosingElementQualifiedId)) {
                                //add binding

                                HashMap<String,String> tokens = helperObject.modifyTokens(associatedClassesInXML.getXmlDocument(), addAdditionalTokens(classInfo.getTokens(), programElementInterface));
                                String valueXpath = Utilities.replaceTokens(config.getGenericXpath(), tokens);
                                bindings.addBinding(new Binding(annotationMemberFQName, config.getAnnotationType().toString(), config.getProgramElementType().toString(), programElementIdentifier, valueXpath, associatedClassesInXML.getFileName(), config.getComparatorConfiguration()));
                            }
                        }
                    }
                }
            }
        }
        return bindings;
    }

    private HashMap<String, String> addAdditionalTokens(HashMap<String, String> tokens, ProgramElementInterface programElement) {
        tokens.put("element_name", programElement.getSimpleIdentificator());
        tokens.put("fully_qualified_element_name", programElement.getQualifiedIdentificator());
        return tokens;
    }

    private List<XMLClassInfo>  getAssociatedClassesInXMLs(String fullClassName) {
        List<XMLClassInfo> filteredClassInfos = new ArrayList<XMLClassInfo>();
        for (XMLClassInfo xmlDocument : xmlDocuments) {
            for (XMLClassInfo.ClassInfo classInfo : xmlDocument.getClasses()) {
                if (classInfo.getFullyQualifiedName().equals(fullClassName)) {
                    filteredClassInfos.add(xmlDocument);
                }
            }
        }
        return filteredClassInfos;
    }
}
