package xammt.binding.generator.helpers;

import org.jdom2.Document;
import xammt.binding.datamodel.XMLClassInfo;

import java.util.HashMap;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: brano
 */
public interface Helper {
    String getFrameworkName();
    String getRootElementName();
    List<XMLClassInfo.ClassInfo> getClassesInDocument(Document document);
    String getElementName(String element);
    String getElementFullName(HashMap<String,String> tokens, String element);
    HashMap<String, String> modifyTokens(Document xmlContext, HashMap<String, String> tokens);
}
