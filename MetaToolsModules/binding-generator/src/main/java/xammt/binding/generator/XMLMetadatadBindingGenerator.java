package xammt.binding.generator;

import org.jdom2.Attribute;
import org.jdom2.Content;
import org.jdom2.Element;
import org.jdom2.xpath.XPathHelper;
import xammt.binding.datamodel.Binding;
import xammt.binding.datamodel.BindingsList;
import xammt.binding.datamodel.ConfigWrapper;
import xammt.binding.datamodel.XMLClassInfo;
import xammt.binding.generator.helpers.Helper;
import xammt.binding.generator.helpers.Utilities;

import java.io.File;
import java.util.HashMap;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: brano
 */
public class XMLMetadatadBindingGenerator extends BindingGenerator {

    public XMLMetadatadBindingGenerator(List<File> xmlFiles, List<ConfigWrapper> configs, Helper helperObject) {
        super(xmlFiles, helperObject, configs, null);
    }

    protected BindingsList generateBindings() {

        BindingsList bindingList = new BindingsList("xammt.validator.modules.metadata.loader.xml.TreePatternModelLoader");
        for (XMLClassInfo xmlDocument : xmlDocuments) {
            for (XMLClassInfo.ClassInfo classInfo : xmlDocument.getClasses()) {
                HashMap<String, String> tokens = new HashMap<String, String>();
                tokens.putAll(classInfo.getTokens());
                for (ConfigWrapper config : configs) {
                    String parentElementXpath = Utilities.replaceTokens(config.getParentElementXPath(), tokens);
                    String programElementXpath = Utilities.replaceTokens(config.getProgramElementIdentifierXpath(), tokens);
                    String expectedValueXpath = Utilities.replaceTokens(config.getExpectedValueXpath(), tokens);

                    List<Object> elements = Utilities.evaluateXPath(xmlDocument.getXmlDocument(), Utilities.replaceTokens(parentElementXpath, helperObject.modifyTokens(xmlDocument.getXmlDocument(), tokens)));
                    for (Object elementObject : elements) {
                        if (elementObject instanceof Element) {
                            Element parentElement = (Element) elementObject;
                            List<Object> programElementObjects = Utilities.evaluateXPath(parentElement, programElementXpath);
                            if (programElementObjects.size() == 1) {
                                if (programElementObjects.get(0) instanceof Element) {
                                    Element programElement = (Element) programElementObjects.get(0);
                                    String target = helperObject.getElementFullName(tokens, programElement.getValue());
                                    String absolutePath = getAbsoluteXpath(parentElement, expectedValueXpath);
                                    Binding binding = new Binding(config.getAnnotationPath(), config.getAnnotationType().toString(), config.getProgramElementType().toString(), target, absolutePath, xmlDocument.getFileName(), config.getComparatorConfiguration());
                                    bindingList.addBinding(binding);
                                } else if (programElementObjects.get(0) instanceof Attribute) {
                                    Attribute programElement = (Attribute) programElementObjects.get(0);
                                    String target = helperObject.getElementFullName(tokens, programElement.getValue());
                                    String absolutePath = getAbsoluteXpath(parentElement, expectedValueXpath);
                                    Binding binding = new Binding(config.getAnnotationPath(), config.getAnnotationType().toString(), config.getProgramElementType().toString(), target, absolutePath, xmlDocument.getFileName(), config.getComparatorConfiguration());
                                    bindingList.addBinding(binding);
                                }

                            }
                        }
                    }
                }
            }
        }
        return bindingList;
    }

    private String getAbsoluteXpath(Object context, String xpath) {
        List<Object> elements = Utilities.evaluateXPath(context, xpath);
        if (elements.get(0) instanceof Content) {
            return XPathHelper.getAbsolutePath((Content)   elements.get(0));
        } else {
            return XPathHelper.getAbsolutePath((Attribute) elements.get(0));
        }
    }
}
