package xammt.binding.generator;

import data.models.program.element.Type;
import metadata.AnnotationType;
import xammt.binding.datamodel.ConfigWrapper;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: brano
 */
public class ConfigParser {
    private static boolean LOGGING = false;

    public static final String COMMENT = "#";
    public static final String DELIMITER = ";";
    public static final String TERMINATOR = ";";
    public static final int NUMBER_OF_TOKENS = 8;

    private File configFile;
    private List<ConfigWrapper> configs;


    public ConfigParser(File configFile) {
        this.configFile = configFile;
        this.configs = new ArrayList<ConfigWrapper>();
    }

    public List<ConfigWrapper> parse() {
        LineNumberReader fileReader = null;
        try {
            FileReader input = new FileReader(configFile);
            fileReader = new LineNumberReader(input);

            String currentLine = null;

            while ((currentLine = fileReader.readLine()) != null) {
                ConfigWrapper configWrapper = parseLine(currentLine, fileReader.getLineNumber());

                if (configWrapper == null) {
                    continue;
                }

                configs.add(configWrapper);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fileReader != null) {
                try {
                    fileReader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return configs;
    }

    private ConfigWrapper parseLine(String line, int lineNumber) {
        ConfigWrapper config = null;

        String trimmed = line.trim();

        if ("".equals(trimmed)) {
            if (LOGGING) System.out.println("Empty line: " + lineNumber);
            return null;
        }

        if (trimmed.contains(COMMENT)) {
            int index = trimmed.indexOf(COMMENT);
            trimmed = trimmed.substring(0, index).trim();
            if (LOGGING) System.out.println("Comment found: " + lineNumber);
            if ("".equals(trimmed)) return null;
        }


        if (!trimmed.endsWith(TERMINATOR)) {
            if (LOGGING) System.out.println("Line not ended well, terminating character = " + TERMINATOR + " : " + lineNumber);
            return null;
        }

        trimmed = trimmed.substring(0, trimmed.length() - 1);

        String[] tokens = trimmed.split(DELIMITER);

        if (tokens.length != NUMBER_OF_TOKENS) {
            if (LOGGING) System.out.println("Wrong format of config, needed 6 values separated by " + DELIMITER +  " : " + lineNumber);
            return null;
        }

        String parentElementXpath = tokens[0];
        String programElementIdentifierXpath = tokens[1];
        String expectedValueXpath = tokens[2];
        String genericXpath = tokens[3];
        String annotationPath = tokens[4];
        AnnotationType annotationType = AnnotationType.valueOf(tokens[5]);;
        if (annotationType == null) {
            if (LOGGING) System.out.println("Expected annotation type (ANNOTATION/ATTRIBUTE): " + lineNumber);
            return null;
        }
        Type programElementType = Type.valueOf(tokens[6]);
        if (programElementType == null) {
            if (LOGGING) System.out.println("Expected program element type (FIELD/CLASS/PACKAGE/METHOD etc.): " + lineNumber);
            return null;
        }
        String comparatorConfig = tokens[7];

        config = new ConfigWrapper();
        config.setParentElementXPath(parentElementXpath);
        config.setGenericXpath(genericXpath);
        config.setAnnotationPath(annotationPath);
        config.setAnnotationType(annotationType);
        config.setExpectedValueXpath(expectedValueXpath);
        config.setProgramElementType(programElementType);
        config.setProgramElementIdentifierXpath(programElementIdentifierXpath);
        config.setComparatorConfiguration(comparatorConfig);

        return config;
    }
}