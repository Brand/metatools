package xammt.binding.generator;

import api.ProgramMetadataModelInterface;
import org.jdom2.Document;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import xammt.binding.datamodel.BindingsList;
import xammt.binding.datamodel.ConfigWrapper;
import xammt.binding.datamodel.XMLClassInfo;
import xammt.binding.generator.helpers.Helper;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: brano
 */
public abstract class BindingGenerator {
    protected List<File> xmlFiles;
    protected List<XMLClassInfo> xmlDocuments;
    protected Helper helperObject;
    protected List<ConfigWrapper> configs;
    protected ProgramMetadataModelInterface programModel;

    protected BindingGenerator(List<File> xmlFiles, Helper helperObject, List<ConfigWrapper> configs, ProgramMetadataModelInterface programModel) {
        this.xmlFiles = xmlFiles;
        this.helperObject = helperObject;
        this.configs = configs;
        this.programModel = programModel;
        this.xmlDocuments = new ArrayList<XMLClassInfo>();
    }

    protected void readXMLFiles() {
        SAXBuilder docBuilder = new SAXBuilder();

        for (File file : xmlFiles) {
            try {
                Document document = docBuilder.build(file);

                if (!helperObject.getRootElementName().equals(document.getRootElement().getName())) {
                    System.out.println("Not a configuration file of " + helperObject.getFrameworkName() + " framework - skipping file: " + file.getName());
                    continue;
                }

                xmlDocuments.add(new XMLClassInfo(file.getName(), file.getAbsolutePath(), document));
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JDOMException e) {
                e.printStackTrace();
            }
        }
    }

    protected void fillClassInfos() {
        for (XMLClassInfo xmlClassInfo : xmlDocuments) {
            xmlClassInfo.setClasses(helperObject.getClassesInDocument(xmlClassInfo.getXmlDocument()));
        }
    }

    public List<File> getXmlFiles() {
        return xmlFiles;
    }

    public List<XMLClassInfo> getXmlDocuments() {
        return xmlDocuments;
    }

    public Helper getHelperObject() {
        return helperObject;
    }

    public List<ConfigWrapper> getConfigs() {
        return configs;
    }

    public BindingsList generate() {
        readXMLFiles();
        fillClassInfos();
        return generateBindings();
    }

    protected abstract BindingsList generateBindings();
}
