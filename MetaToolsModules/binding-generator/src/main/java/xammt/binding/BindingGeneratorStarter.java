package xammt.binding;

import api.ProgramMetadataModelInterface;
import data.models.configuration.metadata.validator.Configuration;
import xammt.binding.datamodel.BindingsList;
import xammt.binding.datamodel.ConfigWrapper;
import xammt.binding.generator.ConfigParser;
import xammt.binding.generator.ProgramMetadataModelBindingGenerator;
import xammt.binding.generator.XMLMetadatadBindingGenerator;
import xammt.binding.generator.helpers.Helper;
import xammt.binding.hibernate.HibernateHelper;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: brano
 */
public class BindingGeneratorStarter {
    private List<File> metadataXMLs;
    private List<ConfigWrapper> configs;
    private ProgramMetadataModelInterface model;
    private File config;
    private Helper chosenHelper;

    private static List<Helper> helpers;

    static {
        helpers = new ArrayList<Helper>(1);
        helpers.add(new HibernateHelper());
    }

    public static List<Helper> getHelpers() {
        return helpers;
    }

    public BindingGeneratorStarter(ProgramMetadataModelInterface model, File config, List<File> metadataXMLs, Helper chosenHelper) {
        this.model = model;
        this.config = config;
        this.metadataXMLs = metadataXMLs;
        this.chosenHelper = chosenHelper;

        ConfigParser configParser = new ConfigParser(config);
        configs = configParser.parse();
    }

    public String generateBindings() {
        XMLMetadatadBindingGenerator generatorFromXML = new XMLMetadatadBindingGenerator(metadataXMLs, configs, chosenHelper);
        ProgramMetadataModelBindingGenerator generatorFromModel = new ProgramMetadataModelBindingGenerator(metadataXMLs, chosenHelper, configs, model);

        BindingsList fromModel = generatorFromModel.generate();
        BindingsList fromXMLs = generatorFromXML.generate();

        return BindingsList.mergeBindingsList(fromXMLs, fromModel).asXMLElement();
    }
}
