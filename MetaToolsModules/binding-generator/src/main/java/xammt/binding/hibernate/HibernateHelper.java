package xammt.binding.hibernate;

/**
 * Created with IntelliJ IDEA.
 * User: brano
 */

import data.models.program.element.IdentificatorParser;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.xpath.XPathExpression;
import org.jdom2.xpath.XPathFactory;
import xammt.binding.datamodel.XMLClassInfo;
import xammt.binding.generator.helpers.Helper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Class which "understands" hibernate mapping files
 */
public class HibernateHelper implements Helper {
    public static final String HIBERNATE_MAPPING_CLASS_XPATH = "/hibernate-mapping/class";
    public static final String HIBERNATE_MAPPING = "/hibernate-mapping";
    public static final String PACKAGE = "package";
    private XPathFactory xPathfactory;
    private XPathExpression<Object> xpath;

    @Override
    public String getFrameworkName() {
        return "hibernate 3.0";
    }

    @Override
    public String getRootElementName() {
        return "hibernate-mapping";
    }

    @Override
    public List<XMLClassInfo.ClassInfo> getClassesInDocument(Document document) {
        List<XMLClassInfo.ClassInfo> classInfos = new ArrayList<XMLClassInfo.ClassInfo>();
        xPathfactory = XPathFactory.instance();

        xpath = xPathfactory.compile(HIBERNATE_MAPPING);
        List<Object> elements = xpath.evaluate(document);
        if (elements.size() == 1 && elements.get(0) instanceof Element) {
            Element rootElement = (Element) elements.get(0);
            String packageName = rootElement.getAttribute(PACKAGE) != null ? rootElement.getAttribute(PACKAGE).getValue() : null;

            xpath = xPathfactory.compile(HIBERNATE_MAPPING_CLASS_XPATH);
            List<Object> classes = xpath.evaluate(rootElement);
            for (Object aClass : classes) {
                if (aClass instanceof Element) {
                    Element classElement = (Element) aClass;
                    String className = classElement.getAttribute("name") != null ? classElement.getAttribute("name").getValue() : null;
                    if (className == null) {
                        System.out.println("Class name missing - not valid hibernate configuration");
                        continue;
                    }

                    if (packageName != null && !"".equals(packageName)) {
                        classInfos.add(new XMLClassInfo.ClassInfo(className, packageName + "." + className, packageName));
                    } else {
                        IdentificatorParser identificatorParser = new IdentificatorParser(className);
                        identificatorParser.parse();
                        String simpleClassName = IdentificatorParser.getSimpleId(className);
                        packageName = identificatorParser.getEnclosingElementQualifiedId();
                        classInfos.add(new XMLClassInfo.ClassInfo(simpleClassName, className, packageName));
                    }
                }
            }
        }

        return classInfos;
    }

    @Override
    public String getElementName(String element) {
        return element;
    }

    @Override
    public String getElementFullName(HashMap<String, String> tokens, String programElementName) {
        return tokens.get("fully_qualified_class_name") + "." + programElementName;
    }

    @Override
    public HashMap<String, String> modifyTokens(Document xmlDocument, HashMap<String, String> tokens) {
        //determine class name and change it if needed to fully qualified or vice versa
        String packageName = getPackageName(xmlDocument);
        if (packageName == null) {
            tokens.put("class_name", tokens.get("fully_qualified_class_name"));
        }

        return tokens;
    }

    private String getPackageName(Document document) {
        xPathfactory = XPathFactory.instance();

        xpath = xPathfactory.compile(HIBERNATE_MAPPING);
        List<Object> elements = xpath.evaluate(document);
        if (elements.size() == 1 && elements.get(0) instanceof Element) {
            Element rootElement = (Element) elements.get(0);
            return rootElement.getAttribute(PACKAGE) != null ? rootElement.getAttribute(PACKAGE).getValue() : null;
        }

        return null;
    }
}
