package forms;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import java.io.File;

/**
 * Trieda reprezentuje rozšírenie grafického dialógu JFileChooser.
 * @author Martin Pribula
 */
public class FileChooser extends JFileChooser {

    /**
     * Členská premenná reprezentuje zvolenú možnosť v dialógu.
     */
    private int option;

    /**
     * Konštruktor triedy.
     * @param filter Filter aký sa na súbory aplikuje.
     */
    public FileChooser(FileFilter filter) {
        this(filter, System.getProperty("user.dir"));
    }

    public FileChooser(FileFilter filter, String directory) {
        super();
        setCurrentDirectory(new File(directory));

        if (filter != null) {
            addChoosableFileFilter(filter);
        } else {
            setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        }
    }

    @Override
    public void approveSelection() {
        super.approveSelection();
        option = JFileChooser.APPROVE_OPTION;
    }

    @Override
    public void cancelSelection() {
        super.cancelSelection();
        option = JFileChooser.CANCEL_OPTION;
    }

    /**
     * Metôda určená na získanie hodnoty členskej premennej option.
     * @return Hodnota členskej premennej option.
     */
    public int getOption() {
        return option;
    }
}
