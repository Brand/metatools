package file.filters;

import java.io.File;

/**
 * Trieda reprezentuje súborový filter. Konkrétne ide o filter na súbory typu .xsd.
 * @author Martin Pribula
 */
public class XsdFileFilter extends javax.swing.filechooser.FileFilter {

    /**
     * Metoda posudzujúca či vstupný súbor vyhovuje filtru.
     * @param f Vstupný súbor na porovnanie.
     * @return true ak súbor vyhovuje filtru.
     */
    @Override
    public boolean accept(File f) {
        return f.getName().endsWith(".xsd") || f.isDirectory();
    }

     /**
     * Metóda vracajúca popis filtrovaných súborov.
     * @return popis filtrovaných súborov.
     */
    @Override
    public String getDescription() {
        return "*.xsd";
    }
}
