package file.filters;

import javax.swing.filechooser.FileFilter;
import java.io.File;

/**
 * Trieda reprezentuje súborový filter. Konkrétne ide o filter na súbory typu .xml.
 * @author Martin Pribula
 */
public class XmlFileFilter extends FileFilter {

    /**
     * Metoda posudzujúca či vstupný súbor vyhovuje filtru.
     * @param f Vstupný súbor na porovnanie.
     * @return true ak súbor vyhovuje filtru.
     */
    @Override
    public boolean accept(File f) {
        return f.getName().endsWith(".xml") || f.isDirectory();
    }

    /**
     * Metóda vracajúca popis filtrovaných súborov.
     * @return popis filtrovaných súborov.
     */
    @Override
    public String getDescription() {
        return "*.xml";
    }
}
