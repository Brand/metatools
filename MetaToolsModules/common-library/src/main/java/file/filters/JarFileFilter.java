package file.filters;

import javax.swing.filechooser.FileFilter;
import java.io.File;

/**
 * Trieda reprezentuje súborový filter. Konkrétne ide o filter na súbory typu .jar.
 * @author Martin Pribula
 */
public class JarFileFilter extends FileFilter {

    /**
     * Metoda posudzujúca či vstupný súbor vyhovuje filtru.
     * @param f Vstupný súbor na porovnanie.
     * @return true ak súbor vyhovuje filtru.
     */
    @Override
    public boolean accept(File f) {
        return f.getName().endsWith(".jar") || f.isDirectory();
    }

    /**
     * Metóda vracajúca popis filtrovaných súborov.
     * @return popis filtrovaných súborov.
     */
    @Override
    public String getDescription() {
        return "*.jar";
    }
}
