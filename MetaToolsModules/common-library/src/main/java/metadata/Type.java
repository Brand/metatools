package metadata;

/**
 * Enumeračný typ, ktorý reprezentuje typ programového metaúdaja.
 * @author Martin Pribula
 */
public enum Type {

    /**
     * Konštanta reprezentuje anotáciu.
     */
    ANNOTATION,
    /**
     * Konštanta reprezentuje atribút anotácie.
     */
    ATTRIBUTE
}
