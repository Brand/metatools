package metadata;

import javax.xml.bind.annotation.XmlEnum;

/**
 * Created with IntelliJ IDEA.
 * User: brano
 */
@XmlEnum
public enum AnnotationType {
    ANNOTATION,
    ATTRIBUTE
}
