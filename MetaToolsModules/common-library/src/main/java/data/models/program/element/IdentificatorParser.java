package data.models.program.element;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This class represents Java identifier parser.
 * @author Martin Pribula
 */
public class IdentificatorParser {

    /**
     * Kvalifikované meno vstupného elementu.
     */
    private String elementQualifiedId;
    /**
     * Jednoduché meno vstupného elementu.
     */
    private String elementSimpleId;
    /**
     * Kvalifikované meno rodiča vstupného elementu.
     */
    private String enclosingElementQualifiedId;
    /**
     * Jednoduché meno rodiča vstupného elementu.
     */
    private String enclosingElementSimpleId;

    /**
     * Konštruktor
     * @param elementQualifiedId Kvalifikované meno vstupného elementu
     */
    public IdentificatorParser(String elementQualifiedId) {
        this.elementQualifiedId = elementQualifiedId;
    }

    /**
     * Hlavná metóda triedy, umožňuje parsovanie všetkých potrebných identifikátorov.
     */
    public void parse() {
        enclosingElementQualifiedId = getEnclosingElementQualifiedId(elementQualifiedId);
        enclosingElementSimpleId = getEnclosingElementSimpleId(elementQualifiedId);
        elementSimpleId = getEnclosingElementSimpleId(elementQualifiedId);
    }

    /**
     * Statická metóda na získanie jednoduchého identifikátora z kvalifikovaného.
     * @param qualifiedId Vstupný kvalifikovaný identifikátor.
     * @return Jednoduchý identifikátor odvodený zo vstupného kvalifikovaného.
     */
    public static String getSimpleId(String qualifiedId) {
        if (qualifiedId != null) {
            String identificatorRegular = "[a-zA-Z_$][\\w$]*";
            Pattern methodIdPattern = Pattern.compile("(" + identificatorRegular + ")(\\.(" + identificatorRegular + "))*(\\.((" + identificatorRegular + ")\\(.*\\).*)){1}", Pattern.DOTALL);
            Matcher matcher = methodIdPattern.matcher(qualifiedId);

            if (matcher.matches()) {
                return matcher.group(5);
            } else {
                int lastDotIndex = qualifiedId.lastIndexOf('.');
                String simpleId = lastDotIndex != -1 ? qualifiedId.substring(lastDotIndex + 1) : qualifiedId;
                return simpleId;
            }
        } else {
            return null;
        }
    }

    /**
     * Statická metóda na odvodenie jednoduchého mena rodiča programového elementu.
     * @param qualifiedProgramElementIdentificator Kvalifikovaný identifikátor programového elementu.
     * @return Jednoduché meno rodičovského elementu.
     */
    public static String getEnclosingElementSimpleId(String qualifiedProgramElementIdentificator) {
        return getSimpleId(getEnclosingElementQualifiedId(qualifiedProgramElementIdentificator));
    }

    /**
     * Statická metóda na odvodenie jednoduchého mena rodiča programového elementu.
     * @param qualifiedId Kvalifikované meno programového elementu.
     * @return Kvalifikované meno rodičovského elementu.
     */
    public static String getEnclosingElementQualifiedId(String qualifiedId) {
        if (qualifiedId != null) {
            String identificatorRegular = "[a-zA-Z_$][\\w$]*";
            Pattern methodIdPattern = Pattern.compile("((" + identificatorRegular + ")(\\.(" + identificatorRegular + "))*)(\\.((" + identificatorRegular + ")\\(.*\\).*)){1}", Pattern.DOTALL);
            Matcher matcher = methodIdPattern.matcher(qualifiedId);

            if (matcher.matches()) {
                return matcher.group(1);
            } else {
                int lastDotIndex = qualifiedId.lastIndexOf('.');
                String identificator = lastDotIndex != -1 ? qualifiedId.substring(0, lastDotIndex) : null;
                return identificator;
            }
        } else {
            return null;
        }
    }

    /**
     * Metóda určená na získanie hodnoty členskej premennej elementQualifiedId.
     * @return Hodnota uchovaná v členskej premennej elementQualifiedId.
     */
    public String getElementQualifiedId() {
        return elementQualifiedId;
    }

    /**
     * Metóda určená na získanie hodnoty členskej premennej elementSimpleId.
     * @return Hodnota uchovaná v členskej premennej elementSimpleId.
     */
    public String getElementSimpleId() {
        return elementSimpleId;
    }

    /**
     * Metóda určená na získanie hodnoty členskej premennej enclosingElementQualifiedId.
     * @return Hodnota uchovaná v členskej premennej enclosingElementQualifiedId.
     */
    public String getEnclosingElementQualifiedId() {
        return enclosingElementQualifiedId;
    }

    /**
     * Metóda určená na získanie hodnoty členskej premennej enclosingElementSimpleId.
     * @return Hodnota uchovaná v členskej premennej enclosingElementSimpleId.
     */
    public String getEnclosingElementSimpleId() {
        return enclosingElementSimpleId;
    }
}
