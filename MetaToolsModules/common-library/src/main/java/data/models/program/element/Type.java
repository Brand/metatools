package data.models.program.element;

import javax.xml.bind.annotation.XmlEnum;

/**
 * Enumeračný typ používaný na určenie typu programového elementu.
 * @author Martin Pribula
 */
@XmlEnum
public enum Type {

    /**
     * Konštanta reprezentujúca typ balík.
     */
    PACKAGE,
    /**
     * Konštanta reprezentujúca typ anotácia.
     */
    ANNOTATION,
    /**
     * Konštanta reprezentujúca typ metóda anotácie.
     */
    ANNOTATION_METHOD,
    /**
     * Konštanta reprezentujúca typ rozhranie.
     */
    INTERFACE,
    /**
     * Konštanta reprezentujúca typ metóda rozhrania.
     */
    INTERFACE_METHOD,
    /**
     * Konštanta reprezentujúca enumeračný typ.
     */
    ENUM,
    /**
     * Konštanta reprezentujúca typ metóda enumeračného typu.
     */
    ENUM_METHOD,
    /**
     * Konštanta reprezentujúca typ konštruktor enumeračného typu.
     */
    ENUM_CONSTRUCTOR,
    /**
     * Konštanta reprezentujúca typ konštanta enumeračného typu.
     */
    CONSTANT,
    /**
     * Konštanta reprezentujúca typ trieda.
     */
    CLASS,
    /**
     * Konštanta reprezentujúca typ členská premenná triedy.
     */
    FIELD,
    /**
     * Konštanta reprezentujúca typ konštruktor triedy.
     */
    CONSTRUCTOR,
    /**
     * Konštanta reprezentujúca typ metóda triedy.
     */
    METHOD;

    /**
     * Metóda zisťujúca či daný objekt reprezentuje triedu, rozhranie alebo enumeračný typ.
     * @return true ak pravda.
     */
    public boolean isType() {
        return equals(CLASS) || equals(ENUM) || equals(INTERFACE);
    }

    /**
     * Metóda zisťujúca či daný objekt reprezentuje anotačný typ.
     * @return true ak pravda.
     */
    public boolean isAnnotation() {
        return equals(ANNOTATION);
    }

    /**
     * Metóda zisťujúca či daný objekt reprezentuje balík.
     * @return true ak pravda.
     */
    public boolean isPackage() {
        return equals(PACKAGE);
    }

    /**
     * Metóda zisťujúca či daný objekt reprezentuje člen.
     * @return true ak pravda.
     */
    public boolean isMember() {
        return equals(METHOD)
                || equals(FIELD)
                || equals(ANNOTATION_METHOD)
                || equals(CONSTANT)
                || equals(CONSTRUCTOR)
                || equals(ENUM_CONSTRUCTOR)
                || equals(ENUM_METHOD)
                || equals(INTERFACE_METHOD);
    }

    /**
     * Metóda zisťujúca či daný objekt reprezentuje metódu.
     * @return true ak pravda.
     */
    public boolean isMethod() {
        return equals(METHOD)
                || equals(ANNOTATION_METHOD)
                || equals(CONSTRUCTOR)
                || equals(ENUM_CONSTRUCTOR)
                || equals(ENUM_METHOD)
                || equals(INTERFACE_METHOD);
    }

    /**
     * Metóda zisťujúca či daný objekt reprezentuje členskú premennú.
     * @return true ak pravda.
     */
    public boolean isField() {
        return equals(FIELD) || equals(CONSTANT);
    }
}
