//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2011.04.27 at 02:05:00 PM CEST 
//


package data.models.metadata.definition;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for target.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="target">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="PACKAGE"/>
 *     &lt;enumeration value="TYPE"/>
 *     &lt;enumeration value="FIELD"/>
 *     &lt;enumeration value="CONSTRUCTOR"/>
 *     &lt;enumeration value="METHOD"/>
 *     &lt;enumeration value="ANNOTATION_TYPE"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "target")
@XmlEnum
public enum Target {

    PACKAGE,
    TYPE,
    FIELD,
    CONSTRUCTOR,
    METHOD,
    ANNOTATION_TYPE;

    public String value() {
        return name();
    }

    public static Target fromValue(String v) {
        return valueOf(v);
    }

}
