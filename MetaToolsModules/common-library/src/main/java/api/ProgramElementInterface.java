package api;

import data.models.program.element.Type;

import java.io.Serializable;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: brano
 */
public interface ProgramElementInterface extends Serializable {
    Type getType();

    String getQualifiedIdentificator();

    String getSimpleIdentificator();

    Map<String, Object> getMetadata();
}
