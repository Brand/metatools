package api;

import java.io.Serializable;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: brano
 */
public interface ProgramMetadataModelInterface extends Cloneable, Serializable {
    Map<String, ProgramElementInterface> getProgramElements();

    ProgramElementInterface getPackageElement(String programElementIdentificator);

    ProgramElementInterface getTypeElement(String programElementIdentificator);

    Object clone() throws CloneNotSupportedException;

    Map<String, ProgramElementInterface> filterProgramElementsByAnnotationPath(String annotationFQ);
}
