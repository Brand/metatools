package xml;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.File;
import java.io.IOException;

/**
 * Pomocná trieda, obsahujúca metódy používané v spojení s technológiou XML a XPath.
 * @author Martin Pribula
 */
public class Utility {

    /**
     * Metóda slúžiaca na výber elementu/atribútu vstupného XML dokumentu na základe XPath výrazu.
     *
     * @param object Vstupný XML dokument.
     * @param expression Výraz XPath nazáklade ktorého sa výberá element z XML súboru.
     * @param xPathConstant Konštanty XPath výberu.
     * @return Elementy/atribúty zo vstupného XML súboru vyhovujúce vstupnému XPath výrazu.
     */
    public static Object evaluateXPathExpression(Object object, String expression, QName xPathConstant) {
        try {
            XPathFactory xPathFactory = XPathFactory.newInstance();
            XPath xpath = xPathFactory.newXPath();
            XPathExpression xPathExpression = xpath.compile(expression);
            return xPathExpression.evaluate(object, xPathConstant);
        } catch (XPathExpressionException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    /**
     * Statická metóda slúžiaca na načítanie XML dokumentu zo stanoveného súboru.
     * @param file Súbor, z ktorého sa má načítať XML dokument.
     * @return Objekt resp. pamäťová reprezentácia XML dokumentu.
     */
    public static Document loadDocument(File file) {
        try {
            DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
            Document document = docBuilder.parse(file);
            return document;
        } catch (SAXException ex) {
            System.out.println(ex.getMessage());
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        } catch (ParserConfigurationException ex) {
            System.out.println(ex.getMessage());
        }
        return null;
    }
}
