package xammt.validator.modules.gui;

import data.models.configuration.metadata.validator.Configuration;
import org.apache.commons.io.FileUtils;
import xammt.validator.modules.ValidatorModule;

import javax.annotation.processing.AbstractProcessor;
import javax.tools.JavaCompiler;
import javax.tools.JavaFileObject;
import javax.tools.StandardJavaFileManager;
import javax.tools.ToolProvider;
import java.io.File;
import java.util.*;

/**
 * Created by IntelliJ IDEA.
 * User: Brano
 * Date: 17.12.2012
 * Time: 0:27
 */
public class AnnotationProcessorRunner {
    private Configuration configuration;
    private ValidatorModule processor;

    public AnnotationProcessorRunner(Configuration configuration) {
        this.configuration = configuration;
    }

    public void runAnnotationProcessor() {
        System.out.println("Obtaining java compiler instance");
        JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
        StandardJavaFileManager fileManager = compiler.getStandardFileManager(null, null, null);

        //list of source files
        Collection sourceFiles = this.getAllFilesFromDirectory(this.configuration.getJavaSources(), "java");
        Iterable<? extends JavaFileObject> compilationUnits = fileManager.getJavaFileObjects((File[]) sourceFiles.toArray(new File[sourceFiles.size()]));

        //create classpath -> must contain dependencies of our annotation processor + annotations
        List<String> optionList = new ArrayList<String>();
        optionList.addAll(Arrays.asList("-classpath", "lib/*" + System.getProperty("path.separator") + this.getAllFilesFromDirectory(this.configuration.getAnnotationsFile())));
        optionList.addAll(Arrays.asList("-proc:only"));

        JavaCompiler.CompilationTask task = compiler.getTask(null, fileManager, null, optionList, null, compilationUnits);
        // Create a list to hold annotation processors
        LinkedList<AbstractProcessor> processors = new LinkedList<AbstractProcessor>();
        // Add an annotation processor to the list
        processor = new ValidatorModule();
        processors.add(processor);
        // Set the annotation processor to the compiler task
        task.setProcessors(processors);

        System.out.println("Running javac task with arguments: " + optionList.toString());
        task.call();
    }

    private Collection getAllFilesFromDirectory(String dir, String extension) {
        File directory = new File(dir);
        if (directory.isDirectory()) {
            return FileUtils.listFiles(directory, new String[]{extension}, true);
        }

        System.out.println("Not a directory!: " + directory.getAbsolutePath());
        return null;
    }

    private String getAllFilesFromDirectory(String dir) {
        StringBuilder stringBuilder = new StringBuilder();
        File directory = new File(dir);
        if (directory.isDirectory()) {
            for (File file : directory.listFiles()) {
                stringBuilder = stringBuilder.append(file.getAbsolutePath()).append(System.getProperty("path.separator"));
            }
        }
        return stringBuilder.toString();
    }

    public ValidatorModule getProcessor() {
        return processor;
    }
}