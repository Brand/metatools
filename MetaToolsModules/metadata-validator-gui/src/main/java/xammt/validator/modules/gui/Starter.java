package xammt.validator.modules.gui;

import data.models.configuration.metadata.validator.Configuration;
import xammt.validator.modules.gui.forms.MainFrame;

import javax.swing.*;
import java.io.File;

public class Starter {

    public static final String DEFAULT_CONFIG_PATH = "./configuration/sources/metadata-validator-configuration.xml";
    private static Configuration c;

    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (UnsupportedLookAndFeelException ex) {
            System.out.println(ex.getMessage());
        } catch (ClassNotFoundException ex) {
            System.out.println(ex.getMessage());
        } catch (InstantiationException ex) {
            System.out.println(ex.getMessage());
        } catch (IllegalAccessException ex) {
            System.out.println(ex.getMessage());
        }
        readConfiguration(DEFAULT_CONFIG_PATH);

        MainFrame mainFrame;
        if (c != null) {
            mainFrame = new MainFrame(c);
        } else {
            mainFrame = new MainFrame();
        }
        mainFrame.setVisible(true);
    }

    public static void readConfiguration(String filePath) {

        try {
            c = Configuration.loadConfiguration(new File(filePath));

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static Configuration getConfiguration() {
        return c;
    }
}
