package xammt.validator.modules.gui;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

public class MetadataValidatorStarter {

    private static final String STARTING_COMMAND_START = "javac -cp \"./metadata-validator.jar;./lib/common-library.jar;";
    private static final String STARTING_COMMAND_MIDDLE = "\" -processor xammt.validator.modules.ValidatorModule -proc:only ";
    private String startingCommand;

    public MetadataValidatorStarter(File sourceFileDirectory, File annotationLanguageFile) {
        startingCommand = STARTING_COMMAND_START + annotationLanguageFile.getPath() + STARTING_COMMAND_MIDDLE + createSourceFilesList(sourceFileDirectory);
        System.out.println(startingCommand);
    }

    public boolean run() {
        try {
            Process process = Runtime.getRuntime().exec(startingCommand);
            BufferedReader input = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String line;

            while ((line = input.readLine()) != null) {
                System.out.println(line);
            }
            input.close();

            process.waitFor();
            return true;
        } catch (InterruptedException ex) {
            System.out.println(ex.getMessage());
            return false;
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
            return false;
        }
    }

    private String createSourceFilesList(File dir) {
        try {
            StringBuilder list = new StringBuilder();
            if (dir.isDirectory()) {
                for (File child : dir.listFiles()) {
                    if (child.isDirectory()) {
                        list.append(createSourceFilesList(child));
                    } else {
                        list.append('\"');
                        list.append(child.getCanonicalPath());
                        list.append("\" ");
                    }
                }
            }
            return list.toString();
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }
}
