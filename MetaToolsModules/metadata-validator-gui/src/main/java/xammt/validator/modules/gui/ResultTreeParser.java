package xammt.validator.modules.gui;

import data.models.program.element.IdentificatorParser;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import xammt.validator.modules.results.datamodel.results.ResultState;
import xml.Utility;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.MutableTreeNode;
import javax.xml.xpath.XPathConstants;
import java.io.File;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class ResultTreeParser {

    private Document resultDocument;
    private DefaultTreeModel resultTree;
    private Map<String, DefaultMutableTreeNode> treeNodesMap = new HashMap<String, DefaultMutableTreeNode>();
    private Set<DefaultMutableTreeNode> topLevelTreeNodes = new HashSet<DefaultMutableTreeNode>();

    public ResultTreeParser() {
    }

    public DefaultTreeModel parse(File resultFile) {
        resultDocument = Utility.loadDocument(resultFile);
        // Vytvorenie koreňového uzla stromu.
        DefaultMutableTreeNode root = new DefaultMutableTreeNode("PROGRAM METADATA");
        parseProgramElements();
        sortAndBuildTree();
        // Pridanie uzlov najvyššej úrovne do koreňového uzla.
        for (DefaultMutableTreeNode subtree : topLevelTreeNodes) {
            root.add(subtree);
        }
        resultTree = new DefaultTreeModel(root);
        return resultTree;
    }

    private void parseProgramElements() {
        String xPath = "/results/program-element";
        NodeList nodeList = (NodeList) Utility.evaluateXPathExpression(resultDocument, xPath, XPathConstants.NODESET);
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node node = nodeList.item(i);
            createNode(node, node.getAttributes().getNamedItem("type").getTextContent());
        }
    }

    private void sortAndBuildTree() {
        for (Map.Entry<String, DefaultMutableTreeNode> programElementEntry : treeNodesMap.entrySet()) {
            String treeNodeIdentificator = programElementEntry.getKey();
            String parentTreeNodeIdentificator = IdentificatorParser.getEnclosingElementQualifiedId(treeNodeIdentificator);
            DefaultMutableTreeNode treeNode = programElementEntry.getValue();
            if (parentTreeNodeIdentificator != null) {
                DefaultMutableTreeNode parentNode = treeNodesMap.get(parentTreeNodeIdentificator);
                if (parentNode != null) {
                    parentNode.add(treeNode);
                } else {
                    topLevelTreeNodes.add(treeNode);
                }
            } else {
                topLevelTreeNodes.add(treeNode);
            }
        }
    }

    private void createNode(Node documentNode, String nodeType) {
        String qualifiedIdentificator = documentNode.getAttributes().getNamedItem("identificator").getTextContent();
        DefaultMutableTreeNode treeNode = treeNodesMap.get(qualifiedIdentificator);
        if (treeNode == null) {
            treeNode = new DefaultMutableTreeNode("<html><font color=blue>" + getTreeNodeDisplayName(nodeType) + "</font> " + getTreeNodeDisplayIdentificator(qualifiedIdentificator, nodeType) + "</html>");
            treeNodesMap.put(qualifiedIdentificator, treeNode);
        }
        createAndAppendMetadataTreeNodes(treeNode, documentNode.getChildNodes());
    }

    private void createAndAppendMetadataTreeNodes(DefaultMutableTreeNode treeNode, NodeList documentMetadataNodes) {
        for (int i = 0; i < documentMetadataNodes.getLength(); i++) {
            Node child = documentMetadataNodes.item(i);

            MutableTreeNode childNode = createResultNode(child);
            if (childNode != null) treeNode.add(childNode);
        }
    }

    private MutableTreeNode createResultNode(Node documentNode) {
        if (documentNode.getNodeType() == Node.ELEMENT_NODE) {
            Element element = (Element) documentNode;

            String attributeIdentificator = element.getAttribute("attribute-id");
            String state = element.getAttribute("state");
            String type = element.getAttribute("type");

            DefaultMutableTreeNode treeNode = new DefaultMutableTreeNode("<html><font color=" + getColorForStateString(state) + ">@" + attributeIdentificator + "</font></html>");
            treeNode.add(new DefaultMutableTreeNode("<html><font color=" + getColorForStateString(state) + ">" + "Relationship: " + type + "</font></html>"));

            NodeList infoList = element.getElementsByTagName("info");
            if (infoList.getLength() > 0) {
                for (int i = 0; i < infoList.getLength(); i++) {
                    Element infoElement = (Element) infoList.item(i);
                    treeNode.add(new DefaultMutableTreeNode(infoElement.getAttribute("name") + infoElement.getAttribute("value")));
                }
            }

            return treeNode;
        }

        return null;
    }

    private String getColorForStateString(String state) {
        if (state == null || "".equals(state.trim())) {
            throw new RuntimeException("Undefined state");
        }

        ResultState resultState = ResultState.getStateByName(state);
        String color = resultState.equals(ResultState.CONSISTENT) ? "green" : "red";
        return color;
    }

    private String getTreeNodeDisplayIdentificator(String qualifiedTreeNodeIdentificator, String nodeType) {
        if (nodeType.equals("PACKAGE")) {
            return qualifiedTreeNodeIdentificator;
        } else {
            return IdentificatorParser.getSimpleId(qualifiedTreeNodeIdentificator);
        }
    }

    private String getTreeNodeDisplayName(String nodeType) {
        if (nodeType.equals("ANNOTATION_METHOD")
                || nodeType.equals("INTERFACE_METHOD")
                || nodeType.equals("CLASS_METHOD")) {
            return "method";


        } else {
            return nodeType.toLowerCase();

        }
    }
}
