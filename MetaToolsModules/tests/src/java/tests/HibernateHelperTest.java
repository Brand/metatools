package tests;

import data.models.configuration.metadata.validator.Configuration;
import org.junit.Test;
import xammt.binding.datamodel.BindingsList;
import xammt.binding.datamodel.ConfigWrapper;
import xammt.binding.generator.ConfigParser;
import xammt.binding.generator.ProgramMetadataModelBindingGenerator;
import xammt.binding.generator.XMLMetadatadBindingGenerator;
import xammt.binding.generator.helpers.Helper;
import xammt.binding.hibernate.HibernateHelper;
import xammt.validator.modules.gui.AnnotationProcessorRunner;
import xammt.validator.modules.gui.Starter;
import xammt.validator.modules.metadata.loader.datamodel.ProgramMetadataModel;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: brano
 */
public class HibernateHelperTest {
    private static List<File> hbmFiles;
    private static List<ConfigWrapper> configs;
    private static Helper helper;
    private static ProgramMetadataModel model;

    /**
     * prepare test data
     */
    static {
        hbmFiles = new ArrayList<File>();
        hbmFiles.add(new File("EmployeeScheme.hbm.xml"));
//        hbmFiles.add(new File("EmployeeDetail.hbm.xml"));
        helper = new HibernateHelper();
        ConfigParser configParser = new ConfigParser(new File("hibernate.config"));
        configs = configParser.parse();
        readModel();
    }

    private static void readModel() {
        Starter.readConfiguration("/home/brano/IdeaProjects/metatools/MetaToolsModules/metadata-validator-gui/sample_data/sample_config.xml");
        Configuration configuration = Starter.getConfiguration();
        AnnotationProcessorRunner annotationProcessorRunner = new AnnotationProcessorRunner(configuration);
        annotationProcessorRunner.runAnnotationProcessor();
        model = (ProgramMetadataModel) annotationProcessorRunner.getProcessor().getAnnotationsModule().getModelLoader().getModel();
    }

    @Test
    public void testClassReading() {
        XMLMetadatadBindingGenerator generatorFromXML = new XMLMetadatadBindingGenerator(hbmFiles, configs, helper);
        ProgramMetadataModelBindingGenerator generatorFromModel = new ProgramMetadataModelBindingGenerator(hbmFiles, helper, configs, model);

        BindingsList fromModel = generatorFromModel.generate();
//        System.out.println(fromModel.asXMLElement());
        BindingsList fromXMLs = generatorFromXML.generate();
//        System.out.println(fromXMLs.asXMLElement());

        BindingsList bindingsList = BindingsList.mergeBindingsList(fromXMLs, fromModel);
        System.out.println(bindingsList.asXMLElement());
    }
}
