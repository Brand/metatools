package tests;

import junit.framework.Assert;
import org.dom4j.DocumentFactory;
import org.jdom2.*;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;
import org.jdom2.xpath.XPathExpression;
import org.jdom2.xpath.XPathFactory;
import org.jdom2.xpath.XPathHelper;
import org.junit.Test;
import org.w3c.dom.*;
import org.xml.sax.SAXException;
import xml.Utility;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathConstants;
import java.io.IOException;
import java.util.List;

public class JDom2Test {

    @Test
    public void testNewInstance() {
        try {
            XPathFactory factory = XPathFactory.instance();
            XPathExpression<Object> xpath = factory.compile("/hibernate-mapping/class/id");
            SAXBuilder sax = new SAXBuilder();
            Document document = sax.build("Employee.hbm.xml");
            List<Object> evaluate = xpath.evaluate(document);
            for (Object o : evaluate) {
                if (o instanceof Element) {
                    xpath = factory.compile("generator");
                    evaluate = xpath.evaluate(o);
                    for (Object o2 : evaluate) {
                        if (o2 instanceof Attribute) {
                            Attribute to = (Attribute) o2;
                            System.out.println(XPathHelper.getAbsolutePath(to));
                        } else if (o2 instanceof Content) {
                            Content to = (Content) o2;
                            System.out.println(XPathHelper.getAbsolutePath(to));
                        }
                    }
                }
            }
        } catch (JDOMException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    @Test
    public void testEquivalentXpathsJDOM2() throws JDOMException, IOException {
        XPathFactory factory = XPathFactory.instance();
        SAXBuilder sax = new SAXBuilder();
        Document document = sax.build("Employee.hbm.xml");
        XPathExpression<Object> xpath1 = factory.compile("/hibernate-mapping/class[@name='hibexam.Employee'][@table='EMPLOYEE']/property[@column='firstname'][@name='firstname']/@column");
        XPathExpression<Object> xpath2 = factory.compile("/hibernate-mapping/class[1]/property[1]/@column");

        List<Object> absoluteElements = xpath1.evaluate(document);
        List<Object> relativeElements = xpath2.evaluate(document);

        Assert.assertEquals("Lists are not equal!", absoluteElements, relativeElements);
    }

    @Test
    public void testEquivalentXpathsW3CDOM() throws IOException, ParserConfigurationException, SAXException {
        DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
        org.w3c.dom.Document document = docBuilder.parse("Employee.hbm.xml");
        Node node1 = (Node) Utility.evaluateXPathExpression(document, "/hibernate-mapping/class[@name='hibexam.Employee'][@table='EMPLOYEE']/property[@column='firstname'][@name='firstname']/@column", XPathConstants.NODE);
        Node node2 = (Node) Utility.evaluateXPathExpression(document, "/hibernate-mapping/class[1]/property[1]/@column", XPathConstants.NODE);

        Assert.assertEquals("Nodes are not equal!", node1, node2);
    }

}

