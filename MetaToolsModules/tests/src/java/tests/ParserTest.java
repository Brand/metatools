package tests;

import org.junit.Assert;
import org.junit.Test;
import xammt.binding.datamodel.Binding;
import xammt.binding.datamodel.BindingsList;
import xammt.binding.datamodel.ConfigWrapper;
import xammt.binding.generator.ConfigParser;


import java.io.File;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: brano
 */
public class ParserTest {

    @Test
    public void parserTest() {
        ConfigParser configParser = new ConfigParser(new File("hibernate.config"));
        List<ConfigWrapper> configs = configParser.parse();

        Assert.assertTrue("Config was not parsed successfully!", configs.size() == 1);

        ConfigWrapper configWrapper = configs.get(0);
        Binding binding = new Binding(configWrapper.getAnnotationPath(), configWrapper.getAnnotationType().toString(), configWrapper.getProgramElementType().toString(), "lala.lula", configWrapper.getProgramElementIdentifierXpath(), "lala.hbm.xml", "Equality(p1,p2,p3)");
        BindingsList bindingList = new BindingsList("xammt.validator.modules.metadata.loader.xml.TreePatternModelLoader");
        bindingList.addBinding(binding);
        String x = bindingList.asXMLElement();

        System.out.println(x);
    }
}
