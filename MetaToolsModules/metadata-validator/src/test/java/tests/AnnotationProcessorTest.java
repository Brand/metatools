package tests;

/**
 * Created by IntelliJ IDEA.
 * User: Brano
 * Date: 16.12.2012
 * Time: 20:15
 */

import org.junit.Test;
import xammt.validator.modules.ValidatorModule;

import javax.annotation.processing.AbstractProcessor;
import javax.tools.JavaCompiler;
import javax.tools.JavaFileObject;
import javax.tools.StandardJavaFileManager;
import javax.tools.ToolProvider;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class AnnotationProcessorTest {
    @Test
    public void testStartProcessor() {
        //Get an instance of java compiler
        JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();

        //Get a new instance of the standard file manager implementation
        StandardJavaFileManager fileManager = compiler.getStandardFileManager(null, null, null);

        Iterable<? extends JavaFileObject> compilationUnits1 =
                fileManager.getJavaFileObjects("C:\\Users\\Brano\\Desktop\\target\\data\\java src\\entity\\Person.java");

        List<String> optionList = new ArrayList<String>();
        // set compiler's classpath to be same as the runtime's
        optionList.addAll(Arrays.asList("-classpath", "lib/*;C:\\Users\\Brano\\Desktop\\target\\data\\annotations src\\Annotation source.jar"));

        JavaCompiler.CompilationTask task = compiler.getTask(null, fileManager, null,
                optionList, null, compilationUnits1);

        // Create a list to hold annotation processors
        LinkedList<AbstractProcessor> processors = new LinkedList<AbstractProcessor>();

        // Add an annotation processor to the list
        processors.add(new ValidatorModule());

        // Set the annotation processor to the compiler task
        task.setProcessors(processors);

        task.call();
    }
}
