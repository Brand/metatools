package tests;

/**
 * Created with IntelliJ IDEA.
 * User: brano
 */

import org.junit.Test;
import xammt.validator.modules.results.datamodel.results.ConflictResult;
import xammt.validator.modules.results.datamodel.results.EqualityResult;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class EqualityTests {

    @Test
    public void testResultsEquals() {
        EqualityResult equalityResult1 = new EqualityResult("foo", "bar");
        EqualityResult equalityResult2 = new EqualityResult("foo", "baR");

        assertFalse("These values should not be equal!", equalityResult1.equals(equalityResult2));

        ConflictResult conflictResult1 = new ConflictResult("foo", "country", "COUNTRY");
        ConflictResult conflictResult2 = new ConflictResult("foo", "street", "STREET");
        ConflictResult conflictResult3 = new ConflictResult("foo", "STREET", "street");

        assertFalse("These values should not be equal!", conflictResult1.equals(conflictResult2));
        assertFalse("These values should not be equal!", conflictResult1.equals(conflictResult3));
        assertTrue("These values should be equal!", conflictResult2.equals(conflictResult3));
    }
}
