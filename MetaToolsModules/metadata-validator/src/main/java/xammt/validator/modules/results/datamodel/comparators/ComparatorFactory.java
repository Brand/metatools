package xammt.validator.modules.results.datamodel.comparators;

import org.reflections.Reflections;
import xammt.validator.datamodel.binding.ComparePolicyPOJO;
import xammt.validator.modules.results.datamodel.comparators.AbstractComparator;
import xammt.validator.modules.results.datamodel.results.MissingResult;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: brano
 */
public class ComparatorFactory {
    private List<AbstractComparator> comparators;
    private static ComparatorFactory instance;
    private Reflections reflections;

    private ComparatorFactory() {
        comparators = new ArrayList<AbstractComparator>(3);
        loadComparators();
    }

    private ComparatorFactory(Reflections reflections) {
        comparators = new ArrayList<AbstractComparator>(3);
        this.reflections = reflections;
        loadComparators();
    }

    private void loadComparators() {
        if (reflections == null) {
            reflections = new Reflections("xammt.validator.modules.results.datamodel");
        }

        Set<Class<? extends AbstractComparator>> comparatorsClasses =
                reflections.getSubTypesOf(AbstractComparator.class);

        for (Class<? extends AbstractComparator> comparatorClass : comparatorsClasses) {
            try {
                Constructor constructor = comparatorClass.getDeclaredConstructor();
                comparators.add(((AbstractComparator) constructor.newInstance()));
            } catch (NoSuchMethodException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            } catch (InvocationTargetException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            } catch (InstantiationException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            } catch (IllegalAccessException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
    }

    public static ComparatorFactory getInstance() {
        if (instance == null) {
            instance = new ComparatorFactory();
        }
        return instance;
    }

    public static void setReflectionAndInit(Reflections reflections) {
        instance = new ComparatorFactory(reflections);
    }

    public Reflections getReflections() {
        return reflections;
    }

    public AbstractComparator getComparator(String attributeName, Object annotationValue, Object xmlValue, MissingResult.Form counterForm, ComparePolicyPOJO comparePolicy) {
        AbstractComparator comparator = this.getComparator(comparePolicy.getComparator());

        if (comparator == null) {
            return null;
        }

        comparator.setAttributeName(attributeName);
        comparator.setAttributeValue(annotationValue);
        comparator.setXmlValue(xmlValue);
        comparator.setCounterForm(counterForm);
        comparator.setComparePolicyPOJO(comparePolicy);

        return comparator;
    }

    private AbstractComparator getComparator(String comparatorName) {
        for (AbstractComparator comparator : comparators) {
            if (comparator.getName().equals(comparatorName)) {
                return comparator;
            }
        }

        return null;
    }

    public static void main(String[] args) {
        System.out.println("test factory");
        ComparatorFactory.getInstance();
    }
}
