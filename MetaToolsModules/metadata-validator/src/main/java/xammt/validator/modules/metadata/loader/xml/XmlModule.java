package xammt.validator.modules.metadata.loader.xml;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;
import xammt.validator.datamodel.xml.documents.DocumentElement;
import xammt.validator.datamodel.xml.documents.DocumentsContainer;
import xammt.validator.interfaces.ModelLoader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class XmlModule {

    private ModelLoader modelLoader;

    public static DocumentsContainer loadDocumentContainer(File metadataRootDirectory) {
        try {
            DocumentsContainer documentsContainer = new DocumentsContainer(metadataRootDirectory.getCanonicalPath().replace(File.separatorChar, '/'));
            for (File file : metadataRootDirectory.listFiles()) {
                if (file.isDirectory()) {
                    documentsContainer.getDocumentElements().putAll(loadDocumentElements(file));
                } else if (file.isFile()) {
                    DocumentElement documentElement = loadDocumentElement(file);
                    documentsContainer.getDocumentElements().put(documentElement.getCanonicalPath(), documentElement);
                }
            }
            return documentsContainer;
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
        return null;
    }

    public static DocumentsContainer loadDocumentContainer(File metadataRootDirectory, List<String> filePaths) {
        try {
            DocumentsContainer documentsContainer = new DocumentsContainer(metadataRootDirectory.getCanonicalPath().replace(File.separatorChar, '/'));
            for (String filePath : filePaths) {
                File file = new File(filePath);
                if (file.isFile()) {
                    DocumentElement documentElement = loadDocumentElement(file);
                    documentsContainer.getDocumentElements().put(documentElement.getCanonicalPath(), documentElement);
                }
            }
            return documentsContainer;
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
        return null;
    }

    public static Map<String, DocumentElement> loadDocumentElements(File directory) {
        Map<String, DocumentElement> documentElements = new HashMap<String, DocumentElement>();
        for (File file : directory.listFiles()) {
            if (file.isDirectory()) {
                documentElements.putAll(loadDocumentElements(file));
            } else if (file.isFile()) {
                DocumentElement documentElement = loadDocumentElement(file);
                documentElements.put(documentElement.getCanonicalPath(), documentElement);
            }
        }
        return documentElements;
    }

    public static DocumentElement loadDocumentElement(File file) {
        try {
            DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
            Document document = docBuilder.parse(file);
            return new DocumentElement(file.getCanonicalPath().replace(File.separatorChar, '/'), document);
        } catch (SAXException ex) {
            System.out.println(ex.getMessage());
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        } catch (ParserConfigurationException ex) {
            System.out.println(ex.getMessage());
        }
        return null;
    }    

    public ModelLoader getModelLoader() {
        return modelLoader;
    }

    public void setModelLoader(ModelLoader modelLoader) {
        this.modelLoader = modelLoader;
    }
}
