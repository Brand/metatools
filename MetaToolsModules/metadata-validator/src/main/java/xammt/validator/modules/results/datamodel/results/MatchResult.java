package xammt.validator.modules.results.datamodel.results;

import org.eclipse.persistence.oxm.annotations.XmlDiscriminatorValue;

/**
 * Created with IntelliJ IDEA.
 * User: brano
 */

@XmlDiscriminatorValue("Match")
public class MatchResult extends Result {
    public MatchResult() {
        super();
    }

    public MatchResult(String attributeIdentificator, String xmlValue, String expectedValue) {
        super(ResultState.CONSISTENT, "Match", attributeIdentificator);
        addInfo("XML value = ", xmlValue);
        addInfo("Expected value = ", expectedValue);
    }
}
