package xammt.validator.modules.metadata.loader.xml;

import api.ProgramElementInterface;
import api.ProgramMetadataModelInterface;
import metadata.AnnotationType;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import xammt.validator.datamodel.binding.ComparePolicyPOJO;
import xammt.validator.datamodel.binding.MetadataBinding;
import xammt.validator.datamodel.xml.documents.DocumentElement;
import xammt.validator.datamodel.xml.documents.DocumentsContainer;
import xammt.validator.interfaces.PatternBasedModelLoader;
import xammt.validator.modules.metadata.loader.datamodel.ValuePolicyContainer;
import xammt.validator.modules.metadata.loader.xml.TreePatternLanguageUtility.KeyWord;
import xml.Utility;

import javax.xml.xpath.XPathConstants;
import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TreePatternModelLoader implements PatternBasedModelLoader {

    private Document bindingsDocument;
    private ProgramMetadataModelInterface programModel;
    private DocumentsContainer metadataDocumentsContainer;
    // Maps with aliases for annotation path, key is alias and value is qualified name
    private Map<String, String> aliasesMap;

    @Override
    public void loadModel() {
        aliasesMap = loadAliases(bindingsDocument);
        for (Entry<String, ProgramElementInterface> programElementEntry : programModel.getProgramElements().entrySet()) {
            ProgramElementInterface programElementInterface = programElementEntry.getValue();
            Map<KeyWord, String> parameterMap = TreePatternLanguageUtility.fillParameterMap(programElementInterface, programModel);
            for (Entry<String, MetadataBinding> attributeBindingEntry : TreePatternLanguageUtility.getMetadataBindings(bindingsDocument, programElementInterface, aliasesMap).entrySet()) {
                
                String id = attributeBindingEntry.getValue().getAnnotationId();
                String xPath = TreePatternLanguageUtility.doSemanticEnrichment(parameterMap, attributeBindingEntry.getValue().getxPath());
                String file = attributeBindingEntry.getValue().getFileLocation();
                ComparePolicyPOJO comparePolicy = attributeBindingEntry.getValue().getComparePolicy();
                boolean isVoid = attributeBindingEntry.getValue().getType().equals(AnnotationType.ANNOTATION);

                List<DocumentElement> documentElements = metadataDocumentsContainer.searchDocument(file);
                if (documentElements.size() == 1) {
                    Node node = (Node) xml.Utility.evaluateXPathExpression(documentElements.get(0).getDocument(), xPath, XPathConstants.NODE);
                    if (node != null) {
                        programElementInterface.getMetadata().put(id, new ValuePolicyContainer(comparePolicy, node, isVoid));
                    }
                } else if (documentElements.size() > 0) {
                    String regex = TreePatternLanguageUtility.getPackageBasedPathRegex(programModel, programElementInterface, metadataDocumentsContainer.getMetadataRootPath());
                    if (regex.equals(metadataDocumentsContainer.getMetadataRootPath())) {
                        Node node = (Node) xml.Utility.evaluateXPathExpression(
                                metadataDocumentsContainer.getDocumentElements().get(metadataDocumentsContainer.getMetadataRootPath() + file).getDocument(),
                                xPath, XPathConstants.NODE);
                        if (node != null) {
                            programElementInterface.getMetadata().put(id, new ValuePolicyContainer(comparePolicy, node, isVoid));
                        }
                    } else {
                        Pattern pattern = Pattern.compile(regex, Pattern.DOTALL);
                        for (DocumentElement documentElement : documentElements) {
                            Matcher matcher = pattern.matcher(documentElement.getCanonicalPath());
                            if (matcher.matches()) {
                                Node node = (Node) xml.Utility.evaluateXPathExpression(
                                        documentElement.getDocument(),
                                        xPath, XPathConstants.NODE);
                                if (node != null) {
                                    programElementInterface.getMetadata().put(id, new ValuePolicyContainer(comparePolicy, node, isVoid));
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    private Map<String, String> loadAliases(Document bindingsDocument) {
        Map<String, String> aliasesMap = new HashMap<String, String>();
        NodeList nodeList = (NodeList) Utility.evaluateXPathExpression(bindingsDocument, "bindings/alias", XPathConstants.NODESET);
        if (nodeList != null && nodeList.getLength() > 0) {
            for(int i = 0; i < nodeList.getLength(); i++) {
                String alias = nodeList.item(i).getAttributes().getNamedItem("id").getTextContent();
                String qualifiedName = nodeList.item(i).getChildNodes().item(0).getTextContent();
                aliasesMap.put(alias, qualifiedName);
            }
        }

        return aliasesMap;
    }

    @Override
    public ProgramMetadataModelInterface getModel() {
        return programModel;
    }

    @Override
    public void setProgramModel(ProgramMetadataModelInterface programModel) {
        this.programModel = programModel;
    }

    @Override
    public void setXmlMetadataSources(File metadataSource) {
        metadataDocumentsContainer = XmlModule.loadDocumentContainer(metadataSource);
    }

    @Override
    public void setXmlMetadataSources(DocumentsContainer documentsContainer) {
        metadataDocumentsContainer = documentsContainer;
    }

    @Override
    public void setBindingDocument(Document bindingDocument) {
        this.bindingsDocument = bindingDocument;
    }
}
