package xammt.validator.modules.metadata.loader.xml;

import api.ProgramElementInterface;
import api.ProgramMetadataModelInterface;
import metadata.AnnotationType;
import org.w3c.dom.*;
import xammt.validator.datamodel.binding.ComparePolicyPOJO;
import xammt.validator.datamodel.binding.MetadataBinding;

import javax.xml.xpath.XPathConstants;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class TreePatternLanguageUtility {

    // Language keywords.
    public enum KeyWord {

        simple_package_id,
        qualified_package_id,
        simple_type_id,
        qualified_type_id,
        simple_field_id,
        qualified_field_id,
        simple_method_signature,
        qualified_method_signature
    }

    public static Map<KeyWord, String> fillParameterMap(ProgramElementInterface programElementInterface, ProgramMetadataModelInterface programModel) {
        Map<KeyWord, String> parameterMap = new EnumMap<KeyWord, String>(KeyWord.class);
        if (programElementInterface.getType().isPackage()) {
            parameterMap.put(KeyWord.qualified_package_id, programElementInterface.getQualifiedIdentificator());
            parameterMap.put(KeyWord.simple_package_id, programElementInterface.getSimpleIdentificator());
        } else if (programElementInterface.getType().isType() || programElementInterface.getType().isAnnotation()) {
            ProgramElementInterface packageProgramElementInterface = programModel.getPackageElement(programElementInterface.getQualifiedIdentificator());
            parameterMap.put(KeyWord.qualified_package_id, packageProgramElementInterface.getQualifiedIdentificator());
            parameterMap.put(KeyWord.simple_package_id, packageProgramElementInterface.getSimpleIdentificator());
            parameterMap.put(KeyWord.qualified_type_id, programElementInterface.getQualifiedIdentificator());
            parameterMap.put(KeyWord.simple_type_id, programElementInterface.getSimpleIdentificator());
        } else if (programElementInterface.getType().isMember()) {
            ProgramElementInterface packageProgramElementInterface = programModel.getPackageElement(programElementInterface.getQualifiedIdentificator());
            ProgramElementInterface typeProgramElementInterface = programModel.getTypeElement(programElementInterface.getQualifiedIdentificator());
            parameterMap.put(KeyWord.qualified_package_id, packageProgramElementInterface.getQualifiedIdentificator());
            parameterMap.put(KeyWord.simple_package_id, packageProgramElementInterface.getSimpleIdentificator());
            parameterMap.put(KeyWord.qualified_type_id, typeProgramElementInterface.getQualifiedIdentificator());
            parameterMap.put(KeyWord.simple_type_id, typeProgramElementInterface.getSimpleIdentificator());
            if (programElementInterface.getType().isMethod()) {
                parameterMap.put(KeyWord.qualified_method_signature, programElementInterface.getQualifiedIdentificator());
                parameterMap.put(KeyWord.simple_method_signature, programElementInterface.getSimpleIdentificator());
            } else if (programElementInterface.getType().isField()) {
                parameterMap.put(KeyWord.qualified_field_id, programElementInterface.getQualifiedIdentificator());
                parameterMap.put(KeyWord.simple_field_id, programElementInterface.getSimpleIdentificator());
            }
        }
        return parameterMap;
    }

    public static Map<String, MetadataBinding> getMetadataBindings(Document treePaternBindingDocument, ProgramElementInterface programElementInterface, Map<String, String> aliasesMap) {
        String metadataBindingXpath = "/bindings/metadata[@target-type='" + programElementInterface.getType().name() + "']";
        Map<String, MetadataBinding> metadataBindings = new HashMap<String, MetadataBinding>();
        NodeList nodeList = (NodeList) xml.Utility.evaluateXPathExpression(treePaternBindingDocument, metadataBindingXpath, XPathConstants.NODESET);
        if (nodeList != null && (nodeList.getLength() > 0)) {
            for (int i = 0; i < nodeList.getLength(); i++) {
                Node item = nodeList.item(i);
                if (item != null && item.getNodeType() == Node.ELEMENT_NODE) {
                    Element element = (Element) item;
                    String id = element.getAttribute("id");
                    Node xpath1tem = element.getElementsByTagName("xpath").item(0);
                    Node fileItem = element.getElementsByTagName("file").item(0);

                    if (id != null && xpath1tem != null && fileItem != null) {
                        String xpath = xpath1tem.getTextContent();
                        String file = fileItem.getTextContent();
                        String target = getTarget(nodeList.item(i).getAttributes());

                        for (Entry<String, String> alias : aliasesMap.entrySet()) {
                            if (id.contains(alias.getKey())) {
                                id = id.replaceAll(alias.getKey(), alias.getValue());
                            }
                        }

                        NodeList comparePolicyNodeList = element.getElementsByTagName("comparePolicy");
                        Element comparePolicyElement = (Element) comparePolicyNodeList.item(0);
                        ComparePolicyPOJO comparePolicyObj = parseComparePolicyObject(comparePolicyElement);

                        AnnotationType type = AnnotationType.valueOf(item.getAttributes().getNamedItem("type").getTextContent());
                        if (programElementInterface.getQualifiedIdentificator().equals(target)) {
                            metadataBindings.put(id, new MetadataBinding(id, xpath, file, comparePolicyObj, type));
                        }
                    }
                }
            }
        }
        return metadataBindings;
    }

    private static ComparePolicyPOJO parseComparePolicyObject(Element comparePolicyElement) {
        if (comparePolicyElement == null) {
            //TODO somehow improve
            return new ComparePolicyPOJO("Equality");
        }

        String comparator = comparePolicyElement.getAttribute("comparator");
        ComparePolicyPOJO policyObj = new ComparePolicyPOJO(comparator);

        NodeList parameters = comparePolicyElement.getElementsByTagName("parameter");
        for (int i = 0; i < parameters.getLength(); i++) {
            policyObj.addParameter(i, parameters.item(i).getTextContent());
        }

        return policyObj;
    }

    private static String getTarget(NamedNodeMap attributesMap) {
        String target = "";
        for (int i = 0; i < attributesMap.getLength(); i++) {
            if (attributesMap.item(i).getNodeName().equals("target")) {
                target = attributesMap.item(i).getNodeValue();
            }
        }
        return target;
    }

    public static String doSemanticEnrichment(Map<KeyWord, String> parameterMap, String parametrizedString) {
        String enrichedString = parametrizedString;
        for (Entry<KeyWord, String> parameterEntry : parameterMap.entrySet()) {
            enrichedString = enrichedString.replaceAll(parameterEntry.getKey().name(), parameterEntry.getValue());
        }
        return enrichedString;
    }

    public static String getPackageBasedPathRegex(ProgramMetadataModelInterface programModel, ProgramElementInterface programElementInterface, String rootPath) {
        String packageId = programModel.getPackageElement(programElementInterface.getQualifiedIdentificator()).getQualifiedIdentificator();
        String[] simplePackageIds = packageId.split("\\.");
        String regex = rootPath;

        for (int i = 0; i < simplePackageIds.length - 1; i++) {
            regex = regex.concat("/" + simplePackageIds[i] + "(/.*)*");
        }
        return regex;
    }
}
