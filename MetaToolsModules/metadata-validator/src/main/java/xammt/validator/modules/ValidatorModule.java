package xammt.validator.modules;

import api.ProgramElementInterface;
import api.ProgramMetadataModelInterface;
import data.models.configuration.metadata.validator.Configuration;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import xammt.validator.datamodel.binding.ComparePolicyPOJO;
import xammt.validator.datamodel.xml.documents.DocumentsContainer;
import xammt.validator.interfaces.ModelLoader;
import xammt.validator.interfaces.PatternBasedModelLoader;
import xammt.validator.modules.metadata.loader.ProgramModelLoader;
import xammt.validator.modules.metadata.loader.annotations.AnnotationsLoader;
import xammt.validator.modules.metadata.loader.annotations.AnnotationsModule;
import xammt.validator.modules.metadata.loader.datamodel.ProgramMetadataModel;
import xammt.validator.modules.metadata.loader.datamodel.ValuePolicyContainer;
import xammt.validator.modules.metadata.loader.xml.XmlModule;
import xammt.validator.modules.results.ResultGenerator;
import xammt.validator.modules.results.datamodel.comparators.AbstractComparator;
import xammt.validator.modules.results.datamodel.comparators.ComparatorFactory;
import xammt.validator.modules.results.datamodel.results.MissingResult;
import xammt.validator.modules.results.datamodel.results.ProgramElementResults;
import xammt.validator.modules.results.datamodel.results.Result;
import xml.Utility;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.TypeElement;
import javax.xml.xpath.XPathConstants;
import java.io.File;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

@SupportedSourceVersion(SourceVersion.RELEASE_6)
@SupportedAnnotationTypes("*")
public class ValidatorModule extends AbstractProcessor {

    // Module Inputs.
    private Configuration configuration;

    // Sub modules.
    private AnnotationsModule annotationsModule = new AnnotationsModule();
    private XmlModule xmlModule = new XmlModule();

    // Program metadata models for comparing.
    private ProgramMetadataModelInterface annotationsMetadataModel;
    private ProgramMetadataModelInterface xmlMetadataModel;

    //clear model
    private ModelLoader programModelLoader;

    private ResultGenerator resultGenerator;
    private Document bindingsDocument;

    public ValidatorModule() {
    }

    public void validateMetadataAndCreateResults(String outputDirectoryPath, String outputFilePath) {
        Set<ProgramElementResults> resultElements = validateMetadataAndCreateProgramElementResults(annotationsMetadataModel, xmlMetadataModel);
        //System.out.println(resultElements);
        resultGenerator = new ResultGenerator(resultElements, outputDirectoryPath, outputFilePath);
        resultGenerator.generate();
    }

    private Set<ProgramElementResults> validateMetadataAndCreateProgramElementResults(ProgramMetadataModelInterface programModel, ProgramMetadataModelInterface counterProgramModel) {
        Set<ProgramElementResults> resultElements = new HashSet<ProgramElementResults>();
        for (Entry<String, ProgramElementInterface> programElementEntry : programModel.getProgramElements().entrySet()) {
            ProgramElementInterface programElement = programElementEntry.getValue();
            ProgramElementInterface counterProgramElement = counterProgramModel.getProgramElements().get(programElement.getQualifiedIdentificator());

            Set<Result> attributeComparingResults = new HashSet<Result>();
            if (programElement.getMetadata().size() > 0) {
                attributeComparingResults = validateMetadataAndCreateAttributeComparingResults(programElement, programElement.getMetadata(), counterProgramElement.getMetadata(), MissingResult.Form.XML);
            }

            if (counterProgramElement.getMetadata().size() > 0) {
                attributeComparingResults.addAll(validateMetadataAndCreateAttributeComparingResults(programElement, counterProgramElement.getMetadata(), programElement.getMetadata(), MissingResult.Form.ATTRIBUTE));
                resultElements.add(new ProgramElementResults(programElement.getQualifiedIdentificator(), programElement.getType(), attributeComparingResults));
            } else {
                resultElements.add(new ProgramElementResults(programElement.getQualifiedIdentificator(), programElement.getType(), attributeComparingResults.size() > 0 ? attributeComparingResults : null));
            }
        }
        return resultElements;
    }

    private Set<Result> validateMetadataAndCreateAttributeComparingResults(ProgramElementInterface currentProgramElement, Map<String, Object> metadata, Map<String, Object> counterMetadata, MissingResult.Form counterForm) {
        Set<Result> attributeComparingResults = new HashSet<Result>();
        String qualifiedIdentificator = currentProgramElement.getQualifiedIdentificator();
        for (Entry<String, Object> attributeEntry : metadata.entrySet()) {
            String attributeName = attributeEntry.getKey();

            ComparePolicyPOJO comparePolicy;
            Object metadataValue = attributeEntry.getValue();
            Object counterMetadataValue = counterMetadata.get(attributeName);
            ValuePolicyContainer valuePolicyContainer;

            Object xmlValue;
            Object annotationValue;

            if (metadataValue instanceof ValuePolicyContainer) {
                valuePolicyContainer = (ValuePolicyContainer) metadataValue;
                comparePolicy = valuePolicyContainer.getComparePolicy();
                xmlValue = valuePolicyContainer.getValue();
                annotationValue = counterMetadataValue;
            } else if (counterMetadataValue instanceof ValuePolicyContainer) {
                valuePolicyContainer = (ValuePolicyContainer) counterMetadataValue;
                comparePolicy = valuePolicyContainer.getComparePolicy();
                xmlValue = valuePolicyContainer.getValue();
                annotationValue = metadataValue;
            } else {
                System.out.println("No binding for: " + attributeName);
                if (existsBinding(qualifiedIdentificator, attributeName)) {
                    attributeComparingResults.add(new MissingResult(attributeName, counterForm, (counterMetadataValue == null ? metadataValue : counterMetadataValue)));
                }
                continue;
            }

            AbstractComparator comparator = ComparatorFactory.getInstance().getComparator(attributeName, annotationValue, xmlValue, counterForm, comparePolicy);
            attributeComparingResults.add(comparator.compare());
        }
        return attributeComparingResults;
    }

    private boolean existsBinding(String qualifiedIdentificator, String attributeName) {
        String xpath = "/bindings/metadata[@id=\"" + attributeName + "\"][@target=\"" + qualifiedIdentificator + "\"]";
        return Utility.evaluateXPathExpression(bindingsDocument, xpath, XPathConstants.NODE) != null;
    }

    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        if (!roundEnv.processingOver()) {

            long startTime = System.currentTimeMillis();
            System.out.println("\nStarted.");

            try {
                configuration = Configuration.loadConfiguration(new File("./configuration/sources/metadata-validator-configuration.xml"));
                System.out.println("Validator configuration loaded.");

                programModelLoader = new ProgramModelLoader(roundEnv, processingEnv.getElementUtils());
                programModelLoader.loadModel();
                //priradi model
                annotationsMetadataModel = programModelLoader.getModel();
                xmlMetadataModel = (ProgramMetadataModel) programModelLoader.getModel().clone();
                System.out.println("Program model created.");

                //uncomment for enabling annotation filtering and change constructor for AnnotationsLoader,
                //call constructor where annotationsId is one of the parameters
                /*PmtdlDefinition xamclDefinition = PmtdlDefinition.load(new File(configuration.getPmtdlFile()));
                annotationsModule.loadAnnotationNames(xamclDefinition);

                Set<? extends TypeElement> filteredAnnotations = annotationsModule.filterAnnotations(annotations);
                System.out.println("Annotations filtered.");
                Set<String> annotationsId = new HashSet<String>();
                for (Annotation annotationId : xamclDefinition.getAnnotation()) {
                    annotationsId.add(annotationId.getId());
                }*/
                annotationsModule.setModelLoader(new AnnotationsLoader(annotations, roundEnv, annotationsMetadataModel, processingEnv.getElementUtils()));
                annotationsModule.getModelLoader().loadModel();
                System.out.println("Annotations metadata loaded to program model.");
                //System.out.println(annotationsMetadataModel);

                //comment this section for MetadataValidatorGUI correct working
                if (true) {
                    return true;
                }
                bindingsDocument = Utility.loadDocument(new File(configuration.getXamblFile()));
                Node node = (Node) Utility.evaluateXPathExpression(bindingsDocument, "//@pattern-model-loader", XPathConstants.NODE);
                System.out.println("Xambl file loaded.");
                PatternBasedModelLoader modelLoader = ProgramModelLoader.loadPatternBasedModelLoader(node.getNodeValue());
                //System.out.println(modelLoader);
                modelLoader.setBindingDocument(bindingsDocument);
                modelLoader.setXmlMetadataSources(new File(configuration.getXmlMetadataSources()));
                modelLoader.setProgramModel(xmlMetadataModel);
                xmlModule.setModelLoader(modelLoader);
                xmlModule.getModelLoader().loadModel();
                System.out.println("Xml metadata loaded to program model.");
                //System.out.println(xmlMetadataModel);

                validateMetadataAndCreateResults("", "");
                System.out.println("Metadata succesfuly compared.");
                long endTime = System.currentTimeMillis();
                System.out.println("Generating results to \".\\data\\results\\results.xml\".");
                System.out.println("Processing lasted for : " + (float) (endTime - startTime) / 1000 + " seconds");

            } catch (CloneNotSupportedException ex) {
                System.out.println(ex.getMessage());
            }
        }
        return true;
    }

    public void loadXmlMetadata(File xamblFile, DocumentsContainer documentsContainer) {
        bindingsDocument = Utility.loadDocument(xamblFile);
        Node node = (Node) Utility.evaluateXPathExpression(bindingsDocument, "//@pattern-model-loader", XPathConstants.NODE);
        System.out.println("Xambl file loaded.");
        PatternBasedModelLoader modelLoader = ProgramModelLoader.loadPatternBasedModelLoader(node.getNodeValue());
        //System.out.println(modelLoader);
        modelLoader.setBindingDocument(bindingsDocument);
        modelLoader.setXmlMetadataSources(documentsContainer);
        try {
            xmlMetadataModel = (ProgramMetadataModel) programModelLoader.getModel().clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        modelLoader.setProgramModel(xmlMetadataModel);
        xmlModule.setModelLoader(modelLoader);
        xmlModule.getModelLoader().loadModel();
        System.out.println("Xml metadata loaded to program model.");
    }

    public AnnotationsModule getAnnotationsModule() {
        return annotationsModule;
    }

    public XmlModule getXmlModule() {
        return xmlModule;
    }
}
