package xammt.validator.modules.results;

import org.apache.velocity.VelocityContext;
import xammt.validator.modules.TemplateGenerator;
import xammt.validator.modules.results.datamodel.results.ProgramElementResults;

import java.io.*;
import java.util.Set;

public class ResultGenerator {

    private Set<ProgramElementResults> resultElements;
    private final File outputDirectory;// = new File("./data/results");
    private final File outputFile;// = new File("./data/results/results.xml");

    public ResultGenerator(Set<ProgramElementResults> resultElements, String outputDirectoryPath, String outputFilePath) {
        this.resultElements = resultElements;
        outputDirectory = new File(outputDirectoryPath);
        outputFile = new File(outputFilePath);
    }

    public void generate() {
        {
            FileOutputStream fileOutputStream = null;
            try {
                overwriteOutput();
                VelocityContext velocityContext = new VelocityContext();
                velocityContext.put("resultElements", resultElements);
                fileOutputStream = new FileOutputStream(outputFile);
                Writer writer = new OutputStreamWriter(fileOutputStream);
                writer.write(TemplateGenerator.generateCode(velocityContext, "/xammt/validator/modules/results/templates/result-file.vm"));
                writer.close();
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            } finally {
                try {
                    fileOutputStream.close();
                } catch (IOException ex) {
                    System.out.println(ex.getMessage());
                }
            }
        }
    }

    public void overwriteOutput() {
        try {
            if (outputFile.exists()) {
                outputFile.delete();
            } else {
                if (!outputDirectory.exists()) {
                    outputDirectory.mkdirs();
                }
            }
            outputFile.createNewFile();
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }
}
