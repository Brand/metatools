package xammt.validator.modules.metadata.loader.datamodel;

/**
 * Created with IntelliJ IDEA.
 * User: Jaroslav
 * Date: 24.2.2013
 * Time: 22:38
 */
public class AnnotationAttributeNamesWrapper {
    private String simpleName;
    private String qualifiedName;

    public AnnotationAttributeNamesWrapper(String simpleName, String qualifiedName) {
        this.simpleName = simpleName;
        this.qualifiedName = qualifiedName;
    }

    public String getSimpleName() {
        return simpleName;
    }

    public void setSimpleName(String simpleName) {
        this.simpleName = simpleName;
    }

    public String getQualifiedName() {
        return qualifiedName;
    }

    public void setQualifiedName(String qualifiedName) {
        this.qualifiedName = qualifiedName;
    }
}
