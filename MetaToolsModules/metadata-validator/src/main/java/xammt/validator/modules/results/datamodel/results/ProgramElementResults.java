package xammt.validator.modules.results.datamodel.results;

import data.models.program.element.Type;
import org.apache.velocity.VelocityContext;
import xammt.validator.modules.TemplateGenerator;

import javax.xml.bind.annotation.*;
import java.util.Set;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "program-element")
public class ProgramElementResults {
    @XmlAttribute(name = "identificator")
    private String programElementIdentificator;
    @XmlAttribute(name = "type")
    private Type programElementType;
    @XmlElement(name = "result")
    private Set<Result> attributeComparingResults;

    public ProgramElementResults() {
    }

    public ProgramElementResults(String programElementIdentificator, Type programElementType, Set<Result> attributeComparingResults) {
        this.programElementIdentificator = programElementIdentificator;
        this.programElementType = programElementType;
        this.attributeComparingResults = attributeComparingResults;
    }

    public String getProgramElementIdentificator() {
        return programElementIdentificator;
    }

    public Type getProgramElementType() {
        return programElementType;
    }

    public Set<Result> getAttributeComparingResults() {
        return attributeComparingResults;
    }

    public String asXmlElement() {
        VelocityContext velocityContext = new VelocityContext();
        velocityContext.put("programElementIdentificator", programElementIdentificator);
        velocityContext.put("programElementType", programElementType.name());
        velocityContext.put("attributeComparingResults", attributeComparingResults != null ? attributeComparingResults : "null");
        return TemplateGenerator.generateCode(velocityContext, "/xammt/validator/modules/results/templates/program-element-result.vm");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ProgramElementResults)) return false;

        ProgramElementResults that = (ProgramElementResults) o;

        if (attributeComparingResults != null ? !attributeComparingResults.equals(that.attributeComparingResults) : that.attributeComparingResults != null)
            return false;
        if (!programElementIdentificator.equals(that.programElementIdentificator)) return false;
        if (programElementType != that.programElementType) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = programElementIdentificator.hashCode();
        result = 31 * result + programElementType.hashCode();
        result = 31 * result + (attributeComparingResults != null ? attributeComparingResults.hashCode() : 0);
        return result;
    }
}
