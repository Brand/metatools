package xammt.validator.modules.metadata.loader.datamodel;

import api.ProgramElementInterface;
import data.models.program.element.IdentificatorParser;
import data.models.program.element.Type;

import java.util.HashMap;
import java.util.Map;

public class ProgramElement implements ProgramElementInterface {
    
    private transient Type type;
    private String qualifiedIdentificator;
    private String simpleIdentificator;
    private Map<String, Object> metadata = new HashMap<String, Object>();

    public ProgramElement(String qualifiedIdentificator, Type type) {
        this.qualifiedIdentificator = qualifiedIdentificator;
        this.simpleIdentificator = IdentificatorParser.getSimpleId(qualifiedIdentificator);
        this.type = type;
    }    

    @Override
    public Type getType() {
        return type;
    }

    @Override
    public String getQualifiedIdentificator() {
        return qualifiedIdentificator;
    }

    @Override
    public String getSimpleIdentificator() {
        return simpleIdentificator;
    }

    @Override
    public Map<String, Object> getMetadata() {
        return metadata;
    }   
}
