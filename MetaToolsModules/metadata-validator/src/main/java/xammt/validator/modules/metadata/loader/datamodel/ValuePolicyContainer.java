package xammt.validator.modules.metadata.loader.datamodel;

import org.w3c.dom.Node;
import xammt.validator.datamodel.binding.ComparePolicyPOJO;

/**
 * User: brano
 */
public class ValuePolicyContainer {
    private ComparePolicyPOJO comparePolicy;
    private Node element;
    private String value;

    public ValuePolicyContainer(ComparePolicyPOJO comparePolicy, Node element, boolean isVoid) {
        this.comparePolicy = comparePolicy;
        this.element = element;
        this.value = isVoid ? "null" : element.getTextContent();
    }

    public ComparePolicyPOJO getComparePolicy() {
        return comparePolicy;
    }

    public Node getElement() {
        return element;
    }

    public String getValue() {
        return value;
    }
}
