package xammt.validator.modules.metadata.loader.datamodel;

import api.ProgramElementInterface;
import api.ProgramMetadataModelInterface;
import data.models.program.element.IdentificatorParser;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class ProgramMetadataModel implements Cloneable, ProgramMetadataModelInterface {

    private Map<String, ProgramElementInterface> programElements = new HashMap<String, ProgramElementInterface>();

    @Override
    public Map<String, ProgramElementInterface> getProgramElements() {
        return programElements;
    }

    @Override
    public ProgramElementInterface getPackageElement(String programElementIdentificator) {
        String identificator = programElementIdentificator;
        ProgramElementInterface potentialPackageProgramElementInterface;
//        System.out.println("Package searching started...");
        do {
            IdentificatorParser identificatorParser = new IdentificatorParser(identificator);
            identificatorParser.parse();
            identificator = identificatorParser.getEnclosingElementQualifiedId();
            if (identificator != null) {
                potentialPackageProgramElementInterface = getProgramElements().get(identificator);
            } else {
                potentialPackageProgramElementInterface = getProgramElements().get(programElementIdentificator);
            }
        } while (!potentialPackageProgramElementInterface.getType().isPackage());

        return potentialPackageProgramElementInterface;
    }

    @Override
    public ProgramElementInterface getTypeElement(String programElementIdentificator) {
        String identificator = programElementIdentificator;
        ProgramElementInterface potentialTypeProgramElementInterface;
        do {
            IdentificatorParser identificatorParser = new IdentificatorParser(identificator);
            identificatorParser.parse();
            identificator = identificatorParser.getEnclosingElementQualifiedId();
            potentialTypeProgramElementInterface = getProgramElements().get(identificator);
        } while (!(potentialTypeProgramElementInterface.getType().isType() || potentialTypeProgramElementInterface.getType().isAnnotation()));
        return potentialTypeProgramElementInterface;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        for (Entry<String, ProgramElementInterface> programElementEntry : programElements.entrySet()) {
            String string = "\n" + programElementEntry.getValue().getType().name() + " " + programElementEntry.getKey();
            builder.append(string);
            for (Entry<String, Object> metadataEntry : programElementEntry.getValue().getMetadata().entrySet()) {
                string = "\n\t" + metadataEntry.getKey() + (!metadataEntry.getValue().equals("") ? " = \"" + metadataEntry.getValue().toString() + "\"" : "");
                builder.append(string);
            }
            builder.append("\n");
        }
        return builder.toString();
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        ProgramMetadataModel copy = new ProgramMetadataModel();
        for (Entry<String, ProgramElementInterface> programElementsEntry : programElements.entrySet()) {
            copy.programElements.put(String.copyValueOf(programElementsEntry.getKey().toCharArray()), new ProgramElement(String.copyValueOf(programElementsEntry.getValue().getQualifiedIdentificator().toCharArray()), programElementsEntry.getValue().getType()));
        }
        return copy;
    }

    @Override
    public Map<String, ProgramElementInterface> filterProgramElementsByAnnotationPath(String annotationFQ) {
        Map<String, ProgramElementInterface> filteredList = new HashMap<String, ProgramElementInterface>();
        for (String programElementIdentifier : programElements.keySet()) {
            ProgramElementInterface programElement = programElements.get(programElementIdentifier);
            for (String metadataIdentifier : programElement.getMetadata().keySet()) {
                if (annotationFQ.equals(metadataIdentifier)) {
                    filteredList.put(programElementIdentifier, programElement);
                }
            }
        }
        return filteredList;
    }
}
