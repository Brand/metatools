package xammt.validator.modules.metadata.loader.annotations;

import api.ProgramElementInterface;
import api.ProgramMetadataModelInterface;
import xammt.validator.interfaces.ModelLoader;
import xammt.validator.modules.metadata.loader.ProgramModelLoader;
import xammt.validator.modules.metadata.loader.datamodel.AnnotationAttributeNamesWrapper;

import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.element.*;
import javax.lang.model.util.Elements;
import java.util.*;
import java.util.Map.Entry;

public class AnnotationsLoader implements ModelLoader {

    private Set<? extends TypeElement> annotations;
    private RoundEnvironment roundEnv;
    private ProgramMetadataModelInterface model;
    private Elements elementUtilities;
    private Set<String> annotationsId;

    public AnnotationsLoader(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv, ProgramMetadataModelInterface model, Elements elementUtilities) {
        this.annotations = annotations;
        this.roundEnv = roundEnv;
        this.model = model;
        this.elementUtilities = elementUtilities;
    }

    public AnnotationsLoader(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv, ProgramMetadataModelInterface model, Elements elementUtilities, Set<String> annotationsId) {
        this(annotations, roundEnv, model, elementUtilities);
        this.annotationsId = annotationsId;
    }

    @Override
    public void loadModel() {
        for (TypeElement annotation : annotations) {
            Set<? extends Element> elements = roundEnv.getElementsAnnotatedWith(annotation);
            for (Element element : elements) {
                String qualifiedIdentificator = ProgramModelLoader.getQualifiedIdentificator(element, elementUtilities);
                ProgramElementInterface programElementInterface = model.getProgramElements().get(qualifiedIdentificator);
                List<? extends AnnotationMirror> annotationMirrors = element.getAnnotationMirrors();
                for (AnnotationMirror annotationMirror : annotationMirrors) {
                    if (annotationsId != null) {
                        if (annotationsId.contains(annotation.getQualifiedName().toString())) {
                            programElementInterface.getMetadata().putAll(load(annotationMirror));
                        }
                    } else {
                        programElementInterface.getMetadata().putAll(load(annotationMirror));
                    }
                }
            }
        }
    }

    private Map<String, Object> load(AnnotationMirror annotationMirror) {
        String annotationQualifiedName = annotationMirror.getAnnotationType().toString();
        return load(annotationMirror.getElementValues(), annotationQualifiedName);
    }

    private Map<String, Object> load(Map<? extends ExecutableElement, ? extends AnnotationValue> attributeElements, String annotationQualifiedName) {
        Map<String, Object> attributes = new HashMap<String, Object>();
        if (!attributeElements.isEmpty()) {
            for (Entry<? extends ExecutableElement, ? extends AnnotationValue> attributeElementsEntry : attributeElements.entrySet()) {               
                String attributeName = attributeElementsEntry.getKey().getSimpleName().toString();
                Object attributeValue = attributeElementsEntry.getValue().getValue();
                List<AnnotationPathWrapper> annotationPathWrappers = buildAnnotationValuePath(attributeValue);
                for (AnnotationPathWrapper annotationPathWrapper : annotationPathWrappers) {
                    annotationPathWrapper.setPath(annotationQualifiedName + "." + attributeName + annotationPathWrapper.getPath());
                    attributes.put(annotationPathWrapper.getPath(), annotationPathWrapper.getValue());
                }
            }
        } else {
            attributes.put(annotationQualifiedName, "null");
        }
        return attributes;
    }
    
    private List<AnnotationPathWrapper> buildAnnotationValuePath(Object object) {
        List<AnnotationPathWrapper> annotationPathWrapperList = new ArrayList<AnnotationPathWrapper>();
        if (object instanceof List) {
            for(int i = 0; i < ((List) object).size(); i++) {
                List<AnnotationPathWrapper> annotationPathWrappers = buildAnnotationValuePath(((List) object).get(i));
                for (AnnotationPathWrapper annotationPathWrapper : annotationPathWrappers) {
                    if (annotationPathWrapper.getPath().length()  > 0) {
                        annotationPathWrapper.setPath(annotationPathWrapper.getPath().substring(0,
                                annotationPathWrapper.getPath().length() - 1) + "[" + i + "]}");
                    } else {
                        annotationPathWrapper.setPath(annotationPathWrapper.getPath() + "[" + i + "]");
                    }

                }
                annotationPathWrapperList.addAll(annotationPathWrappers);
            }
        } else if (object instanceof AnnotationMirror) {
            AnnotationMirror annotationMirror = (AnnotationMirror) object;
            String annotationQualifiedName = annotationMirror.getAnnotationType().toString();           
            for (Entry<? extends ExecutableElement, ? extends AnnotationValue> attributeElementsEntry : annotationMirror.getElementValues().entrySet()) {
                List<AnnotationPathWrapper> annotationPathWrappers = buildAnnotationValuePath(attributeElementsEntry.getValue().getValue());
                String attributeName = attributeElementsEntry.getKey().getSimpleName().toString();
                for (AnnotationPathWrapper annotationPathWrapper : annotationPathWrappers) {
                    annotationPathWrapper.setPath("{" + annotationQualifiedName + "." + attributeName + annotationPathWrapper.getPath() + "}");
                }
                annotationPathWrapperList.addAll(annotationPathWrappers);
            }
        } else {
            annotationPathWrapperList.add(new AnnotationPathWrapper("", object));
            return annotationPathWrapperList;
        }
        return annotationPathWrapperList;
    }

    public List<AnnotationAttributeNamesWrapper> getAnnotationAttributesForElement(Map<? extends ExecutableElement, ? extends AnnotationValue> attributeElements, String annotationQualifiedName) {
        List<AnnotationAttributeNamesWrapper> annotationAttributeNamesWrapperList = new ArrayList<AnnotationAttributeNamesWrapper>();
        if (!attributeElements.isEmpty()) {
            for (Entry<? extends ExecutableElement, ? extends AnnotationValue> attributeElementsEntry : attributeElements.entrySet()) {
                String attributeName = attributeElementsEntry.getKey().getSimpleName().toString();
                Object attributeValue = attributeElementsEntry.getValue().getValue();
                List<AnnotationAttributeNamesWrapper> annotationAttributeNamesWrappers = buildAnnotationNames(attributeValue, attributeName);
                for (AnnotationAttributeNamesWrapper annotationAttributeNamesWrapper : annotationAttributeNamesWrappers) {
                    annotationAttributeNamesWrapper.setQualifiedName(annotationQualifiedName + "." + attributeName + annotationAttributeNamesWrapper.getQualifiedName());
                    annotationAttributeNamesWrapperList.add(annotationAttributeNamesWrapper);
                }
            }
        }
        return annotationAttributeNamesWrapperList;
    }

    private List<AnnotationAttributeNamesWrapper> buildAnnotationNames(Object object, String topAttributeName) {
        List<AnnotationAttributeNamesWrapper> annotationAttributeNamesWrapperList = new ArrayList<AnnotationAttributeNamesWrapper>();
        if (object instanceof List) {
            for (int i = 0; i < ((List) object).size(); i++) {
                List<AnnotationAttributeNamesWrapper> annotationAttributeNamesWrappers = buildAnnotationNames(((List) object).get(i), topAttributeName);
                for (AnnotationAttributeNamesWrapper annotationAttributeNamesWrapper : annotationAttributeNamesWrappers) {
                    if (annotationAttributeNamesWrapper.getQualifiedName().length()  > 0) {
                        annotationAttributeNamesWrapper.setQualifiedName(annotationAttributeNamesWrapper.getQualifiedName().substring(0,
                                annotationAttributeNamesWrapper.getQualifiedName().length() - 1) + "[" + i + "]}");
                    } else {
                        annotationAttributeNamesWrapper.setQualifiedName(annotationAttributeNamesWrapper.getQualifiedName() + "[" + i + "]");
                    }
                }
                annotationAttributeNamesWrapperList.addAll(annotationAttributeNamesWrappers);
            }
        } else if (object instanceof AnnotationMirror) {
            AnnotationMirror annotationMirror = (AnnotationMirror) object;
            String annotationQualifiedName = annotationMirror.getAnnotationType().toString();
            for (Entry<? extends ExecutableElement, ? extends AnnotationValue> attributeElementsEntry : annotationMirror.getElementValues().entrySet()) {
                List<AnnotationAttributeNamesWrapper> annotationAttributeNamesWrappers = buildAnnotationNames(attributeElementsEntry.getValue().getValue(), topAttributeName);
                String attributeName = attributeElementsEntry.getKey().getSimpleName().toString();
                for (AnnotationAttributeNamesWrapper annotationAttributeNamesWrapper : annotationAttributeNamesWrappers) {
                    annotationAttributeNamesWrapper.setQualifiedName("{" + annotationQualifiedName + "." + attributeName + annotationAttributeNamesWrapper.getQualifiedName() + "}");
                    annotationAttributeNamesWrapper.setSimpleName(annotationAttributeNamesWrapper.getSimpleName() + "." + attributeName);
                }
                annotationAttributeNamesWrapperList.addAll(annotationAttributeNamesWrappers);
            }
        } else {
            annotationAttributeNamesWrapperList.add(new AnnotationAttributeNamesWrapper(topAttributeName, ""));
            return annotationAttributeNamesWrapperList;
        }
        return annotationAttributeNamesWrapperList;
    }

    public List<? extends Element> getAnnotationUsages(TypeElement annotation) {
        return new ArrayList<Element>(roundEnv.getElementsAnnotatedWith(annotation));
    }

    public String getQualifiedIdentificator(Element element) {
        return ProgramModelLoader.getQualifiedIdentificator(element, elementUtilities);
    }

    @Override
    public ProgramMetadataModelInterface getModel() {
        return model;
    }

    public List<? extends TypeElement> getAnnotations() {
        return new ArrayList<TypeElement>(annotations);
    }
    
    private class AnnotationPathWrapper {
        private String path;
        private Object value;
        
        public AnnotationPathWrapper(String path, Object value) {
            this.path = path;
            this.value = value;
        }
        
        public String getPath() {
            return path;
        }
        
        public void setPath(String path) {
            this.path = path;
        }
        
        public Object getValue() {
            return value;
        }
    }
}
