package xammt.validator.modules.results.datamodel.comparators;

import xammt.validator.datamodel.binding.ComparePolicyPOJO;
import xammt.validator.modules.results.datamodel.results.MissingResult.Form;
import xammt.validator.modules.results.datamodel.results.Result;

/**
 * User: brano
 */
public abstract class AbstractComparator {
    protected Object xmlValue;
    protected Object attributeValue;
    protected ComparePolicyPOJO comparePolicyPOJO;
    protected String name;

    protected String attributeName;
    protected Form counterForm;

    protected int parametersCount;

    public AbstractComparator(String attributeName, Object attributeValue, Object counterAttributeValue, Form counterForm, ComparePolicyPOJO comparePolicyPOJO) {
        this.attributeValue = attributeValue;
        this.xmlValue = counterAttributeValue;
        this.counterForm = counterForm;
        this.attributeName = attributeName;
        this.comparePolicyPOJO = comparePolicyPOJO;
        this.name = comparePolicyPOJO.getComparator();
    }

    AbstractComparator(String name, int parametersCount) {
        this.parametersCount = parametersCount;
        this.name = name;
    }

    public final int getParametersCount() {
        return parametersCount;
    }

    public abstract Result compare();

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AbstractComparator)) return false;

        AbstractComparator that = (AbstractComparator) o;

        if (!name.equals(that.name)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    public Object getXmlValue() {
        return xmlValue;
    }

    public void setXmlValue(Object xmlValue) {
        this.xmlValue = xmlValue;
    }

    public Object getAttributeValue() {
        return attributeValue;
    }

    public void setAttributeValue(Object attributeValue) {
        this.attributeValue = attributeValue;
    }

    public ComparePolicyPOJO getComparePolicyPOJO() {
        return comparePolicyPOJO;
    }

    public void setComparePolicyPOJO(ComparePolicyPOJO comparePolicyPOJO) {
        this.comparePolicyPOJO = comparePolicyPOJO;
    }

    public String getAttributeName() {
        return attributeName;
    }

    public void setAttributeName(String attributeName) {
        this.attributeName = attributeName;
    }

    public Form getCounterForm() {
        return counterForm;
    }

    public void setCounterForm(Form counterForm) {
        this.counterForm = counterForm;
    }
}
