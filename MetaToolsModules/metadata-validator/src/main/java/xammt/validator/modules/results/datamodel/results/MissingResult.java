package xammt.validator.modules.results.datamodel.results;

import org.eclipse.persistence.oxm.annotations.XmlDiscriminatorValue;

/**
 * Created with IntelliJ IDEA.
 * User: brano
 */
@XmlDiscriminatorValue("Missing value")
public class MissingResult extends Result {

    public static enum Form {
        XML("XML form"),
        ATTRIBUTE("Attribute form");

        private String name;

        private Form(String name) {
            this.name = name;
        }
    }

    public MissingResult() {
        super();
    }

    public MissingResult(String attributeIdentificator, Form form, Object existingValue) {
        super(ResultState.INCONSISTENT, "Missing value", attributeIdentificator);
        addInfo("Missing value for form: ", form.toString());
        addInfo("Existing value = ", existingValue.toString());
    }
}
