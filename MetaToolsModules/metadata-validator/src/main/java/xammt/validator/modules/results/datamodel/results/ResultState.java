package xammt.validator.modules.results.datamodel.results;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;

/**
 * Created with IntelliJ IDEA.
 * User: brano
 */

@XmlEnum
public enum ResultState {
    @XmlEnumValue("consistent")
    CONSISTENT("consistent"),
    @XmlEnumValue("inconsistent")
    INCONSISTENT("inconsistent");

    private String name;

    private ResultState(String name) {
        this.name = name;
    }


    @Override
    public String toString() {
        return name;
    }

    public static ResultState getStateByName(String name) {
        for (ResultState state : values()) {
            if (state.name.equals(name)) {
                return state;
            }
        }

        return null;
    }
}
