package xammt.validator.modules.results.datamodel.results;

import org.eclipse.persistence.oxm.annotations.XmlDiscriminatorValue;

/**
 * Created with IntelliJ IDEA.
 * User: brano
 */
@XmlDiscriminatorValue("Equality")
public class EqualityResult extends Result {
    public EqualityResult() {
        super();
    }

    public EqualityResult(String attributeIdentificator, String value) {
        super(ResultState.CONSISTENT, "Equality", attributeIdentificator);
        addInfo("Attribute value = ", value);
    }
}
