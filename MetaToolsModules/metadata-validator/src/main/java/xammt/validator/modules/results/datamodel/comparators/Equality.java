package xammt.validator.modules.results.datamodel.comparators;


import xammt.validator.annotations.ComparatorClass;
import xammt.validator.modules.results.datamodel.results.ConflictResult;
import xammt.validator.modules.results.datamodel.results.EqualityResult;
import xammt.validator.modules.results.datamodel.results.MissingResult;
import xammt.validator.modules.results.datamodel.results.Result;


@ComparatorClass(xmlName = "Equality", uiName = "Equality", createUI = false)
public class Equality extends AbstractComparator {

    public Equality(String attributeName, Object attributeValue, Object counterAttributeValue, MissingResult.Form counterForm) {
        super(attributeName, attributeValue, counterAttributeValue, counterForm, null);
        this.parametersCount = 0;
    }

    Equality() {
        super("Equality", 0);
    }

    @Override
    public Result compare() {
        if (xmlValue == null) {
            return new MissingResult(attributeName, counterForm, attributeValue);
        } else if (attributeValue == null) {
            return new MissingResult(attributeName, counterForm, xmlValue);
        }

        Object aValue = attributeValue;
        Object xValue = xmlValue;
        // switch values
        if (counterForm.equals(MissingResult.Form.ATTRIBUTE)) {
            Object tmp = attributeValue;
            aValue = xmlValue;
            xValue = tmp;
        }

        try {
            if (aValue instanceof Boolean) {
                xValue = Boolean.parseBoolean(xValue.toString());
            } else if (aValue instanceof Character) {
                xValue = xValue.toString().charAt(0);
            } else if (aValue instanceof Byte) {
                xValue = Byte.parseByte(xValue.toString());
            } else if (aValue instanceof Short) {
                xValue = Short.parseShort(xValue.toString());
            } else if (aValue instanceof Integer) {
                xValue = Integer.parseInt(xValue.toString());
            } else if (aValue instanceof Long) {
                xValue = Long.parseLong(xValue.toString());
            } else if (aValue instanceof Float) {
                xValue = Float.parseFloat(xValue.toString());
            } else if (aValue instanceof Double) {
                xValue = Double.parseDouble(xValue.toString());
            } else if (aValue instanceof Class) {
                aValue = ((Class) aValue).getSimpleName() + ".class";
            } else if (aValue instanceof Enum) {
                aValue = ((Enum) aValue).name();
            }
        } catch (Exception ex) {
            return new ConflictResult(
                    attributeName,
                    counterForm.equals(MissingResult.Form.XML) ? xmlValue.toString() : attributeValue.toString(),
                    counterForm.equals(MissingResult.Form.ATTRIBUTE) ? attributeValue.toString() : xmlValue.toString());
        }

        if (!aValue.equals(xValue)) {
            return new ConflictResult(
                    attributeName,
                    counterForm.equals(MissingResult.Form.XML) ? xmlValue.toString() : attributeValue.toString(),
                    counterForm.equals(MissingResult.Form.XML) ? attributeValue.toString() : xmlValue.toString());
        } else {
            return new EqualityResult(attributeName, attributeValue.toString());
        }
    }
}
