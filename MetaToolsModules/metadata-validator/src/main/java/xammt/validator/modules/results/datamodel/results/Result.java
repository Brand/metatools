package xammt.validator.modules.results.datamodel.results;

import org.apache.velocity.VelocityContext;
import org.eclipse.persistence.oxm.annotations.XmlDiscriminatorNode;
import xammt.validator.modules.TemplateGenerator;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: brano
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlDiscriminatorNode("@type")
@XmlSeeAlso({MatchResult.class, MissingResult.class, ConflictResult.class, EqualityResult.class})
public abstract class Result {
    @XmlAttribute(name = "state")
    private ResultState state;
    @XmlAttribute(name = "type")
    private String type;
    @XmlAttribute(name = "attribute-id")
    private String attributeIdentificator;
    @XmlElement(name = "info")
    private List<Info> infoList;

    protected Result() {
    }

    protected Result(ResultState state, String type, String attributeIdentificator) {
        this.state = state;
        this.type = type;
        this.attributeIdentificator = attributeIdentificator;
        this.infoList = new ArrayList<Info>();
    }

    protected void addInfo(String name, String value) {
        this.infoList.add(new Info(name, value));
    }

    public String asXmlElement() {
        VelocityContext velocityContext = new VelocityContext();
        velocityContext.put("state", state);
        velocityContext.put("type", type);
        velocityContext.put("attributeIdentificator", attributeIdentificator);
        velocityContext.put("infoList", infoList);
        return TemplateGenerator.generateCode(velocityContext, "/xammt/validator/modules/results/templates/result.vm");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Result)) return false;

        Result result = (Result) o;

        if (!attributeIdentificator.equals(result.attributeIdentificator)) return false;
//        if (infoList != null ? !infoList.equals(result.infoList) : result.infoList != null) return false;
        if (state != result.state) return false;
        if (!type.equals(result.type)) return false;

        return true;
    }



    @Override
    public int hashCode() {
        if (state != null && type != null && attributeIdentificator != null) {
            int result = state.hashCode();
            result = 31 * result + type.hashCode();
            result = 31 * result + attributeIdentificator.hashCode();
            return result;
        }
//        result = 31 * result + (infoList != null ? infoList.hashCode() : 0);
        return 0;
    }

    public ResultState getState() {
        return state;
    }

    public String getType() {
        return type;
    }

    public String getAttributeIdentificator() {
        return attributeIdentificator;
    }

    public List<Info> getInfoList() {
        return infoList;
    }

    //=========================================================

    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlRootElement(name = "info")
    public static class Info {
        @XmlAttribute(name = "name")
        private String name;
        @XmlAttribute(name = "value")
        private String value;

        public Info() {
        }

        public Info(String name, String value) {
            this.name = name;
            this.value = value;
        }

        public String getName() {
            return name;
        }

        public String getValue() {
            return value;
        }

//        @Override
//        public boolean equals(Object o) {
//            if (this == o) return true;
//            if (!(o instanceof Info)) return false;
//
//            Info info = (Info) o;
//
//            if (name != null ? !name.equals(info.name) : info.name != null) return false;
//            if (value != null ? !value.equals(info.value) : info.value != null) return false;
//
//            return true;
//        }
//
//        @Override
//        public int hashCode() {
//            int result = name != null ? name.hashCode() : 0;
//            result = 31 * result + (value != null ? value.hashCode() : 0);
//            return result;
//        }
    }
}
