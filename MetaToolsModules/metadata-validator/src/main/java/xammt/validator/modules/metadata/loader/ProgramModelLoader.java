package xammt.validator.modules.metadata.loader;

import api.ProgramMetadataModelInterface;
import data.models.program.element.Type;
import xammt.validator.interfaces.ModelLoader;
import xammt.validator.interfaces.PatternBasedModelLoader;
import xammt.validator.modules.metadata.loader.datamodel.ProgramElement;
import xammt.validator.modules.metadata.loader.datamodel.ProgramMetadataModel;

import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.PackageElement;
import javax.lang.model.util.Elements;
import java.util.Set;

public class ProgramModelLoader implements ModelLoader {

    private ProgramMetadataModelInterface programModel;
    private RoundEnvironment roundEnv;
    private Elements elementUtilities;

    public ProgramModelLoader(RoundEnvironment roundEnv, Elements elementUtilities) {
        this.roundEnv = roundEnv;
        this.elementUtilities = elementUtilities;
    }

    @Override
    public void loadModel() {
        programModel = new ProgramMetadataModel();
        Set<? extends Element> rootElements = roundEnv.getRootElements();
        for (Element rootElement : rootElements) {
            if (isType(rootElement)) {
                loadToModel(elementUtilities.getPackageOf(rootElement));
                loadToModel(rootElement);
                iterateViaElements(rootElement);
            }
        }
    }

    private void iterateViaElements(Element rootElement) {
        for (Element element : elementUtilities.getAllMembers(elementUtilities.getTypeElement(rootElement.asType().toString()))){
            if (element.getEnclosingElement().equals(rootElement)) {
                if (element.getKind().isClass()) {
                    loadToModel(element);
                    iterateViaElements(element);
                } else {
                    loadToModel(element);
                }
            }
        }
    }

    /**
     * Vklada jednotlive elementy do mapy
     * @param element
     */
    private void loadToModel(Element element) {
        String qualifiedIdentificator = getQualifiedIdentificator(element, elementUtilities);
        if (!programModel.getProgramElements().containsKey(qualifiedIdentificator)) {
            programModel.getProgramElements().put(qualifiedIdentificator, new ProgramElement(qualifiedIdentificator, transformKindToType(element)));
        }
    }

    public static String getQualifiedIdentificator(Element element, Elements elementUtilities) {
        switch (element.getKind()) {
            case METHOD:
                return elementUtilities.getPackageOf(element).getQualifiedName().toString() + "." + resolveFullClassName(element.getEnclosingElement()) + "." + element.toString();
            case CONSTRUCTOR:
                String id = elementUtilities.getPackageOf(element).getQualifiedName().toString() + "." + resolveFullClassName(element.getEnclosingElement()) + "." + element.getSimpleName() + element.asType().toString();
                return id.replaceAll("<init>", element.getEnclosingElement().getSimpleName().toString()).replaceAll("void", "");
            case FIELD:
                return elementUtilities.getPackageOf(element).getQualifiedName().toString() + "." + resolveFullClassName(element.getEnclosingElement()) + "." + element.getSimpleName().toString();
            case CLASS:
                return elementUtilities.getPackageOf(element).getQualifiedName().toString() + "." + resolveFullClassName(element);
            case PACKAGE:
                return ((PackageElement) element).getQualifiedName().toString();
            case ANNOTATION_TYPE:
                return elementUtilities.getPackageOf(element).getQualifiedName().toString() + "." + element.getSimpleName().toString();
            case INTERFACE:
                return elementUtilities.getPackageOf(element).getQualifiedName().toString() + "." + element.getSimpleName().toString();
            case ENUM:
                return elementUtilities.getPackageOf(element).getQualifiedName().toString() + "." + element.getSimpleName().toString();
            case ENUM_CONSTANT:
                return elementUtilities.getPackageOf(element).getQualifiedName().toString() + "." + element.getEnclosingElement().getSimpleName().toString() + "." + element.getSimpleName().toString();
            default:
                return null;
        }
    }

    private static String resolveFullClassName(Element element) {
        String className = "";
        if (element.getKind().isClass()) {
            if (element.getEnclosingElement().getKind().isClass()) {
                className += resolveFullClassName(element.getEnclosingElement()) + ".";
            }
            className += element.getSimpleName();
        }
        return className;
    }

    public static Type transformKindToType(Element element) {
        switch (element.getKind()) {
            case PACKAGE:
                return Type.PACKAGE;
            case INTERFACE:
                return Type.INTERFACE;
            case ANNOTATION_TYPE:
                return Type.ANNOTATION;
            case ENUM:
                return Type.ENUM;
            case ENUM_CONSTANT:
                return Type.CONSTANT;
            case CLASS:
                return Type.CLASS;
            case FIELD:
                return Type.FIELD;
            case CONSTRUCTOR:
                switch (element.getEnclosingElement().getKind()) {
                    case CLASS:
                        return Type.CONSTRUCTOR;
                    case ENUM:
                        return Type.ENUM_CONSTRUCTOR;
                    default:
                        return null;
                }
            case METHOD:
                switch (element.getEnclosingElement().getKind()) {
                    case ANNOTATION_TYPE:
                        return Type.ANNOTATION_METHOD;
                    case INTERFACE:
                        return Type.INTERFACE_METHOD;
                    case CLASS:
                        return Type.METHOD;
                    case ENUM:
                        return Type.ENUM_METHOD;
                    default:
                        return null;
                }
            default:
                return null;
        }
    }

    private boolean isType(Element element) {
        return element.getKind().isClass()
                || element.getKind().isInterface()
                || element.getKind().equals(ElementKind.ANNOTATION_TYPE)
                || element.getKind().equals(ElementKind.ENUM);
    }

    public static PatternBasedModelLoader loadPatternBasedModelLoader(String className) {
        try {
            return (PatternBasedModelLoader) (Class.forName(className).newInstance());
        } catch (ClassNotFoundException ex) {
            System.out.println(ex.getMessage());
        } catch (InstantiationException ex) {
            System.out.println(ex.getMessage());
        } catch (IllegalAccessException ex) {
            System.out.println(ex.getMessage());
        }
        return null;
    }

    @Override
    public ProgramMetadataModelInterface getModel() {
        return programModel;
    }
}
