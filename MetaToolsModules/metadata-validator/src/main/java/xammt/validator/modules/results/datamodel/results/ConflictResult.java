package xammt.validator.modules.results.datamodel.results;

import org.eclipse.persistence.oxm.annotations.XmlDiscriminatorValue;

/**
 * Created with IntelliJ IDEA.
 * User: brano
 */
@XmlDiscriminatorValue("Conflict")
public class ConflictResult extends Result {
    public ConflictResult() {
        super();
    }

    public ConflictResult(String attributeIdentificator, String xmlValue, String attributeValue) {
        super(ResultState.INCONSISTENT, "Conflict", attributeIdentificator);
        addInfo("XML value = ", xmlValue);
        addInfo("Attribute value = ", attributeValue);
    }
}
