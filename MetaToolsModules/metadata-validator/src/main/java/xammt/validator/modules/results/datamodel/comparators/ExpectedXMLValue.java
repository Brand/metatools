package xammt.validator.modules.results.datamodel.comparators;

import xammt.validator.annotations.ComparatorClass;
import xammt.validator.annotations.ComparatorParameter;
import xammt.validator.datamodel.binding.ComparePolicyPOJO;
import xammt.validator.modules.results.datamodel.results.ConflictResult;
import xammt.validator.modules.results.datamodel.results.MatchResult;
import xammt.validator.modules.results.datamodel.results.MissingResult;
import xammt.validator.modules.results.datamodel.results.Result;


/**
 * Created with IntelliJ IDEA.
 * User: brano
 */

@ComparatorClass(xmlName = "ExpectedXMLValue", uiName = "Expected XML Value", createUI = true)
public class ExpectedXMLValue extends AbstractComparator {
    @ComparatorParameter(name = "XML Value", checked = false)
    private String expectedXMLvalue;

    public ExpectedXMLValue(String attributeName, Object attributeValue, Object counterAttributeValue, MissingResult.Form counterForm, ComparePolicyPOJO comparePolicyPOJO) {
        super(attributeName, attributeValue, counterAttributeValue, counterForm, comparePolicyPOJO);
        this.parametersCount = 1;
    }

    ExpectedXMLValue() {
        super("ExpectedXMLValue", 1);
    }

    @Override
    public Result compare() {
        expectedXMLvalue = this.comparePolicyPOJO.getParameters().get(0);

        if (xmlValue == null || "".equals(xmlValue)) {
            return new MissingResult(attributeName, MissingResult.Form.XML, attributeValue);
        } else if (!expectedXMLvalue.equals(xmlValue)) {
            return new ConflictResult(attributeName, xmlValue.toString(), expectedXMLvalue);
        } else {
            return new MatchResult(attributeName, xmlValue.toString(), expectedXMLvalue);
        }
    }
}
