package xammt.validator.modules;

import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;

import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;

public abstract class TemplateGenerator {

    public static String generateCode(VelocityContext context, String templateResource) {
        try {
            StringWriter writer = new StringWriter();
            Reader reader = new InputStreamReader(TemplateGenerator.class.getResourceAsStream(templateResource));
            Velocity.evaluate(context, writer, "error", reader);
//            Template template = Velocity.getTemplate(templateResource);
//            template.merge(context, writer);
            return writer.toString();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            return "";
        }
    }
}
