package xammt.validator.modules.metadata.loader.annotations;

import data.models.metadata.definition.Annotation;
import data.models.metadata.definition.PmtdlDefinition;
import xammt.validator.interfaces.ModelLoader;

import javax.lang.model.element.TypeElement;
import java.util.HashSet;
import java.util.Set;

public class AnnotationsModule {

    private ModelLoader modelLoader;
    private Set<String> annotationIds = new HashSet<String>();

    public void loadAnnotationNames(PmtdlDefinition xamclDefinition) {
        for (Annotation annotation : xamclDefinition.getAnnotation()) {
            annotationIds.add(annotation.getId());
        }
    }    

    public Set<? extends TypeElement> filterAnnotations(Set<? extends TypeElement> annotations) {
        Set<TypeElement> filteredEnnotations = new HashSet<TypeElement>();
        for (TypeElement element : annotations) {
            if (annotationIds.contains(element.getQualifiedName().toString())) {
                filteredEnnotations.add(element);
            }
        }
        return filteredEnnotations;
    }

    public Set<String> getAnnotationNames() {
        return annotationIds;
    }

    public ModelLoader getModelLoader() {
        return modelLoader;
    }

    public void setModelLoader(ModelLoader modelLoader) {
        this.modelLoader = modelLoader;
    }
}
