package xammt.validator.datamodel.binding;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

/**
 * User: brano
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "comparePolicy")
public class ComparePolicyPOJO {
    @XmlAttribute(name = "comparator")
    private String comparator;
    @XmlElement(name = "parameter")
    private List<String> parameters;

    public ComparePolicyPOJO(){
        this.parameters = new ArrayList<String>();
    }

    public ComparePolicyPOJO(String comparator) {
        this.comparator = comparator;
        this.parameters = new ArrayList<String>();
    }

    public String getComparator() {
        return comparator;
    }

    public List<String> getParameters() {
        return parameters;
    }

    public void addParameter(String parameter) {
        parameters.add(parameter);
    }

    public void addParameter(int position, String parameter) {
        parameters.add(position, parameter);
    }

    public int getParametersCount() {
        return parameters.size();
    }
}
