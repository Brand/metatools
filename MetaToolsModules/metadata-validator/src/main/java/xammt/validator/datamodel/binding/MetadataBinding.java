package xammt.validator.datamodel.binding;


import metadata.AnnotationType;

import data.models.program.element.Type;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "metadata")
public class MetadataBinding {
    @XmlAttribute(name = "id")
    private String annotationId;
    @XmlAttribute(name = "target")
    private String target;
    @XmlElement(name = "xpath")
    private String xPath;
    @XmlElement(name = "file")
    private String fileLocation;
    @XmlElement(name = "comparePolicy")
    private ComparePolicyPOJO comparePolicy;
    @XmlAttribute(name = "type")
    private AnnotationType type;
    @XmlAttribute(name = "target-type")
    private Type targetType;

    public MetadataBinding() {
    }

    public MetadataBinding(String annotationId, String xPath, String fileLocation, AnnotationType type) {
        this(annotationId, xPath, fileLocation, null, type);
    }

    public MetadataBinding(String annotationId, String xPath, String fileLocation, ComparePolicyPOJO comparePolicy, AnnotationType type) {
        this.annotationId = annotationId;
        this.xPath = xPath;
        this.fileLocation = fileLocation;
        this.comparePolicy = comparePolicy;
        this.type = type;
    }

    public MetadataBinding(String annotationId, String target, String xPath, String fileLocation, ComparePolicyPOJO comparePolicy, AnnotationType type) {
        this(annotationId, xPath, fileLocation, comparePolicy, type);
        this.target = target;
    }

    public String getAnnotationId() {
        return annotationId;
    }

    public void setAnnotationId(String annotationId) {
        this.annotationId = annotationId;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getxPath() {
        return xPath;
    }

    public void setxPath(String xPath) {
        this.xPath = xPath;
    }

    public String getFileLocation() {
        return fileLocation;
    }

    public void setFileLocation(String fileLocation) {
        this.fileLocation = fileLocation;
    }

    public ComparePolicyPOJO getComparePolicy() {
        return comparePolicy;
    }

    public void setComparePolicy(ComparePolicyPOJO comparePolicy) {
        this.comparePolicy = comparePolicy;
    }

    public AnnotationType getType() {
        return type;
    }

    public void setType(AnnotationType type) {
        this.type = type;
    }

    public data.models.program.element.Type getTargetType() {
        return targetType;
    }

    public void setTargetType(data.models.program.element.Type targetType) {
        this.targetType = targetType;
    }
}
