package xammt.validator.datamodel.xml.documents;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class DocumentsContainer {

    private String metadataRootPath;
    private Map<String, DocumentElement> documentElements = new HashMap<String, DocumentElement>();

    public List<DocumentElement> searchDocument(String relativePath) {
        List<DocumentElement> documents = new ArrayList<DocumentElement>();
        for (Entry<String, DocumentElement> documentElementEntry : documentElements.entrySet()) {
            if (documentElementEntry.getKey().contains(relativePath)) {
                documents.add(documentElementEntry.getValue());
            }
        }
        return documents;
    }

    public DocumentsContainer(String metadataRootPath) {
        this.metadataRootPath = metadataRootPath;
    }

    public Map<String, DocumentElement> getDocumentElements() {
        return documentElements;
    }

    public String getMetadataRootPath() {
        return metadataRootPath;
    }
}
