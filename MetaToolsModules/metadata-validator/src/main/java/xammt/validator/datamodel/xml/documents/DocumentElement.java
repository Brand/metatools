package xammt.validator.datamodel.xml.documents;

import org.w3c.dom.Document;

public class DocumentElement {

    private String canonicalPath;
    private Document document;

    public DocumentElement(String canonicalPath, Document document) {
        this.canonicalPath = canonicalPath;
        this.document = document;
    }

    public String getCanonicalPath() {
        return canonicalPath;
    }

    public Document getDocument() {
        return document;
    }
}
