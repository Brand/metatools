package xammt.validator.interfaces;

import api.ProgramMetadataModelInterface;

public interface ModelLoader {

    public void loadModel();
    public ProgramMetadataModelInterface getModel();
}
