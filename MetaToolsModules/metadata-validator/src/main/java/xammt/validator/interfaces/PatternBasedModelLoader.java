package xammt.validator.interfaces;

import api.ProgramMetadataModelInterface;
import org.w3c.dom.Document;
import xammt.validator.datamodel.xml.documents.DocumentsContainer;

import java.io.File;

public interface PatternBasedModelLoader extends ModelLoader {

    void setXmlMetadataSources(File metadataSource);

    void setXmlMetadataSources(DocumentsContainer documentsContainer);

    void setBindingDocument(Document bindingDocument);

    void setProgramModel(ProgramMetadataModelInterface model);
}
