package xammt.validator.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created with IntelliJ IDEA.
 * User: Najdaros
 * Date: 18.3.2013
 * Time: 10:14
 * To change this template use File | Settings | File Templates.
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface ComparatorParameter {
    String name();
    boolean checked();
}
