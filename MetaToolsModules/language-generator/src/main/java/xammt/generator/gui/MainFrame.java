package xammt.generator.gui;

import file.filters.XmlFileFilter;
import file.filters.XsdFileFilter;
import forms.FileChooser;
import xammt.generator.configuration.datamodel.Configuration;
import xammt.generator.configuration.datamodel.JavaOutputType;
import xammt.generator.modules.OutputGenerator;
import xammt.generator.utility.Constants;

import javax.swing.*;
import java.io.File;

public final class MainFrame extends javax.swing.JFrame {

    private FileChooser fileChooser;

    public MainFrame() {
        initComponents();
        buttonGroup.add(jarRadioButton);
        buttonGroup.add(javaRadioButton);
        buttonGroup.add(classRadioButton);
    }

    public MainFrame(Configuration c) {
        this();
        setConfiguration(c);
    }

    public void setConfiguration(Configuration c) {
        pmtdlTextField.setText(c.getPmtdlSourceFile());
        pemdxlTextField.setText(c.getPemdxlSchemasOutputDirectory());
        pemdxlSkeletonsTextField.setText(c.getPemdxlSkeletonsOutputDirectory());
        xamblSchemaTextField.setText(c.getXamblSchemaPath());
        xamblTextField.setText(c.getXamblOutputDirectory());
        xamblSkeletonTextField.setText(c.getXamblSkeletonOutputDirectory());
        jOutputTextField.setText(c.getJavaOutputDirectory());
        if (c.getJavaOutputType() != null) {
            switch (c.getJavaOutputType()) {
                case JAR:
                    jarRadioButton.setSelected(true);
                    break;
                case JAVA:
                    javaRadioButton.setSelected(true);
                    break;
                case CLASS:
                    classRadioButton.setSelected(true);
                    break;
            }
        } else {
            jarRadioButton.setSelected(true);
        }
        enableAccesibleControls();
        generateButton.setEnabled(checkInputs());
    }

    public Configuration getConfiguration() {
        Configuration c = new Configuration();
        c.setPmtdlSourceFile(pmtdlTextField.getText());
        if (pemdxlSchemasCheckBox.isSelected()) {
            c.setPemdxlSchemasOutputDirectory(pemdxlTextField.getText());
        }
        if (pemdxlSkeletonsCheckBox.isSelected()) {
            c.setPemdxlSkeletonsOutputDirectory(pemdxlSkeletonsTextField.getText());
        }
        if (xamblCheckBox.isSelected()) {
            c.setXamblSchemaPath(xamblSchemaTextField.getText());
            c.setXamblOutputDirectory(xamblTextField.getText());
        }
        if (xamblSkeletonsCheckBox.isSelected()) {
            c.setXamblSchemaPath(xamblSchemaTextField.getText());
            c.setXamblSkeletonOutputDirectory(xamblSkeletonTextField.getText());
        }
        if (javaOutputCheckBox.isSelected()) {
            c.setJavaOutputDirectory(jOutputTextField.getText());
            c.setJavaOutputType(getJavaOutputType());
        }
        return c;
    }

    private void enablePemdxlSchemasControls(boolean enable) {
        enableComponents(enable, pemdxlTextField, pemdxlBrowseButton);
        pemdxlSchemasCheckBox.setSelected(enable);
        if (enable) {
            pemdxlSkeletonsCheckBox.setEnabled(enable);
        }
    }

    private void enablePemdxlSchemasOption(boolean enable) {
        enableComponents(enable, pemdxlTextField, pemdxlBrowseButton, pemdxlSkeletonsCheckBox);
        if (!enable) {
            enableComponents(enable, pemdxlSkeletonsTextField, browsePemdxlSkeletonsButton);
            pemdxlSkeletonsCheckBox.setSelected(enable);
        }
    }

    private void enablePemdxlSkeletonsControls(boolean enable) {
        enableComponents(enable, pemdxlSkeletonsTextField, browsePemdxlSkeletonsButton);
        pemdxlSkeletonsCheckBox.setSelected(enable);
        if (enable) {
            enablePemdxlSchemasControls(enable);
        }
    }

    private void enablePemdxlSkeletonsOption(boolean enable) {
        enableComponents(enable, pemdxlSkeletonsTextField, browsePemdxlSkeletonsButton);
    }

    private void enableBindingsSchemaControls(boolean enable) {
        enableComponents(enable, xamblSchemaTextField, browseXamblSchemaButton);
    }

    private void enableBindingsControls(boolean enable) {
        enableComponents(enable, xamblTextField, xamblBrowseButton);
        xamblCheckBox.setSelected(enable);
        if (enable) {
            enableComponents(enable, xamblSchemaTextField, browseXamblSchemaButton);
        }
    }

    private void enableBindingsOption(boolean enable) {
        enableComponents(enable, xamblTextField, xamblBrowseButton);
        if (!enable && !xamblSkeletonsCheckBox.isSelected()) {
            enableBindingsSchemaControls(enable);
        } else if (enable && !xamblSkeletonsCheckBox.isSelected()) {
            enableBindingsSchemaControls(enable);
        }
    }

    private void enableBindingsSkeletonsControls(boolean enable) {
        enableComponents(enable, xamblSkeletonTextField, xamblBindingsSkeletonButton);
        xamblSkeletonsCheckBox.setSelected(enable);
        if (enable) {
            enableComponents(enable, xamblSchemaTextField, browseXamblSchemaButton);
        }
    }

    private void enableBindingsSkeletonsOption(boolean enable) {
        enableComponents(enable, xamblSkeletonTextField, xamblBindingsSkeletonButton);
        if (!enable && !xamblCheckBox.isSelected()) {
            enableBindingsSchemaControls(enable);
        } else if (enable && !xamblCheckBox.isSelected()) {
            enableBindingsSchemaControls(enable);
        }
    }

    private void enableJavaControls(boolean enable) {
        enableComponents(enable,
                jOutputTextField,
                jOutputBrowseButton,
                jarRadioButton,
                javaRadioButton,
                classRadioButton);
        javaOutputCheckBox.setSelected(enable);
    }

    private void enableComponents(boolean enabled, JComponent... components) {
        for (JComponent jComponent : components) {
            jComponent.setEnabled(enabled);
        }
    }

    private void selectCheckBoxes(boolean selected, JCheckBox... checkBoxes) {
        for (JCheckBox jCheckBox : checkBoxes) {
            jCheckBox.setSelected(selected);
        }
    }

    private void enableAccesibleControls() {
        enablePemdxlSchemasControls(pemdxlTextField.getText() != null && !pemdxlTextField.getText().equals(""));
        enablePemdxlSkeletonsControls(pemdxlSkeletonsTextField.getText() != null && !pemdxlSkeletonsTextField.getText().equals(""));
        enableBindingsControls(xamblTextField.getText() != null && !xamblTextField.getText().equals(""));
        enableBindingsSkeletonsControls(xamblSkeletonTextField.getText() != null && !xamblSkeletonTextField.getText().equals(""));
        enableBindingsSchemaControls(xamblSkeletonsCheckBox.isSelected() || xamblCheckBox.isSelected());
        enableJavaControls(jOutputTextField.getText() != null && !jOutputTextField.getText().equals(""));
    }

    private JavaOutputType getJavaOutputType() {
        if (jarRadioButton.isSelected()) {
            return JavaOutputType.JAR;
        } else if (javaRadioButton.isSelected()) {
            return JavaOutputType.JAVA;
        } else if (classRadioButton.isSelected()) {
            return JavaOutputType.CLASS;
        } else {
            return null;
        }
    }

    private boolean checkInputs() {
        boolean xamclSourcePathOk = pmtdlTextField.getText() != null && !pmtdlTextField.getText().equals("");
        boolean xpmdlSchemasPathOk = (pemdxlSchemasCheckBox.isSelected() && pemdxlTextField.getText() != null && !pemdxlTextField.getText().equals("")) || !pemdxlSchemasCheckBox.isSelected();
        boolean xpmdlSkeletonsPathOk = (pemdxlSkeletonsCheckBox.isSelected() && pemdxlSkeletonsTextField.getText() != null && !pemdxlSkeletonsTextField.getText().equals("")) || !pemdxlSkeletonsCheckBox.isSelected();
        boolean bindingsSchemaPathOk = ((xamblSkeletonsCheckBox.isSelected() || xamblCheckBox.isSelected()) && xamblSchemaTextField.getText() != null && !xamblSchemaTextField.getText().equals("")) || !(xamblSkeletonsCheckBox.isSelected() || xamblCheckBox.isSelected());
        boolean bindingsPathOk = (xamblCheckBox.isSelected() && xamblTextField.getText() != null && !xamblTextField.getText().equals("")) || !xamblCheckBox.isSelected();
        boolean bindingsSkeletonPathOk = (xamblSkeletonsCheckBox.isSelected() && xamblSkeletonTextField.getText() != null && !xamblSkeletonTextField.getText().equals("")) || !xamblSkeletonsCheckBox.isSelected();
        boolean javaPathOk = (javaOutputCheckBox.isSelected() && jOutputTextField.getText() != null && !jOutputTextField.getText().equals("")) || !javaOutputCheckBox.isSelected();
        return xamclSourcePathOk && xpmdlSchemasPathOk && xpmdlSkeletonsPathOk && bindingsSchemaPathOk && bindingsPathOk && bindingsSkeletonPathOk && javaPathOk;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup = new javax.swing.ButtonGroup();
        pmtdlBrowseButton = new javax.swing.JButton();
        pemdxlBrowseButton = new javax.swing.JButton();
        xamblBrowseButton = new javax.swing.JButton();
        jOutputBrowseButton = new javax.swing.JButton();
        pmtdlLabel = new javax.swing.JLabel();
        pmtdlTextField = new javax.swing.JTextField();
        pemdxlTextField = new javax.swing.JTextField();
        pemdxlLabel = new javax.swing.JLabel();
        xamblTextField = new javax.swing.JTextField();
        jOutputTextField = new javax.swing.JTextField();
        xamblLabel = new javax.swing.JLabel();
        jOutputLabel = new javax.swing.JLabel();
        jOutputTypeLabel = new javax.swing.JLabel();
        jarRadioButton = new javax.swing.JRadioButton();
        javaRadioButton = new javax.swing.JRadioButton();
        classRadioButton = new javax.swing.JRadioButton();
        generateButton = new javax.swing.JButton();
        pemdxlSchemasCheckBox = new javax.swing.JCheckBox();
        xamblCheckBox = new javax.swing.JCheckBox();
        javaOutputCheckBox = new javax.swing.JCheckBox();
        pemdxlSkeletonsCheckBox = new javax.swing.JCheckBox();
        pemdxlSkeletonsLabel = new javax.swing.JLabel();
        pemdxlSkeletonsTextField = new javax.swing.JTextField();
        browsePemdxlSkeletonsButton = new javax.swing.JButton();
        xamblSchemaLabel = new javax.swing.JLabel();
        xamblSchemaTextField = new javax.swing.JTextField();
        browseXamblSchemaButton = new javax.swing.JButton();
        xamblSkeletonsCheckBox = new javax.swing.JCheckBox();
        xamblBindingsSkeletonButton = new javax.swing.JButton();
        xamblSkeletonLabel = new javax.swing.JLabel();
        xamblSkeletonTextField = new javax.swing.JTextField();
        menuBar = new javax.swing.JMenuBar();
        configurationMenu = new javax.swing.JMenu();
        loadMenuItem = new javax.swing.JMenuItem();
        saveMenuItem = new javax.swing.JMenuItem();
        saveAsMenuItem = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Language Generator");
        setLocationByPlatform(true);
        setResizable(false);

        pmtdlBrowseButton.setText("Browse");
        pmtdlBrowseButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pmtdlBrowseButtonActionPerformed(evt);
            }
        });

        pemdxlBrowseButton.setText("Browse");
        pemdxlBrowseButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pemdxlBrowseButtonActionPerformed(evt);
            }
        });

        xamblBrowseButton.setText("Browse");
        xamblBrowseButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                xamblBrowseButtonActionPerformed(evt);
            }
        });

        jOutputBrowseButton.setText("Browse");
        jOutputBrowseButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jOutputBrowseButtonActionPerformed(evt);
            }
        });

        pmtdlLabel.setText("PMTDL source file :");

        pemdxlLabel.setText("PEMDXL output schemas directory :");

        xamblLabel.setText("XAMBL output directory :");

        jOutputLabel.setText("Annotations output directory :");

        jOutputTypeLabel.setText("Annotations output type :");

        jarRadioButton.setSelected(true);
        jarRadioButton.setText("jar file");

        javaRadioButton.setText("java files");

        classRadioButton.setText("class files");

        generateButton.setText("Generate");
        generateButton.setEnabled(false);
        generateButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                generateButtonActionPerformed(evt);
            }
        });

        pemdxlSchemasCheckBox.setSelected(true);
        pemdxlSchemasCheckBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pemdxlSchemasCheckBoxActionPerformed(evt);
            }
        });

        xamblCheckBox.setSelected(true);
        xamblCheckBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                xamblCheckBoxActionPerformed(evt);
            }
        });

        javaOutputCheckBox.setSelected(true);
        javaOutputCheckBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                javaOutputCheckBoxActionPerformed(evt);
            }
        });

        pemdxlSkeletonsCheckBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pemdxlSkeletonsCheckBoxActionPerformed(evt);
            }
        });

        pemdxlSkeletonsLabel.setText("PEMDXL output skeletons directory :");

        pemdxlSkeletonsTextField.setEnabled(false);

        browsePemdxlSkeletonsButton.setText("Browse");
        browsePemdxlSkeletonsButton.setEnabled(false);
        browsePemdxlSkeletonsButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                browsePemdxlSkeletonsButtonActionPerformed(evt);
            }
        });

        xamblSchemaLabel.setText("XAMBL schema location :");

        browseXamblSchemaButton.setText("Browse");
        browseXamblSchemaButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                browseXamblSchemaButtonActionPerformed(evt);
            }
        });

        xamblSkeletonsCheckBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                xamblSkeletonsCheckBoxActionPerformed(evt);
            }
        });

        xamblBindingsSkeletonButton.setText("Browse");
        xamblBindingsSkeletonButton.setEnabled(false);
        xamblBindingsSkeletonButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                xamblBindingsSkeletonButtonActionPerformed(evt);
            }
        });

        xamblSkeletonLabel.setText("XAMBL output skeletons directory :");

        xamblSkeletonTextField.setEnabled(false);

        configurationMenu.setText("Configuration");

        loadMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_L, java.awt.event.InputEvent.CTRL_MASK));
        loadMenuItem.setText("Load");
        loadMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                loadMenuItemActionPerformed(evt);
            }
        });
        configurationMenu.add(loadMenuItem);

        saveMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.CTRL_MASK));
        saveMenuItem.setText("Save");
        saveMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveMenuItemActionPerformed(evt);
            }
        });
        configurationMenu.add(saveMenuItem);

        saveAsMenuItem.setText("Save as");
        saveAsMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveAsMenuItemActionPerformed(evt);
            }
        });
        configurationMenu.add(saveAsMenuItem);

        menuBar.add(configurationMenu);

        setJMenuBar(menuBar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(xamblSkeletonsCheckBox)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(xamblSkeletonLabel))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(javaOutputCheckBox)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jOutputLabel)
                                    .addComponent(jOutputTypeLabel))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jOutputTextField, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 586, Short.MAX_VALUE)
                                    .addComponent(xamblSkeletonTextField, javax.swing.GroupLayout.DEFAULT_SIZE, 586, Short.MAX_VALUE))
                                .addGap(6, 6, 6))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jarRadioButton)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(javaRadioButton)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(classRadioButton)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(xamblBindingsSkeletonButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jOutputBrowseButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(generateButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(xamblCheckBox)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(xamblLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(xamblSchemaLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(pemdxlSkeletonsCheckBox)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(pemdxlSkeletonsLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 175, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(pemdxlSchemasCheckBox)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(pemdxlLabel))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(27, 27, 27)
                                .addComponent(pmtdlLabel)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(pemdxlSkeletonsTextField, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 585, Short.MAX_VALUE)
                            .addComponent(pemdxlTextField, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 585, Short.MAX_VALUE)
                            .addComponent(pmtdlTextField, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 585, Short.MAX_VALUE)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(xamblSchemaTextField)
                                .addComponent(xamblTextField, javax.swing.GroupLayout.DEFAULT_SIZE, 585, Short.MAX_VALUE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(browsePemdxlSkeletonsButton, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 77, Short.MAX_VALUE)
                            .addComponent(browseXamblSchemaButton, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 77, Short.MAX_VALUE)
                            .addComponent(xamblBrowseButton, javax.swing.GroupLayout.DEFAULT_SIZE, 77, Short.MAX_VALUE)
                            .addComponent(pemdxlBrowseButton, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 77, Short.MAX_VALUE)
                            .addComponent(pmtdlBrowseButton, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 77, Short.MAX_VALUE))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(pmtdlBrowseButton)
                    .addComponent(pmtdlTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(pmtdlLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pemdxlSchemasCheckBox)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(pemdxlBrowseButton)
                        .addComponent(pemdxlTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(pemdxlLabel)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pemdxlSkeletonsCheckBox)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(pemdxlSkeletonsLabel)
                        .addComponent(pemdxlSkeletonsTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(browsePemdxlSkeletonsButton)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(browseXamblSchemaButton)
                    .addComponent(xamblSchemaTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(xamblSchemaLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(xamblBrowseButton)
                        .addComponent(xamblTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(xamblLabel))
                    .addComponent(xamblCheckBox))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(xamblSkeletonTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(xamblBindingsSkeletonButton)
                        .addComponent(xamblSkeletonLabel))
                    .addComponent(xamblSkeletonsCheckBox))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jOutputBrowseButton)
                        .addComponent(jOutputTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jOutputLabel))
                    .addComponent(javaOutputCheckBox))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jOutputTypeLabel)
                    .addComponent(generateButton)
                    .addComponent(jarRadioButton)
                    .addComponent(javaRadioButton)
                    .addComponent(classRadioButton))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void pmtdlBrowseButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pmtdlBrowseButtonActionPerformed
        fileChooser = new FileChooser(new XmlFileFilter());
        fileChooser.showOpenDialog(this);
        if (fileChooser.getOption() == FileChooser.APPROVE_OPTION && fileChooser.getSelectedFile() != null) {
            pmtdlTextField.setText(fileChooser.getSelectedFile().getPath());
        }
        generateButton.setEnabled(checkInputs());
    }//GEN-LAST:event_pmtdlBrowseButtonActionPerformed

    private void pemdxlBrowseButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pemdxlBrowseButtonActionPerformed
        fileChooser = new FileChooser(null);
        fileChooser.showOpenDialog(this);
        if (fileChooser.getOption() == FileChooser.APPROVE_OPTION && fileChooser.getSelectedFile() != null) {
            pemdxlTextField.setText(fileChooser.getSelectedFile().getPath());
        }
        generateButton.setEnabled(checkInputs());
    }//GEN-LAST:event_pemdxlBrowseButtonActionPerformed

    private void xamblBrowseButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_xamblBrowseButtonActionPerformed
        fileChooser = new FileChooser(null);
        fileChooser.showOpenDialog(this);
        if (fileChooser.getOption() == FileChooser.APPROVE_OPTION && fileChooser.getSelectedFile() != null) {
            xamblTextField.setText(fileChooser.getSelectedFile().getPath());
        }
        generateButton.setEnabled(checkInputs());
    }//GEN-LAST:event_xamblBrowseButtonActionPerformed

    private void jOutputBrowseButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jOutputBrowseButtonActionPerformed
        fileChooser = new FileChooser(null);
        fileChooser.showOpenDialog(this);
        if (fileChooser.getOption() == FileChooser.APPROVE_OPTION && fileChooser.getSelectedFile() != null) {
            jOutputTextField.setText(fileChooser.getSelectedFile().getPath());
        }
        generateButton.setEnabled(checkInputs());
    }//GEN-LAST:event_jOutputBrowseButtonActionPerformed

    private void generateButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_generateButtonActionPerformed
        OutputGenerator og = new OutputGenerator();
        og.setConfiguration(this.getConfiguration());
        og.generate();
        JOptionPane.showMessageDialog(this, "Output was generated.", "Information", JOptionPane.INFORMATION_MESSAGE);
    }//GEN-LAST:event_generateButtonActionPerformed

    private void loadMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_loadMenuItemActionPerformed
        fileChooser = new FileChooser(new XmlFileFilter());
        fileChooser.showOpenDialog(this);
        if (fileChooser.getOption() == FileChooser.APPROVE_OPTION && fileChooser.getSelectedFile() != null) {
            String path = fileChooser.getSelectedFile().getPath();
            Configuration c = Configuration.loadConfiguration(new File(path));
            setConfiguration(c);
        }
        generateButton.setEnabled(checkInputs());
    }//GEN-LAST:event_loadMenuItemActionPerformed

    private void saveMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveMenuItemActionPerformed
        Configuration.saveConfiguration(new File(Constants.DEFAULT_CONFIGURATION_FILE), getConfiguration());
    }//GEN-LAST:event_saveMenuItemActionPerformed

    private void browsePemdxlSkeletonsButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_browsePemdxlSkeletonsButtonActionPerformed
        fileChooser = new FileChooser(null);
        fileChooser.showOpenDialog(this);
        if (fileChooser.getOption() == FileChooser.APPROVE_OPTION && fileChooser.getSelectedFile() != null) {
            pemdxlSkeletonsTextField.setText(fileChooser.getSelectedFile().getPath());
        }
        generateButton.setEnabled(checkInputs());
    }//GEN-LAST:event_browsePemdxlSkeletonsButtonActionPerformed

    private void browseXamblSchemaButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_browseXamblSchemaButtonActionPerformed
        fileChooser = new FileChooser(new XsdFileFilter());
        fileChooser.showOpenDialog(this);
        if (fileChooser.getOption() == FileChooser.APPROVE_OPTION && fileChooser.getSelectedFile() != null) {
            xamblSchemaTextField.setText(fileChooser.getSelectedFile().getPath());
        }
        generateButton.setEnabled(checkInputs());
    }//GEN-LAST:event_browseXamblSchemaButtonActionPerformed

    private void xamblBindingsSkeletonButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_xamblBindingsSkeletonButtonActionPerformed
        fileChooser = new FileChooser(null);
        fileChooser.showOpenDialog(this);
        if (fileChooser.getOption() == FileChooser.APPROVE_OPTION && fileChooser.getSelectedFile() != null) {
            xamblSkeletonTextField.setText(fileChooser.getSelectedFile().getPath());
        }
        generateButton.setEnabled(checkInputs());
    }//GEN-LAST:event_xamblBindingsSkeletonButtonActionPerformed

    private void pemdxlSchemasCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pemdxlSchemasCheckBoxActionPerformed
        enablePemdxlSchemasOption(pemdxlSchemasCheckBox.isSelected());
        generateButton.setEnabled(checkInputs());
    }//GEN-LAST:event_pemdxlSchemasCheckBoxActionPerformed

    private void pemdxlSkeletonsCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pemdxlSkeletonsCheckBoxActionPerformed
        enablePemdxlSkeletonsOption(pemdxlSkeletonsCheckBox.isSelected());
        generateButton.setEnabled(checkInputs());
    }//GEN-LAST:event_pemdxlSkeletonsCheckBoxActionPerformed

    private void xamblCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_xamblCheckBoxActionPerformed
        enableBindingsOption(xamblCheckBox.isSelected());
        generateButton.setEnabled(checkInputs());
    }//GEN-LAST:event_xamblCheckBoxActionPerformed

    private void xamblSkeletonsCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_xamblSkeletonsCheckBoxActionPerformed
        enableBindingsSkeletonsOption(xamblSkeletonsCheckBox.isSelected());
        generateButton.setEnabled(checkInputs());
    }//GEN-LAST:event_xamblSkeletonsCheckBoxActionPerformed

    private void javaOutputCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_javaOutputCheckBoxActionPerformed
        enableJavaControls(javaOutputCheckBox.isSelected());
        generateButton.setEnabled(checkInputs());
    }//GEN-LAST:event_javaOutputCheckBoxActionPerformed

    private void saveAsMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveAsMenuItemActionPerformed
        fileChooser = new FileChooser(new XmlFileFilter());
        fileChooser.showSaveDialog(this);
        if (fileChooser.getOption() == FileChooser.APPROVE_OPTION && fileChooser.getSelectedFile() != null && fileChooser.getSelectedFile() != null) {
            File f = fileChooser.getSelectedFile();
            if (!fileChooser.getSelectedFile().getPath().endsWith(".xml")) {
                f = new File(fileChooser.getSelectedFile().getAbsolutePath().concat(".xml"));
            }
            Configuration.saveConfiguration(f, getConfiguration());
        }
    }//GEN-LAST:event_saveAsMenuItemActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton browsePemdxlSkeletonsButton;
    private javax.swing.JButton browseXamblSchemaButton;
    private javax.swing.ButtonGroup buttonGroup;
    private javax.swing.JRadioButton classRadioButton;
    private javax.swing.JMenu configurationMenu;
    private javax.swing.JButton generateButton;
    private javax.swing.JButton jOutputBrowseButton;
    private javax.swing.JLabel jOutputLabel;
    private javax.swing.JTextField jOutputTextField;
    private javax.swing.JLabel jOutputTypeLabel;
    private javax.swing.JRadioButton jarRadioButton;
    private javax.swing.JCheckBox javaOutputCheckBox;
    private javax.swing.JRadioButton javaRadioButton;
    private javax.swing.JMenuItem loadMenuItem;
    private javax.swing.JMenuBar menuBar;
    private javax.swing.JButton pemdxlBrowseButton;
    private javax.swing.JLabel pemdxlLabel;
    private javax.swing.JCheckBox pemdxlSchemasCheckBox;
    private javax.swing.JCheckBox pemdxlSkeletonsCheckBox;
    private javax.swing.JLabel pemdxlSkeletonsLabel;
    private javax.swing.JTextField pemdxlSkeletonsTextField;
    private javax.swing.JTextField pemdxlTextField;
    private javax.swing.JButton pmtdlBrowseButton;
    private javax.swing.JLabel pmtdlLabel;
    private javax.swing.JTextField pmtdlTextField;
    private javax.swing.JMenuItem saveAsMenuItem;
    private javax.swing.JMenuItem saveMenuItem;
    private javax.swing.JButton xamblBindingsSkeletonButton;
    private javax.swing.JButton xamblBrowseButton;
    private javax.swing.JCheckBox xamblCheckBox;
    private javax.swing.JLabel xamblLabel;
    private javax.swing.JLabel xamblSchemaLabel;
    private javax.swing.JTextField xamblSchemaTextField;
    private javax.swing.JLabel xamblSkeletonLabel;
    private javax.swing.JTextField xamblSkeletonTextField;
    private javax.swing.JCheckBox xamblSkeletonsCheckBox;
    private javax.swing.JTextField xamblTextField;
    // End of variables declaration//GEN-END:variables
}
