package xammt.generator.modules.pemdxl;

import data.models.metadata.definition.Target;

/**
 * Trieda reprezentuje element zapuzdrujúci informácie o type
 * metaúdaja definovanom vo výstupnom jazyku PEMDXL.
 * @author Martin Pribula
 */
public class PemdxlCodeUnit {

    /**
     * Identifikátor metaúdaja.
     */
    private String id;
    /**
     * Kód metaúdaja, ktorý sa generuje do výstupnej XML schémy.
     */
    private String code;
    /*
     * Typ cieľového programového elementu, ku ktorému môže
     * byť daný metaúdaj definovaný.
     */
    private Target target;

    /**
     * Konštruktor triedy.
     * @param id Identifikátor metaúdaja.
     * @param code Kód metaúdaja.
     * @param target Typ cieľového programového elementu
     */
    public PemdxlCodeUnit(String id, String code, Target target) {
        this.id = id;
        this.code = code;
        this.target = target;
    }

    /**
     * Metóda získavajúca hodnotu členskej premennej code.
     * @return Hodnota členskej premennej code.
     */
    public String getCode() {
        return code;
    }

    /**
     * Metóda získavajúca hodnotu členskej premennej id.
     * @return Hodnota členskej premennej id.
     */
    public String getId() {
        return id;
    }

    /**
     * Metóda získavajúca hodnotu členskej premennej id.
     * @return Hodnota členskej premennej id.
     */
    public Target getTarget() {
        return target;
    }
}
