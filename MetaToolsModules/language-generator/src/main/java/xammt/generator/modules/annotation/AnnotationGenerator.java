package xammt.generator.modules.annotation;

import com.sun.tools.javac.api.JavacTool;
import data.models.metadata.definition.Annotation;
import data.models.metadata.definition.PmtdlDefinition;
import data.models.program.element.IdentificatorParser;
import org.apache.velocity.VelocityContext;
import xammt.generator.configuration.datamodel.JavaOutputType;
import xammt.generator.modules.TemplateGenerating;
import xammt.generator.utility.Constants;
import xammt.generator.utility.Utility;

import javax.tools.JavaCompiler;
import javax.tools.JavaCompiler.CompilationTask;
import javax.tools.JavaFileObject;
import javax.tools.StandardJavaFileManager;
import javax.tools.StandardLocation;
import java.io.*;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.jar.Attributes;
import java.util.jar.JarEntry;
import java.util.jar.JarOutputStream;
import java.util.jar.Manifest;

/**
 * Modul jazykového generátora nástroja XAMMT, zabezpečujúci
 * generovanie anotačného jazyka.
 * @author Martin Pribula
 */
public class AnnotationGenerator {

    /**
     * Definícia štruktúry cieľových metaúdajov, ktoré výstupný
     * jazyk umožňuje generovať.
     */
    private PmtdlDefinition xamclDefinition;
    /**
     * Výstupný adresár pre uloženie anotačného jazyka v
     * definovanej podobe.
     */
    private File outputDirectory;
    /**
     * Výstupná forma anotačného jazyka. Prípustná forma je
     * v podobe .jar, .java a .class súborov
     */
    private JavaOutputType javaOutput;

    /**
     * Konštruktor triedy.
     * @param xamclDefinition Definícia štruktúry cieľových metaúdajov.
     * @param outputDirectory Výstupný adresár.
     * @param javaOutput Forma anotačného jazyka.
     */
    public AnnotationGenerator(PmtdlDefinition xamclDefinition, File outputDirectory, JavaOutputType javaOutput) {
        this.xamclDefinition = xamclDefinition;
        if (!outputDirectory.exists()) {
            outputDirectory.mkdirs();
        }
        this.outputDirectory = outputDirectory;
        this.javaOutput = javaOutput;
    }

    /**
     * Metóda ralizujúca samotné generovanie výstupného anotačného
     * jazyka.
     */
    public void generateOutput() {
        Iterable<? extends JavaFileObject> codeUnits = generateCodeUnits(xamclDefinition);
        switch (javaOutput) {
            case CLASS:
                generateClassFiles(codeUnits);
                break;
            case JAR:
                generateJarFiles(codeUnits);
                break;
            case JAVA:
                generateJavaFiles(codeUnits);
                break;
        }
    }

    /**
     * Zabezpečenie generovanie jazyka vo forme prekompilovaných .class súborov.
     * @param compilationUnits Elementy určené pre kompiláciu a generovanie .class súborov.
     */
    private void generateClassFiles(Iterable<? extends JavaFileObject> compilationUnits) {
        try {
            JavaCompiler compiler = JavacTool.create();
            StandardJavaFileManager fileManager = compiler.getStandardFileManager(null, Locale.getDefault(), null);
            fileManager.setLocation(StandardLocation.CLASS_OUTPUT, Collections.singleton(new File(outputDirectory.getPath())));
            CompilationTask task = compiler.getTask(null, fileManager, null, null, null, compilationUnits);
            task.call();
            fileManager.close();
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

    /**
     * Zabezpečenie generovanie jazyka vo forme .jar súboru.
     * @param compilationUnits Elementy určené pre kompiláciu a generovanie .jar súboru.
     */
    private void generateJarFiles(Iterable<? extends JavaFileObject> codeUnits) {
        Utility utility = new Utility();
        generateClassFiles(codeUnits);

        try {
            Manifest manifest = new Manifest();
            manifest.getMainAttributes().put(Attributes.Name.MANIFEST_VERSION, "1.0");
            OutputStream outputStream = new FileOutputStream(new File(outputDirectory + "\\" + xamclDefinition.getName() + ".jar"));
            JarOutputStream jarOutputStream = new JarOutputStream(outputStream, manifest);

            for (JavaFileObject javaFileObject : codeUnits) {
                AnnotationCodeUnit codeUnit = (AnnotationCodeUnit) javaFileObject;
                File classFile = new File(outputDirectory + "\\" + utility.packageNameToWindowsPath(codeUnit.getPackageName()) + "\\" + codeUnit.getClassName() + ".class");

                if (!classFile.exists()) {
                    continue;
                }

                InputStream is = new FileInputStream(classFile);
                int c;

                jarOutputStream.putNextEntry(new JarEntry(utility.packageNameToJavaPath(codeUnit.getPackageName()) + "/" + codeUnit.getClassName() + ".class"));
                while ((c = is.read()) != -1) {
                    jarOutputStream.write(c);
                }
                is.close();
            }

            utility.deleteDir(new File(outputDirectory + "\\" + xamclDefinition.getName()));
            jarOutputStream.close();
            outputStream.close();
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

    /**
     * Zabezpečenie generovanie jazyka vo forme prekompilovanýhc .java súborov.
     * @param compilationUnits Elementy určené pre kompiláciu a generovanie .java súborov.
     */
    private void generateJavaFiles(Iterable<? extends JavaFileObject> codeUnits) {
        Utility utility = new Utility();

        for (JavaFileObject javaFileObject : codeUnits) {
            FileOutputStream fileOutputStream = null;

            try {

                AnnotationCodeUnit codeUnit = (AnnotationCodeUnit) javaFileObject;
                String codeUnitsOutputDirectory = utility.getOutputDirectory(utility.packageNameToWindowsPath(codeUnit.getPackageName()), outputDirectory.getPath());
                new File(codeUnitsOutputDirectory).mkdirs();

                fileOutputStream = new FileOutputStream(new File(codeUnitsOutputDirectory + "\\" + codeUnit.getClassName() + ".java"));
                Writer writer = new OutputStreamWriter(fileOutputStream);

                writer.write(codeUnit.getCode());
                writer.close();

            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            } finally {
                try {
                    fileOutputStream.close();
                } catch (IOException ex) {
                    System.out.println(ex.getMessage());
                }
            }
        }
    }

    /**
     * Generovanie elementov reprezentujúcich anotačné typy, z ktorých
     * je vytváraný výstupný jazyk.
     * @param xamclDefinition Definícia štruktúry cieľových anotačných typov.
     * @return Zoznam elementov zapuzdrujúcich údaje o cieľových anotačných typoch.
     */
    private Iterable<? extends JavaFileObject> generateCodeUnits(PmtdlDefinition xamclDefinition) {
        List<AnnotationCodeUnit> codeUnits = new ArrayList<AnnotationCodeUnit>();
        codeUnits.addAll(generateCodeUnits(xamclDefinition.getAnnotation()));
        return codeUnits;
    }

    /**
     * Generovanie elementov reprezentujúcich anotačné typy, z ktorých
     * je vytváraný výstupný anotačný jazyk.
     * @param annotations Zoznam definícií štruktúry cieľových anotačných typov.
     * @return Zoznam elementov zapuzdrujúcich údaje o cieľových anotačných typoch.
     */
    private List<AnnotationCodeUnit> generateCodeUnits(List<Annotation> annotations) {
        List<AnnotationCodeUnit> codeUnits = new ArrayList<AnnotationCodeUnit>();
        for (Annotation annotation : annotations) {
            try {
                codeUnits.add(new AnnotationCodeUnit(IdentificatorParser.getSimpleId(annotation.getId()), generateJavaCode(annotation), IdentificatorParser.getEnclosingElementQualifiedId(annotation.getId())));
            } catch (URISyntaxException ex) {
                System.out.println(ex.getMessage());
            }
        }

        return codeUnits;
    }

    /**
     * Generovanie zdrojového kódu anotačného typu.
     * @param annotation Element zapuzdrujúci údaje o definícii cieľového anotačného typu.
     * @return Zdrojový k=od anotačného typu.
     */
    private String generateJavaCode(Annotation annotation) {
        VelocityContext velocityContext = new VelocityContext();
        velocityContext.put("annotation", annotation);
        velocityContext.put("annotationSimpleId", IdentificatorParser.getSimpleId(annotation.getId()));
        velocityContext.put("package", IdentificatorParser.getEnclosingElementQualifiedId(annotation.getId()));
        velocityContext.put("target", annotation.getTarget() != null ? annotation.getTarget().value() : "");
        velocityContext.put("utility", new Utility());
        return TemplateGenerating.generateCode(velocityContext, Constants.ANNOTATION_TEMPLATE_RESOURCE);
    }
}
