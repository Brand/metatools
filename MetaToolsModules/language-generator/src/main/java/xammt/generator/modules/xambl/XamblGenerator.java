package xammt.generator.modules.xambl;

import data.models.metadata.definition.Annotation;
import data.models.metadata.definition.Attribute;
import data.models.metadata.definition.PmtdlDefinition;
import data.models.metadata.definition.Target;
import data.models.program.element.Type;
import org.apache.velocity.VelocityContext;
import xammt.generator.modules.TemplateGenerating;
import xammt.generator.utility.Constants;

import java.io.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Modul generujúci XAMBL mapovanie medzi výstupným anotačným a PEMDXL jazykom.
 * @author Martin Pribula
 */
public class XamblGenerator {

    // Package metadata XPath bases.
    private static final String PACKAGE_ATTRIBUTE_XPATH_BASE = "/package[@id='qualified_package_id']/metadata/";
    // Annotation metadata XPath bases.
    private static final String ANNOTATION_ATTRIBUTE_XPATH_BASE = "/annotation[@id='qualified_type_id']/metadata/";
    private static final String ANNOTATION_METHOD_ATTRIBUTE_XPATH_BASE = "/annotation[@id='qualified_type_id']/method[@signature='simple_method_signature']/metadata/";
    // Interface metadata XPaths bases.
    private static final String INTERFACE_ATTRIBUTE_XPATH_BASE = "/interface[@id='qualified_type_id']/metadata/";
    private static final String INTERFACE_METHOD_ATTRIBUTE_XPATH_BASE = "/interface[@id='qualified_type_id']/method[@signature='simple_method_signature']/metadata/";
    // Enum metadata XPaths bases.
    private static final String ENUM_ATTRIBUTE_XPATH_BASE = "/enum[@id='qualified_type_id']/metadata/";
    private static final String ENUM_CONSTANT_ATTRIBUTE_XPATH_BASE = "/enum[@id='qualified_type_id']/constant[@id='simple_field_id']/metadata/";
    private static final String ENUM_CONSTRUCTOR_ATTRIBUTE_XPATH_BASE = "/enum[@id='qualified_type_id']/constructor[@signature='simple_method_signature']/metadata/";
    private static final String ENUM_METHOD_ATTRIBUTE_XPATH_BASE = "/enum[@id='qualified_type_id']/method[@signature='simple_method_signature']/metadata/";
    // Class metadata XPaths.
    private static final String CLASS_ATTRIBUTE_XPATH_BASE = "/class[@id='qualified_type_id']/metadata/";
    private static final String CLASS_FIELD_ATTRIBUTE_XPATH_BASE = "/class[@id='qualified_type_id']/field[@id='simple_field_id']/metadata/";
    private static final String CLASS_CONSTRUCTOR_ATTRIBUTE_XPATH_BASE = "/class[@id='qualified_type_id']/constructor[@signature='simple_method_signature']/metadata/";
    private static final String CLASS_METHOD_ATTRIBUTE_XPATH_BASE = "/class[@id='qualified_type_id']/method[@signature='simple_method_signature']/metadata/";
    // Metadata file location constants.
    private static final String PACKAGE_METADATA_FILE_LOCATION = "/simple_package_id/package-level-metadata.xml";
    private static final String TYPE_METADATA_FILE_LOCATION = "/simple_package_id/simple_type_id.xml";
    // Binding code units.
    private Set<XamblCodeUnit> xamblCodeUnits = new HashSet<XamblCodeUnit>();
    // Generator inputs.
    private PmtdlDefinition pmtdlDefinition;
    private File outputDirectory;

    /**
     * Konštruktor triedy.
     * @param metadata Opis štruktúry používaných metaúdajov.
     * @param outputDirectory Výstupný adresár generovania.
     */
    public XamblGenerator(PmtdlDefinition metadata, File outputDirectory) {
        this.pmtdlDefinition = metadata;
        if (!outputDirectory.exists()) {
            outputDirectory.mkdirs();
        }
        this.outputDirectory = outputDirectory;
    }

    /**
     * Generovanie XAMBL mapovania medzi výstupným anotačným a
     * PEMDXL jazykom do určeného súboru.
     */
    public void generateOutput() {
        FileOutputStream fileOutputStream = null;
        try {
            generateBindingCodeUnits(pmtdlDefinition.getAnnotation());
            VelocityContext velocityContext = new VelocityContext();
            velocityContext.put("xamblCodeUnits", xamblCodeUnits);
            String fileContent = TemplateGenerating.generateCode(velocityContext, Constants.XAMBL_TEMPLATE_RESOURCE);
            fileOutputStream = new FileOutputStream(new File(outputDirectory + "\\" + pmtdlDefinition.getName() + "-xambl.xml"));
            Writer writer = new OutputStreamWriter(fileOutputStream);
            writer.write(fileContent);
            writer.close();
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        } finally {
            try {
                fileOutputStream.close();
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }

    /**
     * Generovanie elementov, predstavujúcich vzájomnú vazbu rôznych foriem metaúdaja.
     * @param annotations Štruktúra cieľových metaúdajov.
     */
    public void generateBindingCodeUnits(List<Annotation> annotations) {
        for (Annotation annotation : annotations) {
            generateBindingCodeUnits(annotation);
        }
    }

    /**
     * Generovanie elementov, predstavujúcich vzájomnú vazbu rôznych foriem metaúdaja.
     * @param annotation Štruktúra cieľových metaúdajov.
     */
    public void generateBindingCodeUnits(Annotation annotation) {
        if (!annotation.getAttribute().isEmpty()) {
            for (Attribute attribute : annotation.getAttribute()) {
                generatingAndPlacingBasedOnTarget(attribute.getId(), annotation.getId(), annotation.getTarget(), metadata.Type.ATTRIBUTE);
            }
        } else {
            generatingAndPlacingBasedOnTarget(null, annotation.getId(), annotation.getTarget(), metadata.Type.ANNOTATION);
        }
    }

    /**
     * Generovanie väzbových elementov a ich umiestnenie do zoznamu na základe
     * cieľového programového elementu.
     * @param attributeId Identifikátor atribútu metaúdaja.
     * @param annotationId Identifikátor metaúdaja.
     * @param target Typ cieľového programového elementu.
     * @param type Typ metaúdaja (anotácia alebo atribút anotácie).
     */
    private void generatingAndPlacingBasedOnTarget(String attributeId, String annotationId, Target target, metadata.Type type) {
        if (target != null) {
            if (target.equals(Target.TYPE)) {
                xamblCodeUnits.add(generateBindingCodeUnit(attributeId, annotationId, Type.CLASS, type));
                xamblCodeUnits.add(generateBindingCodeUnit(attributeId, annotationId, Type.INTERFACE, type));
                xamblCodeUnits.add(generateBindingCodeUnit(attributeId, annotationId, Type.ENUM, type));
            } else if (target.equals(Target.METHOD)) {
                xamblCodeUnits.add(generateBindingCodeUnit(attributeId, annotationId, Type.ANNOTATION_METHOD, type));
                xamblCodeUnits.add(generateBindingCodeUnit(attributeId, annotationId, Type.METHOD, type));
                xamblCodeUnits.add(generateBindingCodeUnit(attributeId, annotationId, Type.ENUM_METHOD, type));
                xamblCodeUnits.add(generateBindingCodeUnit(attributeId, annotationId, Type.INTERFACE_METHOD, type));
            } else if (target.equals(Target.ANNOTATION_TYPE)) {
                xamblCodeUnits.add(generateBindingCodeUnit(attributeId, annotationId, Type.ANNOTATION, type));
            } else if (target.equals(Target.CONSTRUCTOR)) {
                xamblCodeUnits.add(generateBindingCodeUnit(attributeId, annotationId, Type.CONSTRUCTOR, type));
                xamblCodeUnits.add(generateBindingCodeUnit(attributeId, annotationId, Type.ENUM_CONSTRUCTOR, type));
            } else if (target.equals(Target.FIELD)) {
                xamblCodeUnits.add(generateBindingCodeUnit(attributeId, annotationId, Type.FIELD, type));
                xamblCodeUnits.add(generateBindingCodeUnit(attributeId, annotationId, Type.CONSTANT, type));
            } else if (target.equals(Target.PACKAGE)) {
                xamblCodeUnits.add(generateBindingCodeUnit(attributeId, annotationId, Type.PACKAGE, type));
            }
        } else {
            for (Type targetType : Type.values()) {
                xamblCodeUnits.add(generateBindingCodeUnit(attributeId, annotationId, targetType, type));
            }
        }
    }

    /**
     * Generovanie väzbového elementu, ktorý zapuzdruje údaje o
     * mapovaní metaúdaja medzi výstupným anotačným a PEMDXL jazykom.
     * @param attributeId Identifikátor atribútu metaúdaja.
     * @param annotationId Identifikátor metaúdaja.
     * @param targetType Typ cieľového programového elementu.
     * @param type Typ metaúdaja (anotácia alebo atribút anotácie).
     * @return Väzbový element.
     */
    public XamblCodeUnit generateBindingCodeUnit(String attributeId, String annotationId, data.models.program.element.Type targetType, metadata.Type type) {
        VelocityContext velocityContext = new VelocityContext();
        velocityContext.put("id", createQualifiedName(annotationId, attributeId));
        velocityContext.put("xpath", getMetadataXPath(attributeId, annotationId, targetType));
        velocityContext.put("file", getMetadataFileLocation(targetType));
        velocityContext.put("type", type);
        velocityContext.put("targetType", targetType);
        String code = TemplateGenerating.generateCode(velocityContext, Constants.XAMBL_ATTRIBUTE_BINDING_TEMPLATE_RESOURCE);
        String attributeQualifiedId = createQualifiedName(annotationId, attributeId);
        return new XamblCodeUnit(code, attributeQualifiedId, targetType);
    }

    /**
     * Získanie parametrizovanej cesty k súboru s metaúdajmi, 
     * prislúchajúcimi k určitému typu programového elementu.
     * @param type Typ programového elementu.
     * @return Parametrizovaná cesta.
     */
    private String getMetadataFileLocation(Type type) {
        if (type.equals(Type.PACKAGE)) {
            return PACKAGE_METADATA_FILE_LOCATION;
        } else {
            return TYPE_METADATA_FILE_LOCATION;
        }
    }

    /**
     * Získanie parametrizovanej XPath cesty k hodnote metaúdaja, v rámci PEMDXL súboru.
     * @param attributeId Identifikátor attribútu metaúdaja.
     * @param annotationId Identifikátor metaúdaja.
     * @param type Typ metaúdaja (anotácia alebo jej atribút).
     * @return Parametrizovaná XPath cesta.
     */
    private String getMetadataXPath(String attributeId, String annotationId, Type type) {
        String xPathBase = null;
        switch (type) {
            case PACKAGE:
                xPathBase = PACKAGE_ATTRIBUTE_XPATH_BASE;
                break;
            case ANNOTATION:
                xPathBase = ANNOTATION_ATTRIBUTE_XPATH_BASE;
                break;
            case ANNOTATION_METHOD:
                xPathBase = ANNOTATION_METHOD_ATTRIBUTE_XPATH_BASE;
                break;
            case INTERFACE:
                xPathBase = INTERFACE_ATTRIBUTE_XPATH_BASE;
                break;
            case INTERFACE_METHOD:
                xPathBase = INTERFACE_METHOD_ATTRIBUTE_XPATH_BASE;
                break;
            case ENUM:
                xPathBase = ENUM_ATTRIBUTE_XPATH_BASE;
                break;
            case CONSTANT:
                xPathBase = ENUM_CONSTANT_ATTRIBUTE_XPATH_BASE;
                break;
            case ENUM_CONSTRUCTOR:
                xPathBase = ENUM_CONSTRUCTOR_ATTRIBUTE_XPATH_BASE;
                break;
            case ENUM_METHOD:
                xPathBase = ENUM_METHOD_ATTRIBUTE_XPATH_BASE;
                break;
            case CLASS:
                xPathBase = CLASS_ATTRIBUTE_XPATH_BASE;
                break;
            case FIELD:
                xPathBase = CLASS_FIELD_ATTRIBUTE_XPATH_BASE;
                break;
            case METHOD:
                xPathBase = CLASS_METHOD_ATTRIBUTE_XPATH_BASE;
                break;
            case CONSTRUCTOR:
                xPathBase = CLASS_CONSTRUCTOR_ATTRIBUTE_XPATH_BASE;
                break;
        }
        String xPath = xPathBase + annotationId + (attributeId != null ? "/" + attributeId : "");
        return xPath;
    }

    /**
     * Vytváranie kvalifikovaného mena metaúdaja resp atribútu metaúdaja.
     * @param annotationId Identifikátor metaúdaja.
     * @param attributeId Identifikátor atribútu metaúdaja.
     * @return Kvalifikované meno.
     */
    private String createQualifiedName(String annotationId, String attributeId) {
        return annotationId + (attributeId != null ? "." + attributeId : "");
    }

    /**
     * Metóda pre získanie hodnoty členskej premennej xamblCodeUnits.
     * @return Hodnota členskej premennej xamblCodeUnits.
     */
    public Set<XamblCodeUnit> getBindingCodeUnits() {
        return xamblCodeUnits;
    }
}
