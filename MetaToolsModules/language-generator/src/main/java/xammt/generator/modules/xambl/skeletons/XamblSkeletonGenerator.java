package xammt.generator.modules.xambl.skeletons;

import data.models.metadata.definition.PmtdlDefinition;
import org.apache.velocity.VelocityContext;
import xammt.generator.modules.TemplateGenerating;
import xammt.generator.utility.Constants;

import java.io.*;

/**
 * Modul zabezpečujúci generovanie kostry mapovacieho súboru.
 * @author Martin Pribula
 */
public class XamblSkeletonGenerator {

    /**
     * Definícia štruktúry metaúdajov, ktoré je možné generovaným PEMDXL jazykom definovať.
     */
    private String xamblSchemaPath;
    /**
     * Adresár obsahujúci vygenerované schémy jazyka PEMDXL.
     */
    private String outputDirectoryPath;
    /**
     * Výstupný adresár pre generovanie kostier.
     */
    private PmtdlDefinition pmtdlDefinition;

    /**
     * Konštruktor triedy.
     * @param pmtdlDefinition Definícia štruktúry metaúdajov.
     * @param xamblSchemaPath Adresár obsahujúci vygenerované schémy jazyka XAMBL.
     * @param outputDirectoryPath Výstupný adresár pre generovanie kostier.
     */
    public XamblSkeletonGenerator(PmtdlDefinition pmtdlDefinition, String xamblSchemaPath, String outputDirectoryPath) {
        this.xamblSchemaPath = xamblSchemaPath;
        this.outputDirectoryPath = outputDirectoryPath;
        this.pmtdlDefinition = pmtdlDefinition;
    }

    /**
     * Generovanie výstupu v podobe kostry mapovacieho súboru.
     */
    public void generateOutput() {
        FileOutputStream fileOutputStream = null;
        try {
            VelocityContext velocityContext = new VelocityContext();
            velocityContext.put("xamblSchemaPath", xamblSchemaPath.replaceAll("\\s", "%20"));
            velocityContext.put("pmtdlDefinition", pmtdlDefinition);
            String fileContent = TemplateGenerating.generateCode(velocityContext, Constants.XAMBL_SKELETON_TEMPLATE_RESOURCE);
            fileOutputStream = new FileOutputStream(new File(outputDirectoryPath + "\\" + pmtdlDefinition.getName() + "-xambl-skeleton.xml"));
            Writer writer = new OutputStreamWriter(fileOutputStream);
            writer.write(fileContent);
            writer.close();
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        } finally {
            try {
                fileOutputStream.close();
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }
}
