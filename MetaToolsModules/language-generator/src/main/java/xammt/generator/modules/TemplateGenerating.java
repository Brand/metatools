package xammt.generator.modules;

import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;

import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;

/**
 * Modul jazykového generátora zabezpečujúci podporu šablónového
 * generovania na základe nástroja Apache Velocity.
 * @author Martin Pribula
 */
public abstract class TemplateGenerating {

    /**
     * Statická metóda umožňujúca generovanie textového výstupu
     * pomocou textovej šablóny a parametrov generovania.
     * @param context Parametre generovania.
     * @param templateResource Meno zdroja, reperzentujúceho šablónu generovania.
     * @return Generovaný textový výstup.
     */
    public static String generateCode(VelocityContext context, String templateResource) {
        String code = "";
        try {
            StringWriter writer = new StringWriter();
            Reader reader = new InputStreamReader(TemplateGenerating.class.getResourceAsStream(templateResource));
            Velocity.evaluate(context, writer, "error", reader);
            code = writer.toString();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return code;
    }
}
