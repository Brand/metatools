package xammt.generator.modules.annotation;

import javax.tools.SimpleJavaFileObject;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

/**
 * Element zapuzdrujúci údaje anotačného typu.
 * @author Martin Pribula
 */
public class AnnotationCodeUnit extends SimpleJavaFileObject {

    /**
     * Zdrojový kód anotačného typu.
     */
    private String code;
    /**
     * Názov anotačného typu.
     */
    private String className;
    /**
     * Názov balíka, do ktorého anotačný typ patrí.
     */
    private String packageName;    

    /**
     * Konštruktor triedy.
     * @param className Názov anotačného typu.
     * @param code Zdrojový kód anotačného typu.
     * @param packageName Názov balíka, do ktorého anotačný typ patrí.
     * @throws URISyntaxException
     */
    public AnnotationCodeUnit(String className, String code, String packageName) throws URISyntaxException {
        super(new URI(className + ".java"), Kind.SOURCE);
        this.code = code;
        this.className = className;
        this.packageName = packageName;        
    }

    /**
     * Metóda získavajúca hodnotu členskej premennej code.
     * @return Hodnota členskej premennej code.
     */
    public String getCode() {
        return code;
    }

    /**
     * Metóda získavajúca hodnotu členskej premennej className.
     * @return Hodnota členskej premennej className.
     */
    public String getClassName() {
        return className;
    }

    /**
     * Metóda získavajúca hodnotu členskej premennej packageName.
     * @return Hodnota členskej premennej packageName.
     */
    public String getPackageName() {
        return packageName;
    }

    @Override
    public CharSequence getCharContent(boolean ignoreEncodingErrors) throws IOException {
    		return code;
    }    
}
