package xammt.generator.modules.pemdxl;

import data.models.metadata.definition.Annotation;
import data.models.metadata.definition.PmtdlDefinition;
import org.apache.velocity.VelocityContext;
import xammt.generator.modules.TemplateGenerating;
import xammt.generator.utility.Constants;
import xammt.generator.utility.Utility;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Táto trieda reprezentuje modul, ktorý sprostredkuváva generovanie jazyka PEMDXL.
 * @author Martin Pribula
 */
public class PemdxlGenerator {

    /**
     * Definícia štruktúry metaúdajov, ktoré je možné generovaným jazykom definovať.
     */
    private PmtdlDefinition metadata;
    /**
     * Výstupný adresár, do ktorého sa má generovať výstup.
     */
    private File outputDirectory;
    /**
     * Zoznam všetkých elementov/jednotiek defiujúcich metaúdaj v jazyku PEMDXL.
     */
    private List<PemdxlCodeUnit> pemdxlCodeUnits = new ArrayList<PemdxlCodeUnit>();
    /**
     * Zoznam elementov/jednotiek definujúcich metaúdaj balíka v jazyku PEMDXL.
     */
    private List<PemdxlCodeUnit> packageMetadataPemdxlCodeUnits = new ArrayList<PemdxlCodeUnit>();
    /**
     * Zoznam elementov/jednotiek definujúcich metaúdaj anotačného typu v jazyku PEMDXL.
     */
    private List<PemdxlCodeUnit> annotationMetadataPemdxlCodeUnits = new ArrayList<PemdxlCodeUnit>();
    /**
     * Zoznam elementov/jednotiek definujúcich metaúdaj typu (rozhranie, trieda, anumeračný typ) v jazyku PEMDXL.
     */
    private List<PemdxlCodeUnit> typeMetadataPemdxlCodeUnits = new ArrayList<PemdxlCodeUnit>();
    /**
     * Zoznam elementov/jednotiek definujúcich metaúdaj členskej premennej v jazyku PEMDXL.
     */
    private List<PemdxlCodeUnit> fieldMetadataPemdxlCodeUnits = new ArrayList<PemdxlCodeUnit>();
    /**
     * Zoznam elementov/jednotiek definujúcich metaúdaj konštruktora v jazyku PEMDXL.
     */
    private List<PemdxlCodeUnit> constructorMetadataPemdxlCodeUnits = new ArrayList<PemdxlCodeUnit>();
    /**
     * Zoznam elementov/jednotiek definujúcich metaúdaj metódy v jazyku PEMDXL.
     */
    private List<PemdxlCodeUnit> methodMetadataPemdxlCodeUnits = new ArrayList<PemdxlCodeUnit>();

    /**
     * Konštruktor triedy
     * @param metadata Definícia štruktúry metaúdajov.
     * @param outputDirectory Výstupný adresár.
     */
    public PemdxlGenerator(PmtdlDefinition metadata, File outputDirectory) {
        this.metadata = metadata;
        if (!outputDirectory.exists()) {
            outputDirectory.mkdirs();
        }
        this.outputDirectory = outputDirectory;
    }

    /**
     * Generovanie všetkých XML schém PEMDXL jazyka.
     */
    public void generateOutput() {
        sortAndSeparateCodeUnits();
        generatePemdxlPackagePart();
        generatePemdxlAnnotationPart();
        generatePemdxlClassPart();
        generatePemdxlInterfacePart();
        generatePemdxlEnumPart();
    }

    /**
     * Generovanie XML schémy určenej na definíciu metaúdajov pre triedu.
     */
    private void generatePemdxlClassPart() {
        FileOutputStream fileOutputStream = null;
        try {
            VelocityContext velocityContext = new VelocityContext();
            velocityContext.put("pemdxlCodeUnits", pemdxlCodeUnits);
            velocityContext.put("typeMetadataPemdxlCodeUnits", typeMetadataPemdxlCodeUnits);
            velocityContext.put("fieldMetadataPemdxlCodeUnits", fieldMetadataPemdxlCodeUnits);
            velocityContext.put("constructorMetadataPemdxlCodeUnits", constructorMetadataPemdxlCodeUnits);
            velocityContext.put("methodMetadataPemdxlCodeUnits", methodMetadataPemdxlCodeUnits);
            fileOutputStream = new FileOutputStream(new File(outputDirectory + "\\" + metadata.getName() + "-c-pemdxl" + ".xsd"));
            Writer writer = new OutputStreamWriter(fileOutputStream);
            writer.write(TemplateGenerating.generateCode(velocityContext, Constants.PEMDXL_CLASS_TEMPLATE_RESOURCE));
            writer.close();
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        } finally {
            try {
                fileOutputStream.close();
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }

    /**
     * Generovanie XML schémy určenej na definíciu metaúdajov pre balík.
     */
    private void generatePemdxlPackagePart() {
        FileOutputStream fileOutputStream = null;
        try {
            VelocityContext velocityContext = new VelocityContext();
            velocityContext.put("packageMetadataPemdxlCodeUnits", packageMetadataPemdxlCodeUnits);
            fileOutputStream = new FileOutputStream(new File(outputDirectory + "\\" + metadata.getName() + "-p-pemdxl" + ".xsd"));
            Writer writer = new OutputStreamWriter(fileOutputStream);
            writer.write(TemplateGenerating.generateCode(velocityContext, Constants.PEMDXL_PACKAGE_TEMPLATE_RESOURCE));
            writer.close();
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        } finally {
            try {
                fileOutputStream.close();
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }

    /**
     * Generovanie XML schémy určenej na definíciu metaúdajov pre anotačný typ.
     */
    private void generatePemdxlAnnotationPart() {
        FileOutputStream fileOutputStream = null;
        try {
            VelocityContext velocityContext = new VelocityContext();
            velocityContext.put("annotationMetadataPemdxlCodeUnits", annotationMetadataPemdxlCodeUnits);
            velocityContext.put("methodMetadataPemdxlCodeUnits", methodMetadataPemdxlCodeUnits);
            velocityContext.put("pemdxlCodeUnits", pemdxlCodeUnits);
            fileOutputStream = new FileOutputStream(new File(outputDirectory + "\\" + metadata.getName() + "-@-pemdxl" + ".xsd"));
            Writer writer = new OutputStreamWriter(fileOutputStream);
            writer.write(TemplateGenerating.generateCode(velocityContext, Constants.PEMDXL_ANNOTATION_TEMPLATE_RESOURCE));
            writer.close();
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        } finally {
            try {
                fileOutputStream.close();
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }

    /**
     * Generovanie XML schémy určenej na definíciu metaúdajov pre rozhranie.
     */
    private void generatePemdxlInterfacePart() {
        FileOutputStream fileOutputStream = null;
        try {
            VelocityContext velocityContext = new VelocityContext();
            velocityContext.put("typeMetadataPemdxlCodeUnits", typeMetadataPemdxlCodeUnits);
            velocityContext.put("methodMetadataPemdxlCodeUnits", methodMetadataPemdxlCodeUnits);
            velocityContext.put("pemdxlCodeUnits", pemdxlCodeUnits);
            fileOutputStream = new FileOutputStream(new File(outputDirectory + "\\" + metadata.getName() + "-i-pemdxl" + ".xsd"));
            Writer writer = new OutputStreamWriter(fileOutputStream);
            writer.write(TemplateGenerating.generateCode(velocityContext, Constants.PEMDXL_INTERFACE_TEMPLATE_RESOURCE));
            writer.close();
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        } finally {
            try {
                fileOutputStream.close();
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }

    /**
     * Generovanie XML schémy určenej na definíciu metaúdajov pre enumeračný typ.
     */
    private void generatePemdxlEnumPart() {
        FileOutputStream fileOutputStream = null;
        try {
            VelocityContext velocityContext = new VelocityContext();
            velocityContext.put("typeMetadataPemdxlCodeUnits", typeMetadataPemdxlCodeUnits);
            velocityContext.put("fieldMetadataPemdxlCodeUnits", fieldMetadataPemdxlCodeUnits);
            velocityContext.put("constructorMetadataPemdxlCodeUnits", constructorMetadataPemdxlCodeUnits);
            velocityContext.put("methodMetadataPemdxlCodeUnits", methodMetadataPemdxlCodeUnits);
            velocityContext.put("pemdxlCodeUnits", pemdxlCodeUnits);
            fileOutputStream = new FileOutputStream(new File(outputDirectory + "\\" + metadata.getName() + "-e-pemdxl" + ".xsd"));
            Writer writer = new OutputStreamWriter(fileOutputStream);
            writer.write(TemplateGenerating.generateCode(velocityContext, Constants.PEMDXL_ENUM_TEMPLATE_RESOURCE));
            writer.close();
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        } finally {
            try {
                fileOutputStream.close();
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }

    /**
     * Metóda zabezpečujúca triedenie a oddeľovanie 
     * elementov definujúcich metaúdaje podľa
     * programového elementu, ku ktorému definíciu umožňujú.
     */
    private void sortAndSeparateCodeUnits() {
        for (Annotation annotation : metadata.getAnnotation()) {
            pemdxlCodeUnits.add(new PemdxlCodeUnit(annotation.getId(), generatePemdxlCode(annotation), annotation.getTarget()));
        }
        for (PemdxlCodeUnit pemdxlCodeUnit : pemdxlCodeUnits) {
            placeCodeUnitToCorrectList(pemdxlCodeUnit);
        }

    }

    /**
     * Metóda zabezpečujúca umiestnenie elementu do
     * korektného zoznamu na základe cieľového programového elementu.
     * @param codeUnit Element, definujúci metaúdaj v cieľovom PEMDXL jazyku.
     */
    private void placeCodeUnitToCorrectList(PemdxlCodeUnit codeUnit) {
        if (codeUnit.getTarget() != null) {
            switch (codeUnit.getTarget()) {
                case PACKAGE:
                    packageMetadataPemdxlCodeUnits.add(codeUnit);
                    break;
                case TYPE:
                    typeMetadataPemdxlCodeUnits.add(codeUnit);
                    break;
                case FIELD:
                    fieldMetadataPemdxlCodeUnits.add(codeUnit);
                    break;
                case METHOD:
                    methodMetadataPemdxlCodeUnits.add(codeUnit);
                    break;
                case CONSTRUCTOR:
                    constructorMetadataPemdxlCodeUnits.add(codeUnit);
                    break;
                case ANNOTATION_TYPE:
                    annotationMetadataPemdxlCodeUnits.add(codeUnit);
                    break;
            }
        } else {
            packageMetadataPemdxlCodeUnits.add(codeUnit);
            typeMetadataPemdxlCodeUnits.add(codeUnit);
            constructorMetadataPemdxlCodeUnits.add(codeUnit);
            fieldMetadataPemdxlCodeUnits.add(codeUnit);
            methodMetadataPemdxlCodeUnits.add(codeUnit);
            annotationMetadataPemdxlCodeUnits.add(codeUnit);
        }
    }

    /**
     * Generovanie XSD kódov elementov, definujúcich metaúdaje v cieľovom PEMDXL jazyku.
     * @param annotation Objekt zapuzdrujúci údaje o štruktúre a type metaúdaja,
     * ktorý má byť výsupným elementom definovaný.
     * @return Kód v rámci XML schémy, umožňujúci definíciu metaúdaja.
     */
    private String generatePemdxlCode(Annotation annotation) {
        VelocityContext velocityContext = new VelocityContext();
        velocityContext.put("annotation", annotation);
        velocityContext.put("utility", new Utility());
        return TemplateGenerating.generateCode(velocityContext, Constants.PEMDXL_TYPE_DEFINITION_TEMPLATE_RESOURCE);
    }
}
