package xammt.generator.modules.xambl;

import data.models.program.element.Type;

/**
 * Trieda reprezentuje element, zapuzdrujúci údaje o
 * mapovaní metaúdaja medzi výstupným anotačným a PEMDXL jazykom.
 * @author Martin Pribula
 */
public class XamblCodeUnit {

    /**
     * Mapovacie údaje zapísane v jazyku XML.
     */
    private String code;
    /**
     * Identifikátor metaúdaja.
     */
    private String attributeId;
    /**
     * Cieľový programový element, s ktorým sa 
     * má metaúdaj viazať.
     */
    private Type type;

    public XamblCodeUnit(String code, String attributeId, Type annotationTarget) {
        this.code = code;
        this.attributeId = attributeId;
        this.type = annotationTarget;
    }

    /**
     * Metóda získavajúca hodnotu členskej premennej type.
     * @return Hodnota členskej premennej type.
     */
    public Type getType() {
        return type;
    }

    /**
     * Metóda získavajúca hodnotu členskej premennej attributeId.
     * @return Hodnota členskej premennej attributeId.
     */
    public String getAttributeId() {
        return attributeId;
    }

    /**
     * Metóda získavajúca hodnotu členskej premennej code.
     * @return Hodnota členskej premennej code.
     */
    public String getCode() {
        return code;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof XamblCodeUnit) {
            XamblCodeUnit other = (XamblCodeUnit) obj;
            return (this.code.equals(other.getCode())
                    && this.attributeId.equals(other.getAttributeId())
                    && this.type.equals(other.getType()));
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 89 * hash + (this.code != null ? this.code.hashCode() : 0);
        hash = 89 * hash + (this.attributeId != null ? this.attributeId.hashCode() : 0);
        hash = 89 * hash + (this.type != null ? this.type.hashCode() : 0);
        return hash;
    }
}
