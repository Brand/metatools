package xammt.generator.modules.pemdxl.skeletons;

import data.models.metadata.definition.PmtdlDefinition;
import org.apache.velocity.VelocityContext;
import xammt.generator.modules.TemplateGenerating;
import xammt.generator.utility.Constants;

import java.io.*;

/**
 * Modul jazykového generátora nástroja XAMMT, ktorého
 * hlavnou úlohou je generovanie kostier súborov
 * definujúcich programové metaúdaje v jazyku PEMDXL.
 * @author Martin Pribula
 */
public class PemdxlSkeletonGenerator {

    /**
     * Definícia štruktúry metaúdajov, ktoré je možné generovaným PEMDXL jazykom definovať.
     */
    private PmtdlDefinition pmtdlDefinition;
    /**
     * Adresár obsahujúci vygenerované schémy jazyka PEMDXL.
     */
    private String pemdxlSchemasDirectoryPath;
    /**
     * Výstupný adresár pre generovanie kostier.
     */
    private String outputDirectoryPath;

    /**
     * Konštruktor triedy.
     * @param pmtdlDefinition Definícia štruktúry metaúdajov.
     * @param pemdxlSchemasDirectoryPath Adresár obsahujúci vygenerované schémy jazyka PEMDXL.
     * @param outputDirectoryPath Výstupný adresár pre generovanie kostier.
     */
    public PemdxlSkeletonGenerator(PmtdlDefinition pmtdlDefinition, String pemdxlSchemasDirectoryPath, String outputDirectoryPath) {
        this.pmtdlDefinition = pmtdlDefinition;
        this.pemdxlSchemasDirectoryPath = pemdxlSchemasDirectoryPath;
        this.outputDirectoryPath = outputDirectoryPath;
    }

    /**
     * Generovanie všetkých kostier súborov, definujúcich
     * programové metaúdaje v jazyku PEMDXL.
     */
    public void generateOutput() {
        generatePackagePart();
        generateAnnotationsPart();
        generateEnumPart();
        generateInterfacePart();
        generateClassPart();
    }

    /**
     * Generovanie súborov pre definíciu metaúdajov anotačného typu.
     */
    private void generateAnnotationsPart() {
        generateOutput(Constants.PEMDXL_ANNOTATION_SKELETON_TEMPLATE_RESOURCE, outputDirectoryPath + "\\simple_annotation_id.xml");
    }

    /**
     * Generovanie súborov pre definíciu metaúdajov enumeračného typu.
     */
    private void generateEnumPart() {
        generateOutput(Constants.PEMDXL_ENUM_SKELETON_TEMPLATE_RESOURCE, outputDirectoryPath + "\\simple_enum_id.xml");
    }

    /**
     * Generovanie súborov pre definíciu metaúdajov rozhrania.
     */
    private void generateInterfacePart() {
        generateOutput(Constants.PEMDXL_INTERFACE_SKELETON_TEMPLATE_RESOURCE, outputDirectoryPath + "\\simple_interface_id.xml");
    }

    /**
     * Generovanie súborov pre definíciu metaúdajov triedy.
     */
    private void generateClassPart() {
        generateOutput(Constants.PEMDXL_CLASS_SKELETON_TEMPLATE_RESOURCE, outputDirectoryPath + "\\simple_class_id.xml");
    }

    /**
     * Generovanie súborov pre definíciu metaúdajov balíka.
     */
    private void generatePackagePart() {
        generateOutput(Constants.PEMDXL_PACKAGE_SKELETON_TEMPLATE_RESOURCE, outputDirectoryPath + "\\package-level-metadata.xml");
    }

    /**
     * Metóda zabezpečuje generovanie textového výstupu pomocou šablóny.
     * Generovaný výstup je umiestnený na určené miesto v súborovom systéme.
     * @param templatePath Zdroj reprezentujúci textovú šablónu pre generovanie.
     * @param outputFilePath Výstupný adresár generovania.
     */
    private void generateOutput(String templatePath, String outputFilePath) {
        FileOutputStream fileOutputStream = null;
        try {
            VelocityContext velocityContext = new VelocityContext();
            velocityContext.put("pemdxlSchemasDirectoryPath", pemdxlSchemasDirectoryPath.replaceAll("\\s", "%20"));
            velocityContext.put("pmtdlDefinition", pmtdlDefinition);
            String fileContent = TemplateGenerating.generateCode(velocityContext, templatePath);
            fileOutputStream = new FileOutputStream(new File(outputFilePath));
            Writer writer = new OutputStreamWriter(fileOutputStream);
            writer.write(fileContent);
            writer.close();
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        } finally {
            try {
                fileOutputStream.close();
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }
}
