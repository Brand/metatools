package xammt.generator.modules;

import data.models.metadata.definition.PmtdlDefinition;
import xammt.generator.configuration.datamodel.Configuration;
import xammt.generator.modules.annotation.AnnotationGenerator;
import xammt.generator.modules.pemdxl.PemdxlGenerator;
import xammt.generator.modules.pemdxl.skeletons.PemdxlSkeletonGenerator;
import xammt.generator.modules.xambl.XamblGenerator;
import xammt.generator.modules.xambl.skeletons.XamblSkeletonGenerator;
import xammt.generator.utility.Constants;

import java.io.File;

/**
 * Trieda predstavuje hlavný modul jazykového generátora nástroja XAMMT.
 * @author Martin Pribula
 */
public class OutputGenerator {

    /**
     * Vstupné konfiguračné údaje.
     */
    private Configuration configuration;
    /**
     * Údaje z používateľovho vstupného PMTDL súboru.
     */
    private PmtdlDefinition pmtdlDefinition;
    /**
     * Modul generátora jazyka PEMDXL.
     */
    private PemdxlGenerator pemdxlGenerator;
    /**
     * Modul generátora anotačného jazyka.
     */
    private AnnotationGenerator annotationGenerator;
    /**
     * Modul generátora mapovacieho XAMBL súboru.
     */
    private XamblGenerator xamblGenerator;
    /**
     * Modul generátora kostry mapovacieho XAMBL súboru.
     */
    private XamblSkeletonGenerator xamblSkeletonGenerator;
    /**
     * Modul generátora kostier PEMDXL súborov.
     */
    private PemdxlSkeletonGenerator pemdxlSkeletonGenerator;

    /**
     * Konštruktor triedy.
     */
    public OutputGenerator() {
    }

    /**
     * Metóda pre získanie hodnoty členskej premennej configuration.
     * @return Hodnota členskej premennej configuration.
     */
    public Configuration getConfiguration() {
        return configuration;
    }

    /**
     * Metóda pre zavedenie hodnoty do členskej premennej configuration.
     * @param configuration Hodnota, ktorá sa zavádza.
     */
    public void setConfiguration(Configuration configuration) {
        this.configuration = configuration;
    }

    /**
     * Metóda umožňujúca ukladanie konfiguračných údajov do explicitne určeného súboru.
     */
    private void saveConfiguration() {
        File defaultFile = new File(Constants.DEFAULT_CONFIGURATION_FILE);
        Configuration.saveConfiguration(defaultFile, configuration);
    }

    /**
     * Hlavná metóda triedy umožňujúca generovanie všetkých požadovaných výstupov jazykového generátora.
     */
    public void generate() {
        pmtdlDefinition = PmtdlDefinition.load(new File(configuration.getPmtdlSourceFile()));

        if (configuration.getJavaOutputDirectory() != null) {
            annotationGenerator = new AnnotationGenerator(pmtdlDefinition, new File(configuration.getJavaOutputDirectory()), configuration.getJavaOutputType());
            annotationGenerator.generateOutput();
        }

        if (configuration.getPemdxlSchemasOutputDirectory() != null) {
            pemdxlGenerator = new PemdxlGenerator(pmtdlDefinition, new File(configuration.getPemdxlSchemasOutputDirectory()));
            pemdxlGenerator.generateOutput();
        }

        if (configuration.getXamblOutputDirectory() != null) {
            xamblGenerator = new XamblGenerator(pmtdlDefinition, new File(configuration.getXamblOutputDirectory()));
            xamblGenerator.generateOutput();
        }

        if (configuration.getXamblSkeletonOutputDirectory() != null) {
            xamblSkeletonGenerator = new XamblSkeletonGenerator(pmtdlDefinition, configuration.getXamblSchemaPath(), configuration.getXamblSkeletonOutputDirectory());
            xamblSkeletonGenerator.generateOutput();
        }

        if (configuration.getPemdxlSkeletonsOutputDirectory() != null) {
            pemdxlSkeletonGenerator = new PemdxlSkeletonGenerator(pmtdlDefinition, configuration.getPemdxlSchemasOutputDirectory(), configuration.getPemdxlSkeletonsOutputDirectory());
            pemdxlSkeletonGenerator.generateOutput();
        }
        saveConfiguration();
    }
}
