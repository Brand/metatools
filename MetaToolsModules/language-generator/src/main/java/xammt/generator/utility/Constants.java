package xammt.generator.utility;

/**
 * Trieda zapuzdrujúca textové konštanty používané v
 * zdrojovom kóde jazykového generátora.
 * @author Martin Pribula
 */
public class Constants {

    public static final String ANNOTATION_TEMPLATE_RESOURCE = "/xammt/generator/modules/annotation/templates/annotation.vm";
    public static final String PEMDXL_PACKAGE_TEMPLATE_RESOURCE = "/xammt/generator/modules/pemdxl/templates/package.vm";
    public static final String PEMDXL_ANNOTATION_TEMPLATE_RESOURCE = "/xammt/generator/modules/pemdxl/templates/annotation.vm";
    public static final String PEMDXL_ENUM_TEMPLATE_RESOURCE = "/xammt/generator/modules/pemdxl/templates/enum.vm";
    public static final String PEMDXL_INTERFACE_TEMPLATE_RESOURCE = "/xammt/generator/modules/pemdxl/templates/interface.vm";
    public static final String PEMDXL_CLASS_TEMPLATE_RESOURCE = "/xammt/generator/modules/pemdxl/templates/class.vm";
    public static final String PEMDXL_TYPE_DEFINITION_TEMPLATE_RESOURCE = "/xammt/generator/modules/pemdxl/templates/type-definition.vm";
    public static final String PEMDXL_PACKAGE_SKELETON_TEMPLATE_RESOURCE = "/xammt/generator/modules/pemdxl/skeletons/templates/package.vm";
    public static final String PEMDXL_ANNOTATION_SKELETON_TEMPLATE_RESOURCE = "/xammt/generator/modules/pemdxl/skeletons/templates/annotation.vm";
    public static final String PEMDXL_ENUM_SKELETON_TEMPLATE_RESOURCE = "/xammt/generator/modules/pemdxl/skeletons/templates/enum.vm";
    public static final String PEMDXL_INTERFACE_SKELETON_TEMPLATE_RESOURCE = "/xammt/generator/modules/pemdxl/skeletons/templates/interface.vm";
    public static final String PEMDXL_CLASS_SKELETON_TEMPLATE_RESOURCE = "/xammt/generator/modules/pemdxl/skeletons/templates/class.vm";
    public static final String XAMBL_TEMPLATE_RESOURCE = "/xammt/generator/modules/xambl/templates/xambl.vm";
    public static final String XAMBL_ATTRIBUTE_BINDING_TEMPLATE_RESOURCE = "/xammt/generator/modules/xambl/templates/attribute-binding.vm";
    public static final String XAMBL_SKELETON_TEMPLATE_RESOURCE = "/xammt/generator/modules/xambl/skeletons/templates/xambl.vm";
    public static final String DEFAULT_CONFIGURATION_FILE = "./configuration/sources/language-generator-configuration.xml";
}
