package xammt.generator.utility;

import java.io.File;

/**
 * Trieda poskytujúca pomocné metódy pre generátor.
 * @author Pribina
 */
public class Utility {

    /**
     * Metóda meniaca meno balíka na súborovú cestu v systéme windows.
     * @param packageName Kvalifikované meno balíka.
     * @return Súborová cesta systému Windows.
     */
    public String packageNameToWindowsPath(String packageName) {
        return packageName.replace('.', '\\');
    }

    /**
     * Metóda meniaca meno balíka na súborovú cestu v rámci platformy Java.
     * @param packageName Kvalifikované meno balíka.
     * @return Súborová cesta v rámci platformy Java.
     */
    public String packageNameToJavaPath(String packageName) {
        return packageName.replace('.', '/');
    }

    /**
     * Metóda pre získanie výstupného adresára, do ktorého je potrebné
     * ukladať elementy zapuzdrené daným java balíkom.
     * @param packageName Mano balíka.
     * @param upperLevelDirectory Rodičovský adresár balíka.
     * @return Výstupný adresár.
     */
    public String getOutputDirectory(String packageName, String upperLevelDirectory) {
        return upperLevelDirectory + "\\" + packageName;
    }

    /**
     * Metóda určená na rekurzívne mazanie adresára.
     * @param dir Adresár ktorý je nutné vymazať.
     * @return Úspešnosť pri mazaní.
     */
    public boolean deleteDir(File dir) {
        if (dir.isDirectory()) {
            String[] children = dir.list();
            for (String child : children) {
                boolean success = deleteDir(new File(dir, child));
                if (!success) {
                    return false;
                }
            }
        }
        return dir.delete();
    }
}
