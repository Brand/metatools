package xammt.generator.starter;

import xammt.generator.configuration.datamodel.Configuration;
import xammt.generator.gui.MainFrame;
import xammt.generator.utility.Constants;

import javax.swing.*;
import java.io.File;

/**
 * Trieda reprezentujúca spúšťač jazykového generátora.
 * @author Martin Pribula
 */
public class Starter {

    /**
     * Hlavná metóda spúšťača.
     * @param args Argumenty príkazového riadku.
     */
    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (UnsupportedLookAndFeelException ex) {
            System.out.println(ex.getMessage());
        } catch (ClassNotFoundException ex) {
            System.out.println(ex.getMessage());
        } catch (InstantiationException ex) {
            System.out.println(ex.getMessage());
        } catch (IllegalAccessException ex) {
            System.out.println(ex.getMessage());
        }
        Configuration configuration = Configuration.loadConfiguration(new File(Constants.DEFAULT_CONFIGURATION_FILE));
        MainFrame mainFrame;
        if (configuration != null) {
            mainFrame = new MainFrame(configuration);
        } else {
            mainFrame = new MainFrame();
        }
        mainFrame.setVisible(true);
    }
}
